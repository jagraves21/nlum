import time

import numpy as np

import pysptools.noise

#import sys
#sys.path.append("./anchor-baggage/anchor-word-recovery/")
from .anchor_baggage.anchor_word_recovery.fastRecover import do_recovery
from .anchor_baggage.anchor_word_recovery.anchors import findAnchors
from .anchor_baggage.anchor_word_recovery.gram_schmidt_stable import Projection_Find

class Params:

    def __init__(self, filename):
        self.log_prefix=None
        self.checkpoint_prefix=None
        self.seed = int(time.time())

        with open(filename) as fp:
            for l in fp:
                if l == "\n" or l[0] == "#":
                    continue
                l = l.strip()
                l = l.split('=')
                if l[0] == "log_prefix":
                    self.log_prefix = l[1]
                elif l[0] == "max_threads":
                    self.max_threads = int(l[1])
                elif l[0] == "eps":
                    self.eps = float(l[1])
                elif l[0] == "checkpoint_prefix":
                    self.checkpoint_prefix = l[1]
                elif l[0] == "new_dim":
                    self.new_dim = int(l[1])
                elif l[0] == "seed":
                    self.seed = int(l[1])
                elif l[0] == "anchor_thresh":
                    self.anchor_thresh = int(l[1])
                elif l[0] == "top_words":
                    self.top_words = int(l[1])

def generate_Q_matrix(M, words_per_doc=None):
    num_spectra = M.shape[0]
    num_pixels = M.shape[1]
    
    diag_M = np.zeros(num_spectra)

    # construct non-zero matrix of M
    boolean_M =  np.zeros(shape=M.shape, dtype=bool)
    boolean_M[M!=0]=1
    # print boolean_M.shape
    words_per_doc = np.sum(boolean_M,axis=0)
    for j in xrange(num_pixels):
        row_indices = (boolean_M[:,j]==True)
        # print row_indices.shape
        # print row_indices
        diag_M[row_indices] += M[row_indices,j]/(words_per_doc[j]*words_per_doc[j]-1)
        M[:,j]=M[:,j]/np.sqrt(words_per_doc[j]*words_per_doc[j]-1)

    Q = np.matmul(M,M.transpose())/num_pixels
    diag_M = diag_M/num_pixels
    Q = Q - np.diag(diag_M)
            
    return Q

# Recover the end members through the anchor words as pure pixels assumption
# holds valid for HSI images
def anchor_words_endmembers(data, K=5, filename="./anchor-baggage/anchor-word-recovery/settings.example"):
    Q = generate_Q_matrix(data.T)
    candidate_anchors = range(data.shape[1])
    params=Params(filename)
    
    gram = np.matmul(data.T , data)
    anchors_new = Projection_Find(gram, K, candidate_anchors)
    #endmembers_anchor, topic_likelihoods = do_recovery(Q, anchors_new[1], 'originalRecover', params)
    
    endmembers_anchor, topic_likelihoods = do_recovery(Q, anchors_new[1], 'L2', params)
    return endmembers_anchor, topic_likelihoods

