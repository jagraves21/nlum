import warnings
import inspect
import numpy as np
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn import utils as sk_utils

from nlum.utils import time_function

import torch
import torch.nn as nn
import torch.optim as optim
import torch.utils.data
class View(nn.Module):
    def __init__(self, *shape):
        super(View, self).__init__()
        self.shape = shape
    
    def forward(self, x):
        return x.view(*self.shape)
    
    def extra_repr(self):
        return ", ".join( map(str,self.shape) )
    
class SoftReLU(nn.Module):
    def __init__(self, num_features, inplace=False):
        super(SoftReLU, self).__init__()
        self.num_features = num_features
        self.inplace = inplace
        alpha = torch.Tensor(num_features)
        alpha.normal_()
        self.alpha = nn.Parameter( alpha )
    
    def forward(self, x):
        return nn.functional.relu(x - self.alpha, inplace=self.inplace)
    
    def extra_repr(self):
        if self.inplace:
            return "{}, inplace".format(self.num_features)
        else:
            return "{}".format(self.num_features)
        
class SumToOne(nn.Module):
    def __init__(self):
        super(SumToOne, self).__init__()
    
    def forward(self, x):
        res = x.cpu().detach().sum(1, keepdim=True)
        return x / (x.sum(1, keepdim=True).clamp(0) + 1e-3)
        
class SoftLeakyReLU(nn.Module):
    def __init__(self, num_features, inplace=False):
        super(SoftLeakyReLU, self).__init__()
        self.num_features = num_features
        self.inplace = inplace
        alpha = torch.Tensor(num_features)
        alpha.normal_()
        self.alpha = nn.Parameter( alpha )
    
    def forward(self, x):
        return nn.functional.leaky_relu(x - self.alpha, inplace=self.inplace)
    
    def extra_repr(self):
        if self.inplace:
            return "{}, inplace".format(self.num_features)
        else:
            return "{}".format(self.num_features)

# https://discuss.pytorch.org/t/writing-a-simple-gaussian-noise-layer-in-pytorch/4694/4
class GaussianNoise(nn.Module):
    """Gaussian noise regularizer.

    Args:
        sigma (float, optional): relative standard deviation used to generate the
            noise. Relative means that it will be multiplied by the magnitude of
            the value your are adding the noise to. This means that sigma can be
            the same regardless of the scale of the vector.
        is_relative_detach (bool, optional): whether to detach the variable before
            computing the scale of the noise. If `False` then the scale of the noise
            won't be seen as a constant but something to optimize: this will bias the
            network to generate vectors with smaller values.
    """

    def __init__(self, sigma=0.1, is_relative_detach=True):
        super(GaussianNoise, self).__init__()
        self.sigma = sigma
        self.is_relative_detach = is_relative_detach
        noise = torch.tensor(0, dtype=torch.float, requires_grad=False)
        self.noise = nn.Parameter(noise, requires_grad=False)

    def forward(self, x):
        if self.training and self.sigma != 0:
            if self.is_relative_detach:
                scale = self.sigma * x.detach()
            else:
                scale = self.sigma * x
            sampled_noise = self.noise.repeat(*x.size()).normal_() * scale
            x = x + sampled_noise
        return x 

def get_model(input_dim, output_dim, target_dim, model):
    kernel_size = 13
    
    if model == 0:
        layers = []
        layers.append( nn.Linear(input_dim, 9*target_dim) )
        layers.append( nn.Sigmoid() )
        layers.append( nn.Linear(9*target_dim, 6*target_dim) )
        layers.append( nn.Sigmoid() )
        layers.append( nn.Linear(6*target_dim, 3*target_dim) )
        layers.append( nn.Sigmoid() )
        layers.append( nn.Linear(3*target_dim, target_dim) )
        layers.append( nn.Sigmoid() )
        layers.append( nn.BatchNorm1d(target_dim) )
        layers.append( SoftReLU(target_dim) )
        layers.append( SumToOne() )
        layers.append( GaussianNoise() )
        encoder = nn.Sequential(*layers)
        
        layers = []
        layers.append( nn.Linear(target_dim, output_dim, bias=False) )
        decoder = nn.Sequential(*layers)
    elif model == 1:
        layers = []
        layers.append( nn.Linear(input_dim, 9*target_dim) )
        layers.append( nn.Sigmoid() )
        layers.append( nn.Linear(9*target_dim, 6*target_dim) )
        layers.append( nn.Sigmoid() )
        layers.append( nn.Linear(6*target_dim, 3*target_dim) )
        layers.append( nn.Sigmoid() )
        layers.append( nn.Linear(3*target_dim, target_dim) )
        layers.append( nn.Sigmoid() )
        layers.append( nn.BatchNorm1d(target_dim) )
        layers.append( SoftLeakyReLU(target_dim) )
        layers.append( SumToOne() )
        layers.append( GaussianNoise() )
        encoder = nn.Sequential(*layers)
        
        layers = []
        layers.append( nn.Linear(target_dim, output_dim, bias=False) )
        decoder = nn.Sequential(*layers)
    elif model == 2:
        layers = []
        layers.append( nn.Linear(input_dim, 9*target_dim) )
        layers.append( nn.ReLU(inplace=True) )
        layers.append( nn.Linear(9*target_dim, 6*target_dim) )
        layers.append( nn.ReLU(inplace=True) )
        layers.append( nn.Linear(6*target_dim, 3*target_dim) )
        layers.append( nn.ReLU(inplace=True) )
        layers.append( nn.Linear(3*target_dim, target_dim) )
        layers.append( nn.ReLU(inplace=True) )
        layers.append( nn.BatchNorm1d(target_dim) )
        layers.append( SoftReLU(target_dim) )
        layers.append( SumToOne() )
        layers.append( GaussianNoise() )
        encoder = nn.Sequential(*layers)
        
        layers = []
        layers.append( nn.Linear(target_dim, output_dim, bias=False) )
        decoder = nn.Sequential(*layers)
    elif model == 3:
        layers = []
        layers.append( nn.Linear(input_dim, 9*target_dim) )
        layers.append( nn.ReLU(inplace=True) )
        layers.append( nn.Linear(9*target_dim, 6*target_dim) )
        layers.append( nn.ReLU(inplace=True) )
        layers.append( nn.Linear(6*target_dim, 3*target_dim) )
        layers.append( nn.ReLU(inplace=True) )
        layers.append( nn.Linear(3*target_dim, target_dim) )
        layers.append( nn.ReLU(inplace=True) )
        layers.append( nn.BatchNorm1d(target_dim) )
        layers.append( SoftLeakyReLU(target_dim) )
        layers.append( SumToOne() )
        layers.append( GaussianNoise() )
        encoder = nn.Sequential(*layers)
        
        layers = []
        layers.append( nn.Linear(target_dim, output_dim, bias=False) )
        decoder = nn.Sequential(*layers)
    elif model == 4:
        layers = []
        layers.append( nn.Linear(input_dim, 9*target_dim) )
        layers.append( nn.LeakyReLU(inplace=True) )
        layers.append( nn.Linear(9*target_dim, 6*target_dim) )
        layers.append( nn.LeakyReLU(inplace=True) )
        layers.append( nn.Linear(6*target_dim, 3*target_dim) )
        layers.append( nn.LeakyReLU(inplace=True) )
        layers.append( nn.Linear(3*target_dim, target_dim) )
        layers.append( nn.LeakyReLU(inplace=True) )
        layers.append( nn.BatchNorm1d(target_dim) )
        layers.append( SoftReLU(target_dim) )
        layers.append( SumToOne() )
        layers.append( GaussianNoise() )
        encoder = nn.Sequential(*layers)
        
        layers = []
        layers.append( nn.Linear(target_dim, output_dim, bias=False) )
        decoder = nn.Sequential(*layers)
    elif model == 5:
        layers = []
        layers.append( nn.Linear(input_dim, 9*target_dim) )
        layers.append( nn.LeakyReLU(inplace=True) )
        layers.append( nn.Linear(9*target_dim, 6*target_dim) )
        layers.append( nn.LeakyReLU(inplace=True) )
        layers.append( nn.Linear(6*target_dim, 3*target_dim) )
        layers.append( nn.LeakyReLU(inplace=True) )
        layers.append( nn.Linear(3*target_dim, target_dim) )
        layers.append( nn.LeakyReLU(inplace=True) )
        layers.append( nn.BatchNorm1d(target_dim) )
        layers.append( SoftLeakyReLU(target_dim) )
        layers.append( SumToOne() )
        layers.append( GaussianNoise() )
        encoder = nn.Sequential(*layers)
        
        layers = []
        layers.append( nn.Linear(target_dim, output_dim, bias=False) )
        decoder = nn.Sequential(*layers)
    elif model == 6:
        layers = []
        layers.append( nn.Linear(input_dim, target_dim) )
        layers.append( nn.BatchNorm1d(target_dim) )
        layers.append( SoftReLU(target_dim) )
        layers.append( SumToOne() )
        layers.append( GaussianNoise() )
        encoder = nn.Sequential(*layers)
        
        layers = []
        layers.append( nn.Linear(target_dim, output_dim, bias=False) )
        decoder = nn.Sequential(*layers)
    
    
    
    
    elif model == 7:
        layers = []
        layers.append( nn.Linear(input_dim, 9*target_dim) )
        layers.append( nn.Sigmoid() )
        layers.append( nn.Linear(9*target_dim, 6*target_dim) )
        layers.append( nn.Sigmoid() )
        layers.append( nn.Linear(6*target_dim, 3*target_dim) )
        layers.append( nn.Sigmoid() )
        layers.append( nn.Linear(3*target_dim, target_dim) )
        layers.append( nn.Sigmoid() )
        layers.append( nn.BatchNorm1d(target_dim) )
        layers.append( SoftReLU(target_dim) )
        layers.append( SumToOne() )
        layers.append( GaussianNoise() )
        encoder = nn.Sequential(*layers)
        
        layers = []
        layers.append( nn.Linear(target_dim, 3*target_dim) )
        layers.append( nn.Sigmoid() )
        layers.append( nn.Linear(3*target_dim, 6*target_dim) )
        layers.append( nn.Sigmoid() )
        layers.append( nn.Linear(6*target_dim, 9*target_dim) )
        layers.append( nn.Sigmoid() )
        layers.append( nn.Linear(9*target_dim, output_dim) )
        decoder = nn.Sequential(*layers)
    elif model == 8:
        layers = []
        layers.append( nn.Linear(input_dim, 9*target_dim) )
        layers.append( nn.Sigmoid() )
        layers.append( nn.Linear(9*target_dim, 6*target_dim) )
        layers.append( nn.Sigmoid() )
        layers.append( nn.Linear(6*target_dim, 3*target_dim) )
        layers.append( nn.Sigmoid() )
        layers.append( nn.Linear(3*target_dim, target_dim) )
        layers.append( nn.Sigmoid() )
        layers.append( nn.BatchNorm1d(target_dim) )
        layers.append( SoftLeakyReLU(target_dim) )
        layers.append( SumToOne() )
        layers.append( GaussianNoise() )
        encoder = nn.Sequential(*layers)
        
        layers = []
        layers.append( nn.Linear(target_dim, 3*target_dim) )
        layers.append( nn.Sigmoid() )
        layers.append( nn.Linear(3*target_dim, 6*target_dim) )
        layers.append( nn.Sigmoid() )
        layers.append( nn.Linear(6*target_dim, 9*target_dim) )
        layers.append( nn.Sigmoid() )
        layers.append( nn.Linear(9*target_dim, output_dim) )
        decoder = nn.Sequential(*layers)
    elif model == 9:
        layers = []
        layers.append( nn.Linear(input_dim, 9*target_dim) )
        layers.append( nn.ReLU(inplace=True) )
        layers.append( nn.Linear(9*target_dim, 6*target_dim) )
        layers.append( nn.ReLU(inplace=True) )
        layers.append( nn.Linear(6*target_dim, 3*target_dim) )
        layers.append( nn.ReLU(inplace=True) )
        layers.append( nn.Linear(3*target_dim, target_dim) )
        layers.append( nn.ReLU(inplace=True) )
        layers.append( nn.BatchNorm1d(target_dim) )
        layers.append( SoftReLU(target_dim) )
        layers.append( SumToOne() )
        layers.append( GaussianNoise() )
        encoder = nn.Sequential(*layers)
        
        layers = []
        layers.append( nn.Linear(target_dim, 3*target_dim) )
        layers.append( nn.ReLU(inplace=True) )
        layers.append( nn.Linear(3*target_dim, 6*target_dim) )
        layers.append( nn.ReLU(inplace=True) )
        layers.append( nn.Linear(6*target_dim, 9*target_dim) )
        layers.append( nn.ReLU(inplace=True) )
        layers.append( nn.Linear(9*target_dim, output_dim) )
        decoder = nn.Sequential(*layers)
    elif model == 10:
        layers = []
        layers.append( nn.Linear(input_dim, 9*target_dim) )
        layers.append( nn.ReLU(inplace=True) )
        layers.append( nn.Linear(9*target_dim, 6*target_dim) )
        layers.append( nn.ReLU(inplace=True) )
        layers.append( nn.Linear(6*target_dim, 3*target_dim) )
        layers.append( nn.ReLU(inplace=True) )
        layers.append( nn.Linear(3*target_dim, target_dim) )
        layers.append( nn.ReLU(inplace=True) )
        layers.append( nn.BatchNorm1d(target_dim) )
        layers.append( SoftLeakyReLU(target_dim) )
        layers.append( SumToOne() )
        layers.append( GaussianNoise() )
        encoder = nn.Sequential(*layers)
        
        layers = []
        layers.append( nn.Linear(target_dim, 3*target_dim) )
        layers.append( nn.ReLU(inplace=True) )
        layers.append( nn.Linear(3*target_dim, 6*target_dim) )
        layers.append( nn.ReLU(inplace=True) )
        layers.append( nn.Linear(6*target_dim, 9*target_dim) )
        layers.append( nn.ReLU(inplace=True) )
        layers.append( nn.Linear(9*target_dim, output_dim) )
        decoder = nn.Sequential(*layers)
    elif model == 11:
        layers = []
        layers.append( nn.Linear(input_dim, 9*target_dim) )
        layers.append( nn.LeakyReLU(inplace=True) )
        layers.append( nn.Linear(9*target_dim, 6*target_dim) )
        layers.append( nn.LeakyReLU(inplace=True) )
        layers.append( nn.Linear(6*target_dim, 3*target_dim) )
        layers.append( nn.LeakyReLU(inplace=True) )
        layers.append( nn.Linear(3*target_dim, target_dim) )
        layers.append( nn.LeakyReLU(inplace=True) )
        layers.append( nn.BatchNorm1d(target_dim) )
        layers.append( SoftReLU(target_dim) )
        layers.append( SumToOne() )
        layers.append( GaussianNoise() )
        encoder = nn.Sequential(*layers)
        
        layers = []
        layers.append( nn.Linear(target_dim, 3*target_dim) )
        layers.append( nn.LeakyReLU(inplace=True) )
        layers.append( nn.Linear(3*target_dim, 6*target_dim) )
        layers.append( nn.LeakyReLU(inplace=True) )
        layers.append( nn.Linear(6*target_dim, 9*target_dim) )
        layers.append( nn.LeakyReLU(inplace=True) )
        layers.append( nn.Linear(9*target_dim, output_dim) )
        decoder = nn.Sequential(*layers)
    elif model == 12:
        layers = []
        layers.append( nn.Linear(input_dim, 9*target_dim) )
        layers.append( nn.LeakyReLU(inplace=True) )
        layers.append( nn.Linear(9*target_dim, 6*target_dim) )
        layers.append( nn.LeakyReLU(inplace=True) )
        layers.append( nn.Linear(6*target_dim, 3*target_dim) )
        layers.append( nn.LeakyReLU(inplace=True) )
        layers.append( nn.Linear(3*target_dim, target_dim) )
        layers.append( nn.LeakyReLU(inplace=True) )
        layers.append( nn.BatchNorm1d(target_dim) )
        layers.append( SoftLeakyReLU(target_dim) )
        layers.append( SumToOne() )
        layers.append( GaussianNoise() )
        encoder = nn.Sequential(*layers)
        
        layers = []
        layers.append( nn.Linear(target_dim, 3*target_dim) )
        layers.append( nn.LeakyReLU(inplace=True) )
        layers.append( nn.Linear(3*target_dim, 6*target_dim) )
        layers.append( nn.LeakyReLU(inplace=True) )
        layers.append( nn.Linear(6*target_dim, 9*target_dim) )
        layers.append( nn.LeakyReLU(inplace=True) )
        layers.append( nn.Linear(9*target_dim, output_dim) )
        decoder = nn.Sequential(*layers)
    
    return encoder, decoder

class _AutoEncoder(nn.Module):
    def __init__(self, input_dim, output_dim, encoder_dim=4, model=0):
        super(_AutoEncoder, self).__init__()
        self.encoder, self.decoder = get_model(input_dim, output_dim, encoder_dim, model)

    def forward(self, X):
        return self.decoder( self.encoder( X ) )







class AutoEncoder(BaseEstimator, TransformerMixin):
    def __init__(
        self,
        encoder_dim=4,
        n_epochs=200,
        batch_learning=True,
        batch_size=128,
        shuffle=True,
        optimizer=optim.Adam,
        optimizer_kwargs={},
        loss_function=nn.MSELoss,
        loss_function_kwargs={},
        model=0,
        verbose=0,
        cuda_device=None
    ):
        super(AutoEncoder, self).__init__()
        args, _, _, values = inspect.getargvalues(inspect.currentframe())
        values.pop("self")
        for arg, val in values.items():
            setattr(self, arg, val)

        if cuda_device:
            if torch.cuda.is_available():
#                 self.device = torch.device("cuda")
                self.device = torch.device(cuda_device)
            else:
                self.device = torch.device("cpu")
                warnings.warn("cuda not avaliable", UserWarning)
        else:
            self.device = torch.device("cpu")

    @time_function("Training Time:")
    def fit(self, X, y=None, target_endmembers=None, supervised_target=None):
        # Check that X and y have correct shape
        #X, y = sk_utils.check_X_y(X, y, ensure_2d=False, allow_nd=True)

        # Build the model
        self._model = self._train_simple_classifier(X, y, target_endmembers, supervised_target)

        # Return the classifier
        return self

    def transform(self, X):
        # Check is fit had been called
        sk_utils.validation.check_is_fitted(self, ['_model'])

        # Input validation
        X = sk_utils.check_array(X)

        # Ensure data lives on the correct device
        X = torch.from_numpy(X.astype(np.float32)).to(self.device)

        with torch.no_grad():
            self._model.eval()
            output = self._model.forward( X )
            return output.cpu().numpy()

    def encode(self, X):
        # Check is fit had been called
        sk_utils.validation.check_is_fitted(self, ['_model'])

        # Input validation
        X = sk_utils.check_array(X)

        # Ensure data lives on the correct device
        X = torch.from_numpy(X.astype(np.float32)).to(self.device)

        with torch.no_grad():
            self._model.eval()
            output = self._model.encoder( X )
            return output.cpu().numpy()

    def decode(self, X):
        # Check is fit had been called
        sk_utils.validation.check_is_fitted(self, ['_model'])

        # Input validation
        X = sk_utils.check_array(X)

        # Ensure data lives on the correct device
        X = torch.from_numpy(X.astype(np.float32)).to(self.device)

        with torch.no_grad():
            self._model.eval()
            output = self._model.decoder( X )
            return output.cpu().numpy()

    def _train_simple_classifier(self, x_train, y_train, target_endmembers, supervised_target):
        n_samples = x_train.shape[0]
        input_dim = x_train.shape[-1]
        output_dim = y_train.shape[-1]

        # Ensure data lives on the correct device
        x_train = torch.from_numpy(x_train.astype(np.float32)).to(self.device)
        y_train = torch.from_numpy(y_train.astype(np.float32)).to(self.device)
        
        if target_endmembers is not None:
            target_endmembers = torch.from_numpy(target_endmembers.astype(np.float32)).to(self.device)

        model = _AutoEncoder(input_dim, output_dim, encoder_dim=self.encoder_dim, model=self.model).to(self.device)
        
        loss_function = self.loss_function(**self.loss_function_kwargs)
        
        optimizer = self.optimizer(model.parameters(), **self.optimizer_kwargs)

        print model
        if self.batch_learning:
            return self._batch_train_simple_classifier(x_train, y_train, target_endmembers, supervised_target, model, loss_function, optimizer)
        else:
            return self._minibatch_train_simple_classifier(x_train, y_train, target_endmembers, supervised_target, model, loss_function, optimizer)

    def _batch_train_simple_classifier(self, x_train, y_train, target_endmembers, supervised_target, model, loss_function, optimizer):
        n_epochs_digits = int(np.log10(self.n_epochs)) + 1
        
        if target_endmembers is not None:
            identity = np.identity(self.encoder_dim,np.float32)[:len(target_endmembers),:]
            identity = torch.from_numpy(identity).to(self.device)

        self.errors = []
        for epoch in range(1, self.n_epochs+1):
            model.train()

            # Clear any calculated gradients
            optimizer.zero_grad()

            # Forward pass, compute outputs
            outputs = model.forward(x_train)

            # Compute loss
            loss = loss_function(outputs, y_train)
            
            if target_endmembers is not None:
                if supervised_target == "autoencoder" or supervised_target == "encoder":
                    endmember_outputs = model.encoder(target_endmembers)
                    endmember_loss = loss_function(endmember_outputs, identity)
                    loss += endmember_loss

                if supervised_target == "autoencoder" or supervised_target == "decoder":
                    endmember_outputs = model.decoder(identity)
                    endmember_loss = loss_function(endmember_outputs, target_endmembers)
                    loss += endmember_loss

            # Backward pass, compute gradients
            loss.backward()
            self.errors.append(loss.item())

            # Update learnable parameters
            optimizer.step()

            if self.verbose > 0:
                if epoch % 100 == 0 or epoch == self.n_epochs:
                    message = "Train Epoch: {}, Loss: {:15.6f}".format(
                        str(epoch).rjust(n_epochs_digits, " "),
                        loss.item()
                    )
                    print message

        self.errors = np.asarray( self.errors )
        return model

    def _minibatch_train_simple_classifier(self, x_train, y_train, target_endmembers, supervised_target, model, loss_function, optimizer):
        train_data = torch.utils.data.TensorDataset(x_train, y_train)
        train_loader = torch.utils.data.DataLoader(train_data, batch_size=self.batch_size, shuffle=self.shuffle)

        batch_mod = 50

        n_epochs_digits = int(np.log10(self.n_epochs)) + 1
        n_dataset_digits = int(np.log10(len(train_loader.dataset))) + 1
        n_batch_digits = int(np.log10(len(train_loader))) + 1
        
        if target_endmembers is not None:
            identity = np.identity(self.encoder_dim,np.float32)[:len(target_endmembers),:]
            identity = torch.from_numpy(identity).to(self.device)

        self.errors = []
        for epoch in range(1, self.n_epochs+1):
            total_samples = 0
            for batch_idx, (data, target) in enumerate(train_loader, 1):
                total_samples += len(data)

                model.train()

                # Clear any calculated gradients
                optimizer.zero_grad()

                # Forward pass, compute outputs
                outputs = model.forward(data)

                # Compute loss
                loss = loss_function(outputs, target)
                
                if target_endmembers is not None:
                    if supervised_target == "autoencoder" or supervised_target == "encoder":
                        endmember_outputs = model.encoder(target_endmembers)
                        endmember_loss = loss_function(endmember_outputs, identity)
                        loss += endmember_loss

                    if supervised_target == "autoencoder" or supervised_target == "decoder":
                        endmember_outputs = model.decoder(identity)
                        endmember_loss = loss_function(endmember_outputs, target_endmembers)
                        loss += endmember_loss

                # Backward pass, compute gradients
                loss.backward()

                # Update learnable parameters
                optimizer.step()

                if self.verbose > 1:
                    if (batch_idx % batch_mod == 0 or batch_idx == len(train_loader)):
                        message = "Train Epoch: {0} [{1}/{2} ({3}/{4}) {5:6.2f}%]   Loss: {6:15.6f}".format(
                            str(epoch).rjust(n_epochs_digits, " "),
                            str(total_samples).rjust(n_dataset_digits, " "),
                            len(train_loader.dataset),
                            str(batch_idx).rjust(n_batch_digits, " "),
                            len(train_loader),
                            100. * batch_idx / len(train_loader),
                            loss.item()
                        )
                        print message

            if self.verbose > 0:
                if self.verbose > 1 or epoch % 10 == 0 or epoch == self.n_epochs:
                    model.eval()
                    outputs = model.forward(x_train)
                    loss = loss_function(outputs, y_train)
                    message = "Train Epoch: {}, Loss: {:15.6f}".format(
                        str(epoch).rjust(n_epochs_digits, " "),
                        loss.item()
                    )
                    print message
                if self.verbose > 1:
                    print

            model.eval()
            outputs = model.forward(x_train)
            loss = loss_function(outputs, y_train)
            self.errors.append(loss.item())

        self.errors = np.asarray( self.errors )
        return model
