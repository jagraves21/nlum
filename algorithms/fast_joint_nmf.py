import inspect

import numpy as np

from sklearn import metrics
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn import utils as sk_utils

from nonnegfac import matrix_utils as mu
from nonnegfac import nnls

from nlum.utils import time_function

# X is of size (n_features x n_samples)
# S is of size (n_samples x n_samples)
# W is of size (n_features x k)
# H is of size (n_samples x k)
# H_hat is of size (n_samples x k)
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn import utils as sk_utils
def _fast_joint_nmf_iter_solver(X, W, H, H_hat, alpha, beta, reg_lambda):
    assert W.shape[1] == H.shape[1], "{} != {}".format(W.shape[1], H.shape[1])
    n_features,n_samples = X.shape 
    k = W.shape[1]

    assert W.shape == (n_features,k), "{} != {}".format(W.shape, (n_features,k))
    assert H.shape == (n_samples,k), "{} != {}".format(H.shape, (n_samples,k))
    assert H_hat.shape == (n_samples,k), "{} != {}".format(H_hat.shape, (n_samples,k))
    
    # equation 9 in paper https://arxiv.org/pdf/1703.09646.pdf
    # AtA is of size (k x k)
    AtA = H.T.dot(H) + reg_lambda*np.identity(H.shape[1])
    assert AtA.shape == (k,k), "{} != {}".format(AtA.shape, (k,k))
    AtB = H.T.dot(X.T)
    Sol, info = nnls.nnlsm_blockpivot(AtA, AtB, is_input_prod=True, init=W.T)
    W = Sol.T

    # equation 10 in paper https://arxiv.org/pdf/1703.09646.pdf
    # lhs is \alpha * HtH + beta*I_k
    # lhs is of size (k x k)
    lhs = alpha * np.matmul(H.T,H) + beta * np.identity(H.shape[1])
    assert lhs.shape == (k,k), "{} != {}".format(lhs.shape, (k,k))
    # tmp is of size (k x n_features)
    tmp = np.matmul(H.T, X.T)
    assert tmp.shape == (k,n_features), "{} != {}".format(tmp.shape, (k,n_features))
    # rhs is \alpha * XtXH + beta*Ht 
    # rhs is of size (k x n_samples)
    rhs = alpha * np.matmul(tmp, X) + beta * H.T
    assert rhs.shape == (k,n_samples), "{} != {}".format(rhs.shape, (k,n_samples))
    #Sol, info = nnls.nnlsm_blockpivot(lhs, rhs)
    # AtA is \alpha * HtH + beta*I_k + reg_lamba*I_k
    # AtA is of size (k x k)
    AtA = lhs + reg_lambda*np.identity(lhs.shape[1])
    assert AtA.shape == (k,k), "{} != {}".format(AtA.shape, (k,k))
    AtB = rhs
    Sol, info = nnls.nnlsm_blockpivot(AtA, AtB, is_input_prod=True)
    # H_hat is of size (n_samples x k)
    H_hat = Sol.T
    assert H_hat.shape == (n_samples,k), "{} != {}".format(H_hat.shape, (n_samples,k))

    # equation 11 in paper https://arxiv.org/pdf/1703.09646.pdf    
    # lhs is WtW + \alpha*H_hatTH_hat + \beta*I_k
    # lhs is of size (k x k)
    lhs = np.matmul(W.T, W) + alpha * np.matmul(H_hat.T, H_hat) + beta * np.identity(H.shape[1])
    assert lhs.shape == (k,k), "{} != {}".format(lhs.shape, (k,k))
    # rhs is WtX + \alpha * H_hatt*S + \beta * H_hatt
    # rhs1 is of size (k x n_samples)
    rhs1 = np.matmul(W.T, X)
    assert rhs1.shape == (k,n_samples), "{} != {}".format(rhs1.shape, (k,n_samples))
    # tmp2 is of size (k x n_features)
    tmp2 = np.matmul(H_hat.T, X.T)
    assert tmp2.shape == (k,n_features), "{} != {}".format(tmp2.shape, (k,n_features))
    # rhs is of size (k x n_samples)
    rhs = rhs1 + alpha *np.matmul(tmp2, X) + beta * H_hat.T    
    assert rhs.shape == (k,n_samples), "{} != {}".format(rhs.shape, (k,n_samples))
    # AtA is lhs + reg_lamba*I_k    
    # AtA is of size (k x k)
    AtA = lhs + reg_lambda*np.identity(lhs.shape[1])
    assert AtA.shape == (k,k), "{} != {}".format(AtA.shape, (k,k))
    # AtB is of size (k x n_samples)
    AtB = rhs
    assert AtB.shape == (k,n_samples), "{} != {}".format(AtB.shape, (k,n_samples))
    Sol, info = nnls.nnlsm_blockpivot(AtA, AtB, is_input_prod=True)
    # H is of size (n_samples x k)
    H = Sol.T
    assert H.shape == (n_samples,k), "{} != {}".format(H.shape, (n_samples,k))
    return (W, H, H_hat)

def fast_joint_nmf(
    X, W=None, H=None, n_components=2,
    max_iter=200, alpha=0., beta=1., reg_lambda=1e-3,
    random_state=None
):
    """ Run a NMF algorithm
            Parameters
            ----------
            A : numpy.array or scipy.sparse matrix, shape (m,n)
            m : Number of features
            n : Number of samples
            k : int - target lower rank
            Returns
            -------
            (W, H, rec)
            W : Obtained factor matrix, shape (m,k)
            H : Obtained coefficient matrix, shape (n,k)
    """
    rng = sk_utils.check_random_state(random_state)
    X = sk_utils.check_array(X, dtype=float)
    sk_utils.validation.check_non_negative(
        X, "joint_non_negative_factorization"
    )
    H_hat = rng.rand(X.shape[1], n_components)
    norm_X = mu.norm_fro(X)
    if W is None and H is None:
        W = rng.rand(X.shape[0], n_components)
        H = rng.rand(X.shape[1], n_components)
    elif W is None:
        #Sol, info = nnls.nnlsm_blockpivot(H, X.T)
        AtA = H.T.dot(H) + reg_lambda*np.identity(H.shape[1])
        AtB = H.T.dot(X.T)
        Sol, info = nnls.nnlsm_blockpivot(AtA, AtB, is_input_prod=True)
        W = Sol.T
    elif H is None:
        #Sol, info = nnls.nnlsm_blockpivot(W, X)
        AtA = W.T.dot(W) + reg_lambda*np.identity(W.shape[1])
        AtB = W.T.dot(X)
        Sol, info = nnls.nnlsm_blockpivot(AtA, AtB, is_input_prod=True)
        H = Sol.T 

        lhs = alpha * np.matmul(H.T,H) + beta * np.identity(H.shape[1])
        tmp = np.matmul(H.T, X.T)
        rhs = alpha * np.matmul(tmp, X) + beta *H.T
        #Sol, info = nnls.nnlsm_blockpivot(lhs, rhs, True)
        AtA = lhs.T.dot(lhs) + reg_lambda*np.identity(lhs.shape[1])
        AtB = lhs.T.dot(rhs)
        Sol, info = nnls.nnlsm_blockpivot(AtA, AtB, is_input_prod=True)
        H_hat = Sol.T

        lhs = np.matmul(W.T, W) + alpha * np.matmul(H_hat.T, H_hat) + beta * np.identity(H.shape[1])
        tmp1 = np.matmul(W.T, X)
        tmp2 = np.matmul(H.T, X.T)
        rhs = tmp1 + alpha *np.matmul(tmp2, X) + beta * H_hat.T
        #Sol, info = nnls.nnlsm_blockpivot(lhs, rhs, True)
        AtA = lhs.T.dot(lhs) + reg_lambda*np.identity(lhs.shape[1])
        AtB = lhs.T.dot(rhs)
        Sol, info = nnls.nnlsm_blockpivot(AtA, AtB, is_input_prod=True)
        H = Sol.T
    #print "Fast Joint-NMF"
    #print "*Initial W"
    #print W
    #print "*Initial H"
    #print H
    Ws = [W]
    Hs = [H]
    
    for i in xrange(max_iter):
        (W, H, H_hat) = _fast_joint_nmf_iter_solver(X, W, H, H_hat, alpha, beta, reg_lambda)
        rel_error = mu.norm_fro_err(X, W, H, norm_X) / norm_X
        #print "* Iteration", i+1
        #print "@Current W"
        #print W
        #print "@Current H"
        #print H
        #print "@Current H_hat"
        #print H_hat
        #print "@Rel Error"
        #print rel_error
        Ws.append(W.copy())
        Hs.append(H.copy())
    return W, H, Ws, Hs

class FastJointNMF(BaseEstimator, TransformerMixin):
    """
    solves min_W>=0,H>=0,H_hat>=0 ||X-WH.T||_F^2 + alpha * ||S-H_hat*H.T||_F^2 +
                                   beta * ||H_hat - H||_F^2
    Equation 8 in paper https://arxiv.org/pdf/1703.09646.pdf
    """
    def __init__(
        self, n_components=2,
        max_iter=200, alpha=0., beta=1., reg_lambda=1e-3,
        random_state=None
    ):
        super(FastJointNMF, self).__init__()
        args, _, _, values = inspect.getargvalues(inspect.currentframe())
        values.pop("self")
        for arg, val in values.items():
            setattr(self, arg, val)

    def fit_transform(self, X, y=None, W=None, H=None):
        X = sk_utils.check_array(X, dtype=float)
        X = X.T
        W, H = H, W
        if W is not None:
            W = W.T
        
        W, H, self.Ws, self.Hs = fast_joint_nmf(
            X=X, W=W, H=H, n_components=self.n_components,
            max_iter=self.max_iter,
            alpha=self.alpha, beta=self.beta, reg_lambda=self.reg_lambda,
            random_state=self.random_state
        )
        
        W, H = H, W.T

        self.n_components_ = H.shape[0]
        self.components_ = H
        return W

    def fit(self, X, **params):
        self.fit_transform(X, **params)
