import numpy as np
from sklearn import metrics
from nonnegfac import matrix_utils as mu

# implementation of the paper
# https://www.hindawi.com/journals/mpe/2015/239589/
# Deng cai graph regularized nmf.

from sklearn.base import BaseEstimator, TransformerMixin
from sklearn import utils as sk_utils
def _graph_regularized_nmf_iter_solver(X, S, D, W, H, lambda_reg, alpha_reg):
    # equation 11 of paper https://www.hindawi.com/journals/mpe/2015/239589/
    XtW = np.matmul(X.T, W)
    SH = np.matmul(S, H)
    WtW = np.matmul(W.T, W)
    HWtW = np.matmul(H, WtW)
    DH = np.matmul(np.diagflat(D), H)
    H_nr = 2 * (XtW + lambda_reg*SH) - alpha_reg
    H_dr = 2 * (HWtW + lambda_reg*DH)
    H = np.divide(H_nr, H_dr)
    XH = np.matmul(X, H)
    HtH = np.matmul(H.T, H)
    WHtH = np.matmul(W, HtH)
    W = np.divide(XH, WHtH)
    W = W / (W.sum(axis=1)[:,np.newaxis])
    return (W, H)
    
def graph_regularized_nmf(X, W=None, H=None, n_components=None,
                                     max_iter=200, lambda_reg=0.1, alpha_reg=0.1):
    """ Run a NMF algorithm
            Parameters
            ----------
            A : numpy.array or scipy.sparse matrix, shape (m,n)
            m : Number of features
            n : Number of samples
            k : int - target lower rank
            lambda_reg : Regularization constant for GRNMF
            alpha : L1 regularization constant for H matrix
            Returns
            -------
            (W, H, rec)
            W : Obtained factor matrix, shape (m,k)
            H : Obtained coefficient matrix, shape (n,k)
    """
    X = sk_utils.check_array(X, dtype=float)
    sk_utils.validation.check_non_negative(X, "joint_non_negative_factorization")
    if W is None and H is None:
        W = np.random.rand(X.shape[0], n_components)
        H = np.random.rand(X.shape[1], n_components)
    elif W is None:
        Sol, info = nnls.nnlsm_blockpivot(H, X.T)
        W = Sol.T
    elif H is None:
        Sol, info = nnls.nnlsm_blockpivot(W.T, X)
        H = Sol.T
    S = metrics.pairwise_kernels(X.T)
    # normalize the distance matrix between 0 to 1
    S = S-np.min(S)/(np.max(S)-np.min(S))
    D = np.sum(S, axis=1)
    norm_X = mu.norm_fro(X)
    for i in range(1, max_iter + 1):
        (W, H) = _graph_regularized_nmf_iter_solver(X, S, D, W, H, lambda_reg, alpha_reg)
        rel_error = mu.norm_fro_err(X, W, H, norm_X) / norm_X
    return W, H.T

class GraphRegularizedNMF(BaseEstimator, TransformerMixin):
    """
    solves min_W>=0,H>=0 ||A-WH.T||_F^2 + lambda * Tr(H.T L H) +
                                   alpha * sum_i=1^n || h_i||_1
    where, L = D - W. Remember, if we explicity compute L, we cannot
    use in MU type of algorithms as the off diagonal entries of L are negative. 

    Equation 10 in paper https://www.hindawi.com/journals/mpe/2015/239589/
    If lambda is zero, it is spparse NMF and alpha is 0 it is deng cai
    graph regularized nmf
    """
    def __init__(self, n_components=None, max_iter=100, lambda_reg=0.1, alpha_reg=0.1):
        self.n_components = n_components
        self.max_iter = max_iter
        self.lambda_reg = lambda_reg
        self.alpha_reg = alpha_reg
    
    def fit_transform(self, X, y=None, W=None, H=None):
        X = sk_utils.check_array(X, dtype=float)
        W, H = graph_regularized_nmf(
            X=X, W=W, H=H, n_components=self.n_components,
            max_iter=self.max_iter, lambda_reg=self.lambda_reg,
            alpha_reg=self.alpha_reg)
        
        self.n_components_ = H.shape[0]
        self.components_ = H
        return W
    
    def fit(self, X, y=None, **params):
        self.fit_transform(X, **params)
