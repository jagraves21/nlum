import inspect

import numpy as np

from sklearn import metrics
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn import utils as sk_utils

import scipy

from nonnegfac import matrix_utils as mu
from nonnegfac import nnls

# X is of size (n_features x n_samples)
# S is of size (n_samples x n_samples)
# W is of size (n_features x k)
# H is of size (n_samples x k)
# H_hat is of size (n_samples x k)
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn import utils as sk_utils

# import nlum
# @nlum.utils.time_function("_joint_nmf_iter_solver")
def _joint_nmf_iter_solver(X, S, W, H, H_hat, alpha, beta, reg_lambda):
    assert W.shape[1] == H.shape[1], "{} != {}".format(W.shape[1], H.shape[1])
    n_features,n_samples = X.shape
    k = W.shape[1]

    assert S.shape == (n_samples,n_samples), "{} != {}".format(S.shape == (n_samples,n_samples))
    assert W.shape == (n_features,k), "{} != {}".format(W.shape, (n_features,k))
    assert H.shape == (n_samples,k), "{} != {}".format(H.shape, (n_samples,k))
    assert H_hat.shape == (n_samples,k), "{} != {}".format(H_hat.shape, (n_samples,k))
    
    # equation 9 in paper https://arxiv.org/pdf/1703.09646.pdf
    # AtA is of size (k x k)
    AtA = H.T.dot(H) + reg_lambda*np.identity(H.shape[1])
    assert AtA.shape == (k,k), "{} != {}".format(AtA.shape, (k,k))
    # AtB is of size (k x n_feastures)
    AtB = H.T.dot(X.T)
    assert AtB.shape == (k,n_features), "{} != {}".format(AtB.shape, (k,n_features))
    Sol, info = nnls.nnlsm_blockpivot(AtA, AtB, is_input_prod=True, init=W.T)
    # W is of size mxk
    W = Sol.T

    # equation 10 in paper https://arxiv.org/pdf/1703.09646.pdf
    # tmp is of size (k x k)
    tmp = np.sqrt(beta) * np.identity(H.shape[1])
    assert tmp.shape == (k,k), "{} != {}".format(tmp.shape, (k,k))
    # lhs is of size ((n_samples + k) x k)
    lhs = np.concatenate((np.sqrt(alpha) * H, tmp))
    assert lhs.shape == (n_samples + k,k), "{} != {}".format(lhs.shape, (n_samples + k,k))
    # tmp1 is of size (n_samples x k)
    tmp1 = np.sqrt(beta)*H
    assert tmp1.shape == (n_samples,k), "{} != {}".format(tmp1.shape, (n_samples,k))
    # rhs is of size (n_samples x (n_samples + k))
    # rhs is of size ((n_samples + k) x n_samples)
#     rhs = np.concatenate((np.sqrt(alpha)*S, tmp1.T))
#     assert rhs.shape == (n_samples+k,n_samples), "{} {}\n {} != {}".format(S.shape, tmp1.T.shape, rhs.shape, (n_samples+k,n_samples))
    # AtA will be \alpha * HtH + \beta*I_k + reg_lamba*(I_k)
    # AtA is of size (k x k)
    AtA = lhs.T.dot(lhs) + reg_lambda*np.identity(lhs.shape[1])
    assert AtA.shape == (k,k), "{} != {}".format(AtA.shape, (k,k))
    # AtB is of size (k x n_samples)    
#     AtB = lhs.T.dot(rhs)
    if scipy.sparse.issparse(S):
        AtB = alpha * H.T * S + beta * H.T
    else:    
        AtB = alpha * H.T.dot(S) + beta * H.T
    assert AtB.shape == (k,n_samples), "{} != {}".format(AtB.shape, (k,n_samples))
    Sol, info = nnls.nnlsm_blockpivot(AtA, AtB, is_input_prod=True)
    # H_hat is of size (n_samples x k)
    H_hat = Sol.T
    assert H_hat.shape == (n_samples,k), "{} != {}".format(H_hat.shape, (n_samples,k))

    # equation 11 in paper https://arxiv.org/pdf/1703.09646.pdf
    # tmp1 is of size (n_samples x k)
    tmp1 = np.sqrt(alpha) * H_hat
    assert tmp1.shape == (n_samples,k), "{} != {}".format(tmp1.shape, (n_samples,k))
    # tmp2 is of size (k x k)
    tmp2 = np.sqrt(beta) * np.identity(H.shape[1])
    assert tmp2.shape == (k,k), "{} != {}".format(tmp2.shape, (k,k))
    # lhs is of size ((n_samples + n_features + k) x k)
    lhs = np.concatenate((W, tmp1, tmp2))
    assert lhs.shape == (n_samples+n_features+k,k), "{} != {}".format(lhs.shape, (n_samples+n_features+k,k))
    # tmp3 is of size (n_samples x n_samples)
    tmp3 = np.sqrt(alpha)*S
    assert tmp3.shape == (n_samples,n_samples), "{} != {}".format(tmp3.shape, (n_samples,n_samples))
    # tmp4 is of size (k x n_samples)
    tmp4 = np.sqrt(beta) * H_hat.T
    assert tmp4.shape == (k,n_samples), "{} != {}".format(tmp4.shape, (k,n_samples))
    # rhs is of size ((n_features + n_samples + k) x n_samples)
#     rhs = np.concatenate((X, tmp3, tmp4))
#     assert rhs.shape == (n_features+n_samples+k,n_samples), "{} != {}".format(rhs.shape, (n_features+n_samples+k,n_samples))
    # AtA is WtW + \alpha*H_hatTH_hat + \beta*I_k + reg_lamba*I_k
    # AtA is of size (k x k)
    AtA = lhs.T.dot(lhs) + reg_lambda*np.identity(lhs.shape[1])
    assert AtA.shape == (k,k), "{} != {}".format(AtA.shape, (k,k))
    # AtB is WtX + \alpha * H_hatt*S + \beta * H_hatt
    # AtB is of size kxn
#     AtB = lhs.T.dot(rhs)
    if scipy.sparse.issparse(S):
        AtB = W.T.dot(X) + alpha * scipy.sparse.csr_matrix.dot(S,H_hat).T + beta * H_hat.T
    else:
        AtB = W.T.dot(X) + alpha * H_hat.T.dot(S) + beta * H_hat.T
    assert AtB.shape == (k,n_samples), "{} != {}".format(AtB.shape, (k,n_samples))
    Sol, info = nnls.nnlsm_blockpivot(AtA, AtB, is_input_prod=True)
    H = Sol.T
    return (W, H, H_hat)

def joint_nmf(
    X, W=None, H=None, n_components=2,
    max_iter=200, alpha=0., beta=1., reg_lambda=1e-3,
    random_state=None,
    kernel_kwargs=dict()
):
    """ Run a NMF algorithm
            Parameters
            ----------
            A : numpy.array or scipy.sparse matrix, shape (m,n)
            m : Number of features
            n : Number of samples
            k : int - target lower rank
            Returns
            -------
            (W, H, rec)
            W : Obtained factor matrix, shape (m,k)
            H : Obtained coefficient matrix, shape (n,k)
    """
    rng = sk_utils.check_random_state(random_state)
    X = sk_utils.check_array(X, dtype=float)
    sk_utils.validation.check_non_negative(
        X,
        "joint_nmf"
    )
    H_hat = rng.rand(X.shape[1], n_components)
    kernel_kwargs.setdefault("X", X.T)
    S = metrics.pairwise_kernels(**kernel_kwargs)
    metric = kernel_kwargs.get("metric", "linear")
    if metric == "rbf":
        S = scipy.sparse.csr_matrix(S)
    norm_X = mu.norm_fro(X)
    if W is None and H is None:
        W = rng.rand(X.shape[0], n_components)
        H = rng.rand(X.shape[1], n_components)
    elif W is None:
        #Sol, info = nnls.nnlsm_blockpivot(H, X.T)
        AtA = H.T.dot(H) + reg_lambda*np.identity(H.shape[1])
        AtB = H.T.dot(X.T)
        Sol, info = nnls.nnlsm_blockpivot(AtA, AtB, is_input_prod=True)
        W = Sol.T
    elif H is None:
        Sol, info = nnls.nnlsm_blockpivot(W, X)
        H = Sol.T  
        #H = random.rand(X.shape[1], n_components)
        tmp = np.sqrt(beta) * np.identity(H.shape[1])
        lhs = np.concatenate((np.sqrt(alpha) * H, tmp))
        tmp = np.sqrt(beta)*H
        rhs = np.concatenate((np.sqrt(alpha)*S, tmp.T))
        #Sol, info = nnls.nnlsm_blockpivot(lhs, rhs)
        AtA = lhs.T.dot(lhs) + reg_lambda*np.identity(lhs.shape[1])
        AtB = lhs.T.dot(rhs)
        Sol, info = nnls.nnlsm_blockpivot(AtA, AtB, is_input_prod=True)
        H_hat = Sol.T

        tmp_1 = np.sqrt(alpha) * H_hat
        tmp_2 = np.sqrt(beta) * np.identity(H.shape[1])
        lhs = np.concatenate((W, tmp_1, tmp_2))
        tmp_1 = np.sqrt(alpha)*S
        tmp_2 = np.sqrt(beta) * H_hat.T
        rhs = np.concatenate((X, tmp_1, tmp_2))
        #Sol, info = nnls.nnlsm_blockpivot(lhs, rhs)
        AtA = lhs.T.dot(lhs) + reg_lambda*np.identity(lhs.shape[1])
        AtB = lhs.T.dot(rhs)
        Sol, info = nnls.nnlsm_blockpivot(AtA, AtB, is_input_prod=True)
        H = Sol.T
    #print "Joint-NMF"
    #print "*Initial W"
    #print W
    #print "*Initial H"
    #print H
    Ws = [W]
    Hs = [H]
    
    if scipy.sparse.issparse(S):
        print "INFO", type(S), S.count_nonzero(), S.count_nonzero() / float(np.prod(S.shape))
        
    import cProfile
    profiler=cProfile.Profile()
    profiler.enable()
    for i in xrange(max_iter):
        (W, H, H_hat) = _joint_nmf_iter_solver(
            X, S, W, H, H_hat, alpha, beta, reg_lambda
        )
        rel_error = mu.norm_fro_err(X, W, H, norm_X) / norm_X
        #print "* Iteration", i
        #print "@Current W"
        #print W
        #print "@Current H"
        #print H
        #print "@Current H_hat"
        #print H_hat
        #print "@Rel Error"
        #print rel_error
        Ws.append(W.copy())
        Hs.append(H.copy())
    profiler.disable()
    profiler.dump_stats("_joint_nmf_iter_solver.cprof")
    return W, H, Ws, Hs

class JointNMF(BaseEstimator, TransformerMixin):
    """
    solves min_W>=0,H>=0,H_hat>=0 ||X-WH.T||_F^2 + alpha * ||S-H_hat*H.T||_F^2 +
                                   beta * ||H_hat - H||_F^2
    Equation 8 in paper https://arxiv.org/pdf/1703.09646.pdf
    """
    def __init__(
        self, n_components=2,
        metric="linear",
        max_iter=200, alpha=0., beta=1., reg_lambda=1e-3,
        random_state=None
    ):
        super(JointNMF, self).__init__()
        args, _, _, values = inspect.getargvalues(inspect.currentframe())
        values.pop("self")
        for arg, val in values.items():
            setattr(self, arg, val)

    def fit_transform(self, X, y=None, W=None, H=None, **kernel_kwargs):
        X = sk_utils.check_array(X, dtype=float)
        X = X.T
        W, H = H, W
        if W is not None:
            W = W.T
        
        kernel_kwargs["metric"] = self.metric
        
        W, H, self.Ws, self.Hs = joint_nmf(
            X=X, W=W, H=H, n_components=self.n_components,
            max_iter=self.max_iter,
            alpha=self.alpha,
            beta=self.beta,
            reg_lambda=self.reg_lambda,
            random_state=self.random_state,
            kernel_kwargs=kernel_kwargs
        )
        
        W, H = H, W.T

        self.n_components_ = H.shape[0]
        self.components_ = H
        return W

    def fit(self, X, **params):
        self.fit_transform(X, **params)
    
    # finish?
    # https://github.com/scikit-learn/scikit-learn/blob/b7b4d3e2f/sklearn/decomposition/nmf.py#L1074
