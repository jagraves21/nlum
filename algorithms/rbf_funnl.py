import numpy as np

from sklearn import preprocessing

import pysptools

from .joint_nmf import JointNMF
from .AnchorWords import anchor_words_endmembers

class RBFFUNNL(JointNMF):
    def __init__(
        self, n_components=None,
        max_iter=200, alpha=1e-3, beta=1, reg_lambda=1e-3,
        random_state=None,
        params_filename=None
    ):
        if params_filename is None:
            import os
            params_filename = os.path.join(os.path.dirname(__file__), "anchor_baggage/anchor_word_recovery/settings.example")
        self.params_filename = params_filename
        super(RBFFUNNL, self).__init__(
            n_components=n_components,
            metric="rbf",
            max_iter=max_iter,
            alpha=alpha,
            beta=beta,
            random_state=random_state
        )
        
    def fit_transform(self, X):
        cube = np.reshape(X, (1, X.shape[0], X.shape[1]))
        cube_denoised = pysptools.noise.SavitzkyGolay().denoise_spectra(cube, 3, 1)
        X_denoised = np.reshape(cube_denoised, X.shape)
        
        endmembers_anchor, topic_likelihoods = \
            anchor_words_endmembers(X_denoised, self.n_components, self.params_filename)

        #X_normalized = preprocessing.normalize(X_denoised, norm="l1", axis=0)
        
        return super(RBFFUNNL, self).fit_transform(X_denoised, H=endmembers_anchor.T)
        
        
