# standard library imports
import argparse

# third party imports
import numpy as np
from scipy import optimize

# local application imports
from .linear_mixing_model import linear_mixing_model

def generalized_bilinear_model(hsi_3d, endmembers, abundances=None):
    if abundances is None:
        _, abundances = linear_mixing_model(hsi_3d, endmembers)
    
    def model_gbm(endmembers, abundances):
        res = endmembers * abundances[:len(endmembers),np.newaxis]
        tmp = np.zeros(endmembers.shape[1])
        gamma_ii = len(endmembers)
        for ii in xrange(len(endmembers)-1):
            for jj in xrange(ii+1, len(endmembers)):
                tmp += abundances[gamma_ii] * res[ii] * res[jj]
                gamma_ii += 1
        res = np.sum(res,axis=0)
        res += tmp 
        return res

    hsi_2d = np.reshape(hsi_3d, (-1,hsi_3d.shape[2]))
    hsi_2d_res = np.zeros(hsi_2d.shape)
    
    abundances = np.reshape(abundances, (len(endmembers),-1))
    abundances = abundances.copy()
    
    guess = np.zeros(
        shape=(len(endmembers) + (len(endmembers)*(len(endmembers)-1)/2)),
    )
    
    for index,pixel in enumerate(hsi_2d):
        guess[:len(endmembers)] = abundances[:,index]
        
        res = optimize.minimize(
            lambda abundances: np.linalg.norm(pixel - model_gbm(endmembers,abundances)),
            guess,
            method="SLSQP",
            bounds=[(0.,1.) for xx in guess],
            constraints=[{"type":"eq", "fun":lambda xx: 1-np.sum(xx[:len(endmembers)])}]
        )
        
        hsi_2d_res[index] = model_gbm(endmembers, res.x)
        abundances[:,index] = res.x[:len(endmembers)]
        
        if not res.success:
            print res
            print res.x, np.sum(res.x)
        
            fig,ax = plt.subplots()
            ax.plot(pixel)
            ax.plot( model_gbm(endmembers, res.x) )
            plt.show()
    
    return np.reshape(hsi_2d_res, hsi_3d.shape), np.reshape(abundances, (len(endmembers),)+hsi_3d.shape[:-1])

def main(**kwargs):
    from utils import run_standalone
    run_standalone("generalized_bilinear", **args)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Generalized Bilinear Model",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )

    parser.add_argument(
        "hsi_3d_filename",
        type=str,
        metavar="hsi_infile",
        help="HSI input file"
    )
    parser.add_argument(
        "endmembers_filename",
        type=str,
        metavar="em_infile",
        help="endmembers input file"
    )
    parser.add_argument(
        "abundance_filename",
        type=str,
        nargs="?",
        metavar="am_infile",
        help="abundances input file"
    )
    parser.add_argument(
        "results_filename",
        type=str,
        metavar="outfile",
        help="output filename"
    )
    
    args = vars(parser.parse_args())
    main(**args)
