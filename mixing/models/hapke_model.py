# standard library imports
import argparse

# third party imports
import numpy as np
from scipy import optimize

# local application imports
from .linear_mixing_model import linear_mixing_model

def hapke_model(hsi_3d, endmembers, abundances=None, m0=np.cos(np.pi/4.), mm=1.):
    def to_albedos(XX):
        res = np.square(m0+mm) * np.square(XX)
        res += (1+4*mm*m0*XX) * (1-XX)
        res = np.sqrt(res)
        res -= (m0*mm)*XX
        res /= (1+4*mm*m0*XX)
        return 1 - np.square(res)
    
    def to_reflectance(XX):
        return XX / ((1+2*mm*np.sqrt(1-XX)) * (1+2*m0*np.sqrt(1-XX)));
    
    hsi_3d, abundances = linear_mixing_model(
        to_albedos(hsi_3d),
        to_albedos(endmembers)
    )
    
    return to_reflectance(hsi_3d), abundances

def main(**kwargs):
    from utils import run_standalone
    run_standalone("hapke", abundance_filename=None, **args)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Hapke Model",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    
    parser.add_argument(
        "--m0",
        type=float,
        default=np.cos(np.pi/4.),
        metavar="float",
        help="???"
    )
    parser.add_argument(
        "--mm",
        type=float,
        default=1.,
        metavar="float",
        help="???"
    )

    parser.add_argument(
        "hsi_3d_filename",
        type=str,
        metavar="hsi_infile",
        help="HSI input file"
    )
    parser.add_argument(
        "endmembers_filename",
        type=str,
        metavar="em_infile",
        help="endmembers input file"
    )
    parser.add_argument(
        "results_filename",
        type=str,
        metavar="outfile",
        help="output filename"
    )
    
    args = vars(parser.parse_args())
    main(**args)