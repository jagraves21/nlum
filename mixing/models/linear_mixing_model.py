# standard library imports
import argparse

# third party imports
import numpy as np
from scipy import optimize

def linear_mixing_model(hsi_3d, endmembers, abundances=None):
    def model_lmm(endmembers, abundances):
        return np.matmul(abundances, endmembers)

    hsi_2d = np.reshape(hsi_3d, (-1,hsi_3d.shape[2]))
    hsi_2d_res = np.zeros(hsi_2d.shape)
    abundances = np.zeros( (len(endmembers),hsi_2d.shape[0]) )
    
    guess = np.full(
        shape=(len(endmembers)),
        fill_value=1./len(endmembers)
    )
    
    for index,pixel in enumerate(hsi_2d):
        res = optimize.minimize(
            lambda abundances: np.linalg.norm(pixel - model_lmm(endmembers,abundances)),
            guess,
            method="SLSQP",
            bounds=[(0.,1.) for xx in guess],
            constraints=[{"type":"eq", "fun":lambda xx: 1-np.sum(xx)}]
        )
        
        hsi_2d_res[index] = model_lmm(endmembers, res.x)
        abundances[:,index] = res.x
        
        if not res.success:
            print res
            print res.x, np.sum(res.x)
        
            fig,ax = plt.subplots()
            ax.plot(pixel)
            ax.plot( model_lmm(endmembers, res.x) )
            plt.show()
    
    return np.reshape(hsi_2d_res, hsi_3d.shape), np.reshape(abundances, (len(endmembers),)+hsi_3d.shape[:-1])

def main(**kwargs):
    from utils import run_standalone
    run_standalone("linear", abundance_filename=None, **args)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Linear Mixing Model",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )

    parser.add_argument(
        "hsi_3d_filename",
        type=str,
        metavar="hsi_infile",
        help="HSI input file"
    )
    parser.add_argument(
        "endmembers_filename",
        type=str,
        metavar="em_infile",
        help="endmembers input file"
    )
    parser.add_argument(
        "results_filename",
        type=str,
        metavar="outfile",
        help="output filename"
    )
    
    args = vars(parser.parse_args())
    main(**args)