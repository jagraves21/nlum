# standard library imports
import argparse

# third party imports
import numpy as np
from scipy import optimize

# local application imports
from .linear_mixing_model import linear_mixing_model

def multilinear_mixing_model(hsi_3d, endmembers, abundances=None, p_min=-100.):
    if abundances is None:
        _, abundances = linear_mixing_model(hsi_3d, endmembers)
    
    def model_mlmm(endmembers, abundances):
        res = np.matmul(abundances[:-1], endmembers)
        res += abundances[-1] * np.square(res)
        return res
 
    hsi_2d = np.reshape(hsi_3d, (-1,hsi_3d.shape[2]))
    hsi_2d_res = np.zeros(hsi_2d.shape)
    
    abundances = np.reshape(abundances, (len(endmembers),-1))
    abundances = abundances.copy()
    
    guess = np.zeros(
        shape=(len(endmembers) + 1),
    )
    
    for index,pixel in enumerate(hsi_2d):
        guess[:len(endmembers)] = abundances[:,index]
        
        res = optimize.minimize(
            lambda abundances: np.linalg.norm(pixel - model_mlmm(endmembers,abundances)),
            guess,
            method="SLSQP",
            bounds=[(0.,1.) for xx in guess[:-1]] + [(p_min,1.)],
            constraints=[{"type":"eq", "fun":lambda xx: 1-np.sum(xx[:len(endmembers)])}]
        )
        
        hsi_2d_res[index] = model_mlmm(endmembers, res.x)
        abundances[:,index] = res.x[:len(endmembers)]
        
        if not res.success:
            print res
            print res.x, np.sum(res.x)
        
            fig,ax = plt.subplots()
            ax.plot(pixel)
            ax.plot( model_mlmm(endmembers, res.x) )
            plt.show()
    
    return np.reshape(hsi_2d_res, hsi_3d.shape), np.reshape(abundances, (len(endmembers),)+hsi_3d.shape[:-1])

def main(**kwargs):
    from utils import run_standalone
    run_standalone("multilinear", **args)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Multilinear Model",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )

    parser.add_argument(
        "--p_min",
        type=float,
        default=-100.,
        metavar="float",
        help="???"
    )
        
    parser.add_argument(
        "hsi_3d_filename",
        type=str,
        metavar="hsi_infile",
        help="HSI input file"
    )
    parser.add_argument(
        "endmembers_filename",
        type=str,
        metavar="em_infile",
        help="endmembers input file"
    )
    parser.add_argument(
        "abundance_filename",
        type=str,
        nargs="?",
        metavar="am_infile",
        help="abundances input file"
    )
    parser.add_argument(
        "results_filename",
        type=str,
        metavar="outfile",
        help="output filename"
    )
    
    args = vars(parser.parse_args())
    main(**args)
