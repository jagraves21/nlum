# standard library imports
import datetime
import functools
import platform
import socket

# third party imports
import numpy as np

# local application imports
from .linear_mixing_model import linear_mixing_model
from .fan_bilinear_model import fan_bilinear_model
from .generalized_bilinear_model import generalized_bilinear_model
from .multilinear_mixing_model import multilinear_mixing_model
from .polynomial_postnonlinear_model import polynomial_postnonlinear_model
from .hapke_model import hapke_model

MIXING_MODELS = {
    "linear":linear_mixing_model,
    "fan_bilinear":fan_bilinear_model,
    "generalized_bilinear":generalized_bilinear_model,
    "multilinear":multilinear_mixing_model,
    "polynomial_postnonlinear":polynomial_postnonlinear_model,
    "hapke":hapke_model
}

def mix(hsi_3d, endmembers, abundances=None, model="linear", **kwargs):
    if model in MIXING_MODELS:
        model = MIXING_MODELS[model]
    elif not callable(model):
        message = "Unknown mixing model `{}`".format(model)
        raise ValueError(message)
    func = functools.partial(model, hsi_3d, endmembers, abundances)
    return func(**kwargs)

def run_standalone(model, hsi_3d_filename, endmembers_filename, abundance_filename, results_filename, **kwargs):
    hsi_3d = np.load(hsi_3d_filename)
    endmembers = np.load(endmembers_filename)
    if abundance_filename is not None:
        abundances = np.load(abundance_filename)
    else:
        abundances = None
    
    res_hsi_3d, res_abundances = mix(
        hsi_3d=hsi_3d,
        endmembers=endmembers,
        abundances=abundances,
        model=model,
        **kwargs
    )
    
    timestamp = datetime.datetime.now().strftime("%Y_%m_%d-%H_%M_%S")
    machine_id = socket.getfqdn()
    platform_string = platform.platform()
    
    np.savez(
        results_filename,
        hsi_3d=res_hsi_3d,
        endmembers=endmembers,
        abundances=res_abundances,
        model=model,
        hsi_3d_filename=hsi_3d_filename,
        endmembers_filename=endmembers_filename,
        abundance_filename=abundance_filename,
        timestamp=timestamp,
        machine_id=machine_id,
        platform=platform_string,
        **kwargs
    )