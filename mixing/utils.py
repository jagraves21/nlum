# standard library imports
import functools
import os
import re
import warnings

# third party imports
import enum
import h5py

# local application imports
from ..utils import hdf as hdf_utils
from .models import mix

class Compute(enum.Enum):
    AUTO = "AUTO"
    RECOMPUTE = "RECOMPUTE"
    CACHE_ONLY = "CACHE_ONLY"
    
def _get_mixing_results_base_location(
    algorithm_name,
    hdf_hsi_3d_source=None,
):
    return "{}-{}".format(os.path.basename(hdf_hsi_3d_source.name), algorithm_name)
    
def _find_newest_mixing_result(
    algorithm_name,
    hdf_hsi_3d_source=None,
):
    group_name = _get_mixing_results_base_location(algorithm_name=algorithm_name, hdf_hsi_3d_source=hdf_hsi_3d_source) 
    pattern = "^{}[0-9]{{3}}$".format( re.escape("{}_".format(group_name)) )
    hdf_groups = hdf_utils.get_groups(hdf_hsi_3d_source.parent, pattern, regex=True)
    if len(hdf_groups) > 0:
        hdf_groups.sort(key=lambda hdf_group: hdf_group.attrs["time_stamp"])
        return hdf_groups[-1]
    else:
        return None
    
def create_mixing_dataset(
    hdf_group,
    
    hsi_3d,
    endmembers,
    abundances,
    
    hsi_3d_attrs=dict(),
    endmembers_attrs=dict(),
    abundances_attrs=dict()
):
    hsi_3d_attrs = dict(hsi_3d_attrs)
    if "name" not in hsi_3d_attrs:
        hsi_3d_attrs["name"] = "Mixed_Data"
    hdf_hsi_3d_data = hdf_utils.create_hsi_3d_dataset(hdf_group, hsi_3d=hsi_3d, **hsi_3d_attrs)
    
    if endmembers is not None:
        endmembers_attrs = dict(endmembers_attrs)
        if "name" not in endmembers_attrs:
            endmembers_attrs["name"] = "Endmembers"
        hdf_endmembers_data = hdf_utils.create_endmembers_dataset(hdf_group, endmembers=endmembers, **endmembers_attrs)
    else:
        hdf_endmembers_data = None
    
    if abundances is not None:
        abundances_attrs = dict(abundances_attrs)
        if "name" not in abundances_attrs:
            abundances_attrs["name"] = "Abundances"
        if hdf_endmembers_data is not None:
            if "Position_Indices" not in abundances_attrs:
                abundances_attrs["Position_Indices"] = hdf_endmembers_data.attrs["Position_Indices"]
            if "Position_Values" not in abundances_attrs:
                abundances_attrs["Position_Values"] = hdf_endmembers_data.attrs["Position_Values"]
        hdf_abundances_data = hdf_utils.create_abundances_dataset(hdf_group, abundances=abundances, **abundances_attrs)
    else:
        hdf_abundances_data = None
    
    return hdf_hsi_3d_data, hdf_endmembers_data, hdf_abundances_data

def load_mixing_results(
    hdf_group,
    hsi_3d_name="Mixed_Data",
    endmembers_name="Endmembers",
    abundances_name="Abundances"
):
    hsi_3d = None if hsi_3d_name is None else hdf_utils.load_dataset(hdf_group, hsi_3d_name)
    endmembers = None if endmembers_name is None else hdf_utils.load_dataset(hdf_group, endmembers_name)
    abundances = None if abundances_name is None else hdf_utils.load_dataset(hdf_group, abundances_name)
    return hsi_3d, endmembers, abundances 

def save_mixing_results(
    algorithm_name,
    
    hsi_3d,
    endmembers,
    abundances,
    
    hdf_hsi_3d_source=None,
    hdf_endmembers_source=None,
    hdf_abundances_source=None,
    
    hsi_3d_attrs=dict(),
    endmembers_attrs=dict(),
    abundances_attrs=dict(),
    group_attrs=dict()
):
    group_name = _get_mixing_results_base_location(
        algorithm_name,
        hdf_hsi_3d_source=hdf_hsi_3d_source,
    )
    
    pattern = "^{}[0-9]{{3}}$".format( re.escape("{}_".format(group_name)) )
    hdf_groups = hdf_utils.get_groups(hdf_endmembers_source.parent, pattern, regex=True)
    idx = 0
    for hdf_group in hdf_groups:
        try:
            tmp_idx = int( hdf_group.name.split("_")[-1] )
            if idx <= tmp_idx:
                idx = tmp_idx+1
        except:
            pass
    group_name = "{}_{:03}".format(group_name, idx)
    
    group_attrs = dict(group_attrs)
    group_attrs.setdefault("algorithm", algorithm_name)
    group_attrs.setdefault("source", hdf_hsi_3d_source.ref)
    hdf_mixing_group = hdf_utils.create_group(hdf_endmembers_source.parent, group_name, **group_attrs)
    
    hdf_hsi_3d_data, hdf_endmembers_data, hdf_abundances_data = create_mixing_dataset(
        hdf_mixing_group,
        
        hsi_3d=hsi_3d,
        endmembers=endmembers,
        abundances=abundances,
        
        hsi_3d_attrs=hsi_3d_attrs,
        endmembers_attrs=endmembers_attrs,
        abundances_attrs=abundances_attrs
    )
    
    return hdf_mixing_group, hdf_hsi_3d_data, hdf_endmembers_data, hdf_abundances_data

def run_algorithm(
    algorithm,
    compute,
    
    hsi_3d,
    endmembers,
    abundances=None,
    
    hdf_hsi_3d_source=None,
    hdf_endmembers_source=None,
    hdf_abundances_source=None,
    
    hsi_3d_attrs=dict(),
    endmembers_attrs=dict(),
    abundances_attrs=dict(),
    
    group_attrs=dict(),
    
    save_results=True
):
    if not isinstance(compute, Compute):
        raise TypeError("`compute` must be an instance of Compute Enum")
        
    algorithm_name = algorithm.title()
    
    if compute != Compute.RECOMPUTE:
        try:
            message = []
            if hdf_hsi_3d_source is None:
                message.append("hdf_hsi_3d_source must not be None")
            if hdf_endmembers_source is None:
                message.append("endmembers_attrs must not be None")
            if len(message) > 0:
                message = "When compute = Compute.CACHE_ONLY, {}".format( ",".join(message) )
                raise ValueError(message)
        
            hdf_mixing_group = _find_newest_mixing_result(
                algorithm_name=algorithm_name,
                hdf_hsi_3d_source=hdf_hsi_3d_source,
            )

            if hdf_mixing_group is None:
                message = "{}: no suitable results found".format(algorithm)
                raise RuntimeError(message)

            mixed_hsi_3d, mixed_endmembers, mixed_abundances = load_mixing_results(hdf_mixing_group)

            return mixed_hsi_3d, mixed_endmembers, mixed_abundances, hdf_mixing_group
        except StandardError as se:
            if compute == Compute.CACHE_ONLY: 
                raise
            else:
                message = "{}, executing {}".format(str(se), algorithm)
                warnings.warn(message)
    
    mixed_hsi_3d, mixed_abundances = mix(hsi_3d, endmembers, abundances=None, model=algorithm)
    mixed_endmembers = endmembers.copy()
    
    if hdf_hsi_3d_source is None:
        hdf_mixing_group = None
        if save_results:
            message = "unable to save results: hdf_hsi_3d_source not provided"
            warnings.warn(message)
    else:
        hsi_3d_attrs = dict(hsi_3d_attrs)
        if "Position_Indices" in hdf_hsi_3d_source.attrs:
            hsi_3d_attrs.setdefault("Position_Indices", hdf_hsi_3d_source.attrs["Position_Indices"])
        if "Position_Values" in hdf_hsi_3d_source.attrs:
            hsi_3d_attrs.setdefault("Position_Values", hdf_hsi_3d_source.attrs["Position_Values"])
        if "Spectroscopic_Indices" in hdf_hsi_3d_source.attrs:
            hsi_3d_attrs.setdefault("Spectroscopic_Indices", hdf_hsi_3d_source.attrs["Spectroscopic_Indices"])
        if "Spectroscopic_Values" in hdf_hsi_3d_source.attrs:
            hsi_3d_attrs.setdefault("Spectroscopic_Values", hdf_hsi_3d_source.attrs["Spectroscopic_Values"])

        if hdf_endmembers_source is not None:
            endmembers_attrs = dict(endmembers_attrs)
            if "Position_Indices" in hdf_endmembers_source.attrs:
                endmembers_attrs.setdefault("Position_Indices", hdf_endmembers_source.attrs["Position_Indices"])
            if "Position_Values" in hdf_endmembers_source.attrs:
                endmembers_attrs.setdefault("Position_Values", hdf_endmembers_source.attrs["Position_Values"])
            if "Spectroscopic_Indices" in hdf_endmembers_source.attrs:
                endmembers_attrs.setdefault("Spectroscopic_Indices", hdf_endmembers_source.attrs["Spectroscopic_Indices"])
            if "Spectroscopic_Values" in hdf_endmembers_source.attrs:
                endmembers_attrs.setdefault("Spectroscopic_Values", hdf_endmembers_source.attrs["Spectroscopic_Values"])
            group_attrs["endmembers"] = hdf_endmembers_source.ref
        
        if abundances is not None:
            if hdf_abundances_source is not None:
                abundances_attrs = dict(abundances_attrs)
                if "Position_Indices" in hdf_abundances_source.attrs:
                    abundances_attrs.setdefault("Position_Indices", hdf_abundances_source.attrs["Position_Indices"])
                if "Position_Values" in hdf_abundances_source.attrs:
                    abundances_attrs.setdefault("Position_Values", hdf_abundances_source.attrs["Position_Values"])
                if "Spectroscopic_Indices" in hdf_abundances_source.attrs:
                    abundances_attrs.setdefault("Spectroscopic_Indices", hdf_abundances_source.attrs["Spectroscopic_Indices"])
                if "Spectroscopic_Values" in hdf_abundances_source.attrs:
                    abundances_attrs.setdefault("Spectroscopic_Values", hdf_abundances_source.attrs["Spectroscopic_Values"])
                group_attrs["abundances"] = hdf_abundances_source.ref
            else:
                group_attrs["abundances"] = "N/A"
        
        if save_results:
            hdf_mixing_group, _, _, _ = save_mixing_results(
                algorithm_name=algorithm_name,

                hsi_3d=mixed_hsi_3d,
                endmembers=mixed_endmembers,
                abundances=mixed_abundances,

                hdf_hsi_3d_source=hdf_hsi_3d_source,
                hdf_endmembers_source=hdf_endmembers_source,
                hdf_abundances_source=hdf_abundances_source,

                hsi_3d_attrs=hsi_3d_attrs,
                endmembers_attrs=endmembers_attrs,
                abundances_attrs=abundances_attrs,
                group_attrs=group_attrs
            )
    return mixed_hsi_3d, mixed_endmembers, mixed_abundances, hdf_mixing_group