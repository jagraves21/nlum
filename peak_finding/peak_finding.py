import numpy as np

from scipy import ndimage

from nlum.utils import time_function

@time_function("Peak Finding")
def find_peaks(data, seperation=10):
    patch_size = 2 * seperation + 1
    max_data = np.zeros(data.shape)
    ndimage.maximum_filter(data, size=patch_size, output=max_data, mode="constant")

    idx = data == max_data
    max_data.fill(0.)
    max_data[idx] = data[idx]
    
    peaks = np.nonzero(max_data)
    
    peaks = np.asarray([
        (row,col) 
        for row,col in zip(*peaks)
    ])
    
    return peaks