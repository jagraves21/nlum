from nlum.utils import time_function

import numpy as np

import matplotlib as mpl
from matplotlib import pyplot as plt

from scipy import ndimage

from sklearn import model_selection
from sklearn import neighbors

def build_kde_model(points, cv=5, n_jobs=None):
    points = np.reshape(points, (-1,1))
    spread = np.max(points) - np.min(points)

    tuned_parameters = {
        "kernel":["gaussian"],
        "bandwidth":np.linspace(spread/100., spread/5., 100)
    }

    grid = model_selection.GridSearchCV(neighbors.kde.KernelDensity(), tuned_parameters, cv=cv, n_jobs=n_jobs)

    @time_function("KDE Estimation Time")
    def fit():
        grid.fit(points)
    fit()
    
    kde = grid.best_estimator_
    print "Best Bandwidth:", kde.bandwidth
    
    return kde