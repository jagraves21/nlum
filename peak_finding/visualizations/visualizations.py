import numpy as np

import matplotlib as mpl
from matplotlib import pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib import animation

from nlum.peak_finding.utils import build_kde_model

def display_kde_model(points, kde, figsize=(5,5), title_kwargs=None, xlabel_kwargs=None, ylabel_kwargs=None, ax=None):
    points = np.reshape(points, (-1,1))
    spread = np.max(points) - np.min(points)
    
    vmin = -0.1 * spread + points.min() 
    vmax = 0.1 * spread + points.max() 
    xx = np.linspace(vmin,vmax,1000)[:, np.newaxis]
    yy = np.exp( kde.score_samples(xx) )
    xx = np.reshape(xx, yy.shape)

    no_ax = ax is None
    if no_ax:
        fig, ax = plt.subplots(figsize=figsize)
    
    if title_kwargs is not None:
        ax.set_title(**title_kwargs)
    if xlabel_kwargs is not None:
        ax.set_xlabel(**xlabel_kwargs)
    if ylabel_kwargs is not None:
        ax.set_ylabel(**ylabel_kwargs)
    
    ax.plot(xx, yy, c="r")
    ax.plot(points[:, 0], -1. * (yy.max()-yy.min())/10. * np.random.random(len(points)), '+k')
    
    bins = int(np.rint( (points.max()-points.min())/kde.bandwidth ))
    ax.hist(points[:, 0], color="g", bins=bins, density=True)
    
    if no_ax:
        plt.show()

def display_frames(data, n_frames=8, n_columns=4, cmap=mpl.cm.viridis, figsize=(3,3)):
    qq,rr = divmod(len(data), n_frames)
    frames = xrange(0, len(data)-rr, qq)

    n_rows = int(np.ceil( len(frames)/float(n_columns) ) )
    
    figsize = (n_columns*figsize[0], n_rows*figsize[1])

    fig = plt.figure(figsize=figsize)
    gs = mpl.gridspec.GridSpec(n_rows, n_columns)
    for index,frame in enumerate(frames):
        row,col = divmod(index, n_columns)
        frame_image = data[frame,:,:]
        vmin, vmax = np.min([0., frame_image.min()]), frame_image.max()
        
        ax = fig.add_subplot(gs[row,col])
        ax.set_title("Frame {}".format(frame))
        ax.set_xticks([])
        ax.set_yticks([])
        ax.imshow(frame_image, vmin=vmin, vmax=vmax, cmap=cmap)
    plt.tight_layout()
    plt.show()

def display_stacked_frames(frames, cmap=mpl.cm.viridis, figsize=(5,5), title_kwargs=None):
    vmin, vmax = np.min([0., frames.min()]), frames.max()
    
    fig, ax = plt.subplots(figsize=figsize)
    
    if title_kwargs is not None:
        ax.set_title(**title_kwargs)
    ax.set_xticks([])
    ax.set_yticks([])
        
    im = ax.imshow(frames[0,:,:], cmap=cmap, vmin=vmin, vmax=vmax)
    def init():
        im.set_data(np.zeros(frames[0].shape))
        return im,
    def animate(frame):
        if frame % 10 == 0:
            print frame,
        frame_image = frames[frame,:,:]
        im.set_data(frame_image)
        im.set_clim(frame_image.min(), frame_image.max())
        return im,
    anim = mpl.animation.FuncAnimation(fig, animate, init_func=init, frames=len(frames), interval=50, blit=True)
    return anim

def display_peak_locations(frame, peak_locations, vmin=None, vmax=None, cmap=mpl.cm.viridis, figsize=(10,10), ax=None):
    no_ax = ax is None
    if no_ax:
        fig,ax = plt.subplots(figsize=figsize)
    
    ax.set_xticks([])
    ax.set_yticks([])
    
    ax.imshow(frame, vmin=vmin, vmax=vmax, cmap=cmap)
    xlim, ylim = ax.get_xlim(), ax.get_ylim()
    ax.scatter(peak_locations[:,1], peak_locations[:,0], marker=".", c="r")
    ax.set_xlim(xlim)
    ax.set_ylim(ylim)
    
    if no_ax:
        plt.show()
        
def display_patch_and_signal(patch, title=None, vmin=None, vmax=None, cmap=mpl.cm.viridis, figsize=(16,4)):
    fig = plt.figure(figsize=figsize)
    gs = mpl.gridspec.GridSpec(1, 2, width_ratios=[3, 1])
    
    if title:
        fig.suptitle(title)
    
    ax = fig.add_subplot(gs[0,0])
    if vmin is not None and vmax is not None:
        ax.set_ylim(vmin, vmax)
    
    ax.plot( np.reshape(patch, (-1,)) )
    
    ax = fig.add_subplot(gs[0,1])
    ax.set_axis_off()
    
    im = ax.imshow(patch, vmin=vmin, vmax=vmax, cmap=cmap)
    
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.05)
    plt.colorbar(im, cax=cax)
    
    plt.show()