import enum
import operator
import itertools
import numpy as np

def possible_metric(items, metric):
    values = np.asarray( [metric(item,item) for item in items] )
    res = np.max( values[np.isfinite(values)] )
    return np.isclose(res, 0.0, atol=1e-07)

def match_best(distances, is_metric=True):
    distances = distances.astype(float)
    if is_metric:
        distances[np.isnan(distances)] = np.inf
        ff = np.argmin
    else:
        distances[np.isnan(distances)] = 0.0
        ff = np.argmax
    matches = [ff(distances[ii,:]) for ii in xrange(distances.shape[0])]
    return matches

def match_greedy(distances, is_metric=True):
    distances = distances.astype(float)
    mean = np.nanmean(distances)
    if is_metric:
        distances[np.isnan(distances)] = np.inf
        fun = np.nanargmin
        compare = operator.lt
    else:
        distances[np.isnan(distances)] = 0.0
        fun = np.nanargmax
        compare = operator.gt
    
    matches = [-1] * distances.shape[0]
    n_iter = np.min(distances.shape)
    for ii in xrange(n_iter):
        loc = np.unravel_index(fun(distances), distances.shape)
        if compare(distances[loc], mean):
            matches[loc[0]] = loc[1]
        distances[loc[0],:] = np.nan
        distances[:,loc[1]] = np.nan
    
    return matches

def match_optimal(distances, is_metric=True):
    distances = distances.astype(float)
    if is_metric:
        distances[np.isnan(distances)] = np.inf
        fun = np.nanargmin
        compare = operator.lt
    else:
        distances[np.isnan(distances)] = 0.0
        fun = np.nanargmax
        compare = operator.gt
    
    transpose = False
    if distances.shape[0] > distances.shape[1]:
        transpose = True
        distances = distances.transpose()
    
    permutations = itertools.permutations(range(distances.shape[1]), distances.shape[0])
    best_perm = permutations.next()
    best_dist = np.sum(distances[range(distances.shape[0]),best_perm])
    for perm in permutations:
        tmp_dist = np.sum(distances[range(distances.shape[0]),perm])
        if compare(tmp_dist,best_dist):
            best_perm = perm
            best_dist = tmp_dist
    
    if transpose:
        tmp_perm = [-1] * distances.shape[1]
        for index,loc in enumerate(best_perm):
            tmp_perm[loc] = index
        best_perm = tmp_perm
        distances = distances.transpose()
    else:
        best_perm = list(best_perm)
    
    mean = np.nanmean(distances)
    for src,dst in enumerate(best_perm):
        if compare(mean, distances[src,dst]):
            best_perm[src] = -1
    
    return best_perm