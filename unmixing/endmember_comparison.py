import numpy as np

from scipy.spatial.distance import braycurtis as bray_curtis_distance
from scipy.spatial.distance import canberra as canberra_distance
from scipy.spatial.distance import chebyshev as chebyshev_distance
from scipy.spatial.distance import cityblock as manhattan_distance
from scipy.spatial.distance import cosine as cosine_distance
from scipy.spatial.distance import euclidean as euclidean_distance
from scipy.spatial.distance import jensenshannon as jensen_shannon_distance
from scipy.spatial.distance import sqeuclidean as squared_euclidean_distance

def normalized_squared_euclidean_distance(u, v, w=None):
    # 1/2*Norm[(u-Mean[u])-(v-Mean[v])]^2/(Norm[u-Mean[u]]^2+Norm[v-Mean[v]]^2)
    numerator = np.square( np.linalg.norm((u-np.mean(u))-(v-np.mean(v))) )
    denominator = np.square( np.linalg.norm(u-np.mean(u))) + np.square(np.linalg.norm(v-np.mean(v)))
    return 1/2. * numerator / denominator

def cosine_similarity(u, v, w=None):
    return 1 - cosine_distance(u, v, w)

def angular_distance(u, v, w=None):
    return np.arccos( cosine_similarity(u, v, w) ) / np.pi
    
def angular_similarity(u, v, w=None):
    return 1 - angular_distance(u, v, w)

def spectral_angle_mapper(u, v, w=None):
    return np.arccos( cosine_similarity(u, v, w) )

def spectral_information_divergence(u, v, w=None):
    def kl_divergence(u, v):
        from scipy.stats import entropy
        return entropy(u, v)

    u1 = u + 1e-6
    u1 = u1 / np.sum(u1)
    
    v1 = v + 1e-6
    v1 = v1 / np.sum(v1)
    return kl_divergence(u1, v1) + kl_divergence(v1, u1)

def cross_correlation(u, v, w=None):
    from scipy.spatial.distance import correlation
    return np.correlate(u,v)[0]
    
def normalized_cross_correlation(u, v, w=None):
    return cross_correlation(u, v) / np.sqrt( cross_correlation(u, u) * cross_correlation(v,v) )

DISTANCE_FUNCTIONS = {
    "angular":angular_distance,
    "bray_curtis":bray_curtis_distance,
    "canberra":canberra_distance,
    "chebyshev":chebyshev_distance,
    "cosine":cosine_distance,
    "euclidean":euclidean_distance,
    "jensen_shannon":jensen_shannon_distance,
    "manhattan":manhattan_distance,
    "normalized_squared_euclidean":normalized_squared_euclidean_distance,
    "spectral_angle_mapper":spectral_angle_mapper,
    "spectral_information_divergence":spectral_information_divergence,
    "squared_euclidean":squared_euclidean_distance
}

SIMILARITY_FUNCTIONS = {
    "angular":angular_similarity,
    "cosine":cosine_similarity
}