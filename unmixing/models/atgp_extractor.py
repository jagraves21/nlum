import argparse

import pysptools.eea

from nlum.utils import time_function

from .base_extractors import run_extractor
from .base_extractors import EndmembersExtractor

class ATGPExtractor(EndmembersExtractor):
    ALGORITHM = "ATGP"

    def __init__(self, profile=False, **kwargs):
        super(ATGPExtractor, self).__init__(profile)
        
    @time_function("endmember extraction time:")
    def extract_endmembers_2d(self, hsi_2d, n_endmembers, **kwargs):
        return pysptools.eea.eea.ATGP(hsi_2d, n_endmembers, **kwargs)

    @time_function("endmember extraction time:")
    def extract_endmembers_3d(self, hsi_3d, n_endmembers, **kwargs):
        return pysptools.eea.ATGP().extract(hsi_3d, n_endmembers, **kwargs)

def main(in_filename, out_filename, atgp_args, profile):
    extractor = ATGPExtractor(profile=profile, **atgp_args)
    run_extractor(in_filename,
            out_filename, extractor, atgp_args["n_components"])

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
            description="Automatic Target Generation Process (ATGP)",
            formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument("--atgp-n-components",
            type=int, default=2, metavar="int",
            help="number of sparse atoms to extract")

    parser.add_argument("in_filename", type=str, metavar="infile")
    parser.add_argument("out_filename", type=str, metavar="outfile")

    parser.add_argument("--profile",
            action="store_true",
            help="profile program execution")

    arguments = vars(parser.parse_args())

    args = {}
    for key,value in arguments.iteritems():
        if key.startswith("atgp_"):
            args.setdefault("atgp_args", {})[key[len("atgp_"):]] = value
        else:
            args[key] = value

    main(**args)
