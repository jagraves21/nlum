import numpy as np

from pysptools.abundance_maps.amaps import FCLS
from pysptools.abundance_maps.amaps import NNLS

from nlum.utils import time_function, profile_code

class ComponentExtractor(object):
    ALGORITHM = None

    def __init__(self):
        pass
    
    def get_components_2d(self, hsi_2d, n_components, **kwargs):
        raise NotImplementedError()

    def get_components_3d(self, hsi_3d, n_components, **kwargs):
        hsi_2d = np.reshape(hsi_3d, (-1, hsi_3d.shape[2]))
        endmembers, abundance_maps = self.get_components_2d(hsi_2d, n_components, **kwargs)
        abundance_maps = np.reshape(abundance_maps, abundance_maps.shape[:1]+hsi_3d.shape[:-1] )
        return endmembers, abundance_maps
        
    def get_components(self, data, n_components, **kwargs):
        if len(data.shape) == 3:
            return self.get_components_3d(data, n_components, **kwargs)
        else:
            return self.get_components_2d(data, n_components, **kwargs)

    @staticmethod
    @time_function(msg="endmembers estimation time:")
    def estimate_endmembers_2d(hsi_2d, abundance_maps_2d):
        abundance_maps_2d = np.moveaxis(abundance_maps_2d, 0, 1)
        endmembers = NNLS(hsi_2d.transpose(), abundance_maps_2d.transpose())
        endmembers = endmembers.transpose()
        return endmembers
    
    @staticmethod
    @time_function(msg="endmembers estimation time:")
    def estimate_endmembers_3d(hsi_3d, abundance_maps_3d):
        hsi_2d = np.reshape(hsi_3d, (-1, hsi_3d.shape[2]) )
        abundance_maps_2d = np.reshape(abundance_maps_3d, (abundance_maps_3d.shape[0],-1))
        return ComponentExtractor.estimate_endmembers_2d(hsi_2d, abundance_maps_2d)
    
    @staticmethod
    def estimate_abundance_maps_2d(hsi_2d, endmembers):
        abundance_maps = NNLS(hsi_2d, endmembers)
        abundance_maps = np.moveaxis(abundance_maps, 1, 0)
        return abundance_maps

    @staticmethod
    @time_function(msg="abundance maps estimation time:")
    def estimate_abundance_maps_3d(hsi_3d, endmembers):
        hsi_2d = np.reshape(hsi_3d, (-1, hsi_3d.shape[2]) )
        abundance_maps = ComponentExtractor.estimate_abundance_maps_2d(hsi_2d, endmembers)
        abundance_maps = np.reshape(abundance_maps, (abundance_maps.shape[0], hsi_3d.shape[0], hsi_3d.shape[1]))
        return abundance_maps

class EndmembersExtractor(ComponentExtractor):
    def __init__(self, profile=False):
        self.profile = profile
        super(EndmembersExtractor, self).__init__()
    
    def extract_endmembers_2d(self, hsi_2d, n_endmembers, **kwargs):
        raise NotImplementedError()

    def extract_endmembers_3d(self, hsi_3d, n_endmembers, **kwargs):
        hsi_2d = np.reshape(hsi_3d, (-1, hsi_3d.shape[2]))
        return self.extract_endmembers_2d(hsi_2d, n_endmembers, **kwargs)
        
    def get_components_2d(self, hsi_2d, n_components, **kwargs):
        profile_filename = "{}.{}.prof".format(self.ALGORITHM.lower(), n_components)
        endmembers = profile_code(self.profile, profile_filename)(self.extract_endmembers_2d)(hsi_2d, n_components, **kwargs)
        #endmembers = self.extract_endmembers_2d(hsi_2d, n_components)
        abundance_maps = self.estimate_abundance_maps_2d(hsi_2d, endmembers)
        return endmembers, abundance_maps

    def get_components_3d(self, hsi_3d, n_components, **kwargs):
        profile_filename = "{}.{}.prof".format(self.ALGORITHM.lower(), n_components)
        endmembers = profile_code(self.profile, profile_filename)(self.extract_endmembers_3d)(hsi_3d, n_components, **kwargs)
        #endmembers = self.extract_endmembers_3d(hsi_3d, n_components)
        abundance_maps = self.estimate_abundance_maps_3d(hsi_3d, endmembers)
        return endmembers, abundance_maps

class AbundanceMapsExtractor(ComponentExtractor):
    def __init__(self, profile=False):
        self.profile = profile
        super(AbundanceMapsExtractor, self).__init__()

    def extract_abundance_maps_2d(self, hsi_2d, n_abundance_maps, **kwargs):
        raise NotImplementedError()

    def extract_abundance_maps_3d(self, hsi_3d, n_endmembers, **kwargs):
        hsi_2d = np.reshape(hsi_3d, (-1, hsi_3d.shape[2]))
        abundance_maps = self.extract_abundance_maps_2d(hsi_2d, n_endmembers, **kwargs)
        abundance_maps = np.reshape(abundance_maps, (abundance_maps.shape[0],)+hsi_3d.shape[:-1])
        return abundance_maps
        
    def get_components_2d(self, hsi_2d, n_components, **kwargs):
        profile_filename = "{}.{}.prof".format(self.ALGORITHM.lower(), n_components)
        abundance_maps = profile_code(self.profile, profile_filename)(self.extract_abundance_maps_2d)(hsi_2d, n_components)
        #abundance_maps = self.extract_abundance_maps_2d(hsi_2d, n_components)
        endmembers = self.estimate_endmembers_2d(hsi_2d, abundance_maps)
        return endmembers, abundance_maps

    def get_components_3d(self, hsi_3d, n_components, **kwargs):
        profile_filename = "{}.{}.prof".format(self.ALGORITHM.lower(), n_components)
        abundance_maps = profile_code(self.profile, profile_filename)(self.extract_abundance_maps_3d)(hsi_3d, n_components)
        #abundance_maps = self.extract_abundance_maps_3d(hsi_3d, n_components)
        endmembers = self.estimate_endmembers_3d(hsi_3d, abundance_maps)
        return endmembers, abundance_maps

def run_extractor(in_filename, out_filename, extractor, n_components, **kwargs):
    with np.load(in_filename) as npzfile:
        data = npzfile["data"]
    
    endmembers, abundance_maps = extractor.get_components(data, n_components, **kwargs)
    if out_filename:
        np.savez(out_filename, abundance_maps=abundance_maps, endmembers=endmembers)