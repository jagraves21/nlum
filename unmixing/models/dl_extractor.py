import argparse
from sklearn import decomposition

from .base_extractors import run_extractor
from .sklearn_base_extractors import SKLearnJointExtractor

class DLExtractor(SKLearnJointExtractor):
    ALGORITHM = "DL"

    def __init__(self, profile=False, **kwargs):
        self.model = decomposition.DictionaryLearning(**kwargs)
        super(DLExtractor, self).__init__(profile)

def main(in_filename, out_filename, dl_args, profile):
    extractor = DLExtractor(profile=profile, **dl_args)
    run_extractor(in_filename,
            out_filename, extractor, dl_args["n_components"])

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
            description="Dictionary Learning",
            formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument("--dl-n-components",
            type=int, default=2, metavar="int",
            help="number of dictionary elements to extract")
    parser.add_argument("--dl-alpha",
            type=float, default=1.0, metavar="float",
            help="sparsity controlling parameter")
    parser.add_argument("--dl-max-iter",
            type=int, default=1000, metavar="int",
            help="maximum number of iterations to preform")
    parser.add_argument("--dl-tol",
            type=float, default=1e-08, metavar="float",
            help="tolerance for numerical error")
    parser.add_argument("--dl-fit-algorithm",
            type=str, default="lars", metavar="str",
            choices=["lars", "cd"],
            help="choices: {0}".format(["lars", "cd"]))
    parser.add_argument("--dl-transform-algorithm",
            type=str, default="lars", metavar="str",
            choices=["lasso_lars", "lasso_cd", "lars", "omp", "threshold"],
            help="choices: {0}".format(["lasso_lars", "lasso_cd", "lars", "omp", "threshold"]))
    parser.add_argument("--dl-n-jobs",
            type=int, default=1, metavar="int",
            help="number of parallel jobs; if -1, then the number of jobs is set to the number of cores")
    parser.add_argument("--dl-verbose",
            type=int, default=0, metavar="int",
            help="level of verbosity")
    parser.add_argument("--dl-split-sign",
            action="store_true",
            help="split the sparse feature vector into the concatenation of its negative part and its positive part")
    parser.add_argument("--dl-random-state",
            type=int, default=None, metavar="int",
            help="seed used by the random number generator")

    parser.add_argument("in_filename", type=str, metavar="infile")
    parser.add_argument("out_filename", type=str, metavar="outfile")

    parser.add_argument("--profile",
            action="store_true",
            help="profile program execution")

    arguments = vars(parser.parse_args())

    args = {}
    for key,value in arguments.iteritems():
        if key.startswith("dl_"):
            args.setdefault("dl_args", {})[key[len("dl_"):]] = value
        else:
            args[key] = value

    main(**args)
