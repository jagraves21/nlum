import argparse
from sklearn import decomposition

from .base_extractors import run_extractor
from .sklearn_base_extractors import SKLearnJointExtractor

class FAExtractor(SKLearnJointExtractor):
    ALGORITHM = "FA"

    def __init__(self, profile=False, **kwargs):
        self.model = decomposition.FactorAnalysis(**kwargs)
        super(FAExtractor, self).__init__(profile)

def main(in_filename, out_filename, fa_args, profile):
    extractor = FAExtractor(profile=profile, **fa_args)
    run_extractor(in_filename,
            out_filename, extractor, fa_args["n_components"])

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
            description="Factor Analysis",
            formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument("--fa-n-components",
            type=int, default=2, metavar="int",
            help="dimensionality of latent space")
    parser.add_argument("--fa-tol",
            type=float, default=0.01, metavar="float",
            help="tolerance for numerical error")
    parser.add_argument("--fa-max-iter",
            type=int, default=1000, metavar="int",
            help="maximum number of iterations")
    parser.add_argument("--fa-svd-method",
            type=str, default="randomized", metavar="str",
            choices=["lapack", "randomized"],
            help="choices: {0}".format(["lapack", "randomized"]))
    parser.add_argument("--fa-iterated-power",
            type=int, default=3, metavar="int",
            help="number of iterations of the power method, if svd method is 'randomized'")
    parser.add_argument("--fa-random-state",
            type=int, default=None, metavar="int",
            help="seed used by the random number generator")

    parser.add_argument("in_filename", type=str, metavar="infile")
    parser.add_argument("out_filename", type=str, metavar="outfile")

    parser.add_argument("--profile",
            action="store_true",
            help="profile program execution")

    arguments = vars(parser.parse_args())

    args = {}
    for key,value in arguments.iteritems():
        if key.startswith("fa_"):
            args.setdefault("fa_args", {})[key[len("fa_"):]] = value
        else:
            args[key] = value

    main(**args)
