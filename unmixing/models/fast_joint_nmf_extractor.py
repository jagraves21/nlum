import argparse
from sklearn import decomposition

from nlum.algorithms import FastJointNMF
from .base_extractors import run_extractor
from .sklearn_base_extractors import SKLearnJointExtractor

class FastJointNMFExtractor(SKLearnJointExtractor):
    ALGORITHM = "Fast-Joint-NMF"

    def __init__(self, profile=False, **kwargs):
        self.model = FastJointNMF(**kwargs)
        super(FastJointNMFExtractor, self).__init__(profile)

def main(in_filename, out_filename, fast_joint_nmf_args, profile):
    extractor = FastJointNMFExtractor(profile=profile, **fast_joint_nmf_args)
    run_extractor(in_filename,
            out_filename, extractor, fast_joint_nmf_args["n_components"])

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
            description="Fast Joint-NMF",
            formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument("--fast-joint-nmf-n-components",
            type=int, default=2, metavar="int",
            help="number of dictionary elements to extract")
    parser.add_argument("--fast-joint-nmf-max-iter",
            type=int, default=10, metavar="int",
            help="maximum number of iterations to preform")
    parser.add_argument("--fast-joint-nmf-alpha",
            type=float, default=1e-3, metavar="float",
            help="???")
    parser.add_argument("--fast-joint-nmf-beta",
            type=float, default=1.0, metavar="float",
            help="???")
    parser.add_argument("--fast-joint-nmf-reg-lambda",
            type=float, default=1e-3, metavar="float",
            help="???")

    parser.add_argument("in_filename", type=str, metavar="infile")
    parser.add_argument("out_filename", type=str, metavar="outfile")

    parser.add_argument("--profile",
            action="store_true",
            help="profile program execution")

    arguments = vars(parser.parse_args())

    args = {}
    for key,value in arguments.iteritems():
        if key.startswith("fast_joint_nmf_"):
            args.setdefault("fast_joint_nmf_args", {})[key[len("fast_joint_nmf_"):]] = value
        else:
            args[key] = value

    main(**args)
