import argparse
from sklearn import decomposition

from nlum.algorithms import GraphRegularizedNMF 
from .base_extractors import run_extractor
from .sklearn_base_extractors import SKLearnJointExtractor

class GRNMFExtractor(SKLearnJointExtractor):
    ALGORITHM = "GR-NMF"

    def __init__(self, profile=False, **kwargs):
        self.model = GraphRegularizedNMF(**kwargs)
        super(GRNMFExtractor, self).__init__(profile)

def main(in_filename, out_filename, gr_nmf_args, profile):
    extractor = GRNMFExtractor(profile=profile, **gr_nmf_args)
    run_extractor(in_filename,
            out_filename, extractor, gr_nmf_args["n_components"])

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
            description="GR-NMF",
            formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument("--gr-nmf-n-components",
            type=int, default=2, metavar="int",
            help="number of dictionary elements to extract")
    parser.add_argument("--gr-nmf-max-iter",
            type=int, default=10, metavar="int",
            help="maximum number of iterations to preform")
    parser.add_argument("--gr-nmf-lambda-reg",
            type=float, default=0.1, metavar="float",
            help="???")
    parser.add_argument("--gr-nmf-alpha-reg",
            type=float, default=1.0, metavar="float",
            help="???")

    parser.add_argument("in_filename", type=str, metavar="infile")
    parser.add_argument("out_filename", type=str, metavar="outfile")

    parser.add_argument("--profile",
            action="store_true",
            help="profile program execution")

    arguments = vars(parser.parse_args())

    args = {}
    for key,value in arguments.iteritems():
        if key.startswith("gr_nmf_"):
            args.setdefault("gr_nmf_args", {})[key[len("gr_nmf_"):]] = value
        else:
            args[key] = value

    main(**args)
