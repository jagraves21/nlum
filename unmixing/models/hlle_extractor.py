import argparse
import warnings
from sklearn import manifold

from .base_extractors import run_extractor
from .sklearn_base_extractors import SKLearnAMExtractor

class HLLEExtractor(SKLearnAMExtractor):
    ALGORITHM = "HLLE"

    def __init__(self, profile=False, **kwargs):
        kwargs["method"] = "hessian"
        kwargs["eigen_solver"] = "dense"
        self.model = manifold.LocallyLinearEmbedding(**kwargs)
        super(HLLEExtractor, self).__init__(profile)

def main(in_filename, out_filename, hlle_args, profile):
    val = hlle_args["n_components"] * (hlle_args["n_components"] + 3) / 2
    if hlle_args["n_neighbors"] <= val:
        message = "n_neighbors ({}) must be greater than [n_components * (n_components + 3) / 2] ({})"
        message = message.format(hlle_args["n_neighbors"], val)
        message += ", setting n_neighbors={}".format(val+1)
        warnings.warn(message, UserWarning)
        hlle_args["n_neighbors"] = val + 1

    extractor = HLLEExtractor(profile=profile, **hlle_args)

    run_extractor(in_filename,
            out_filename, extractor, hlle_args["n_components"])

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Hessian-Based Locally Linear Embeding",
            formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument("--hlle-n-neighbors",
            type=int, default=6, metavar="int",
            help="number of neighbors to consider for each point")
    parser.add_argument("--hlle-n-components",
            type=int, default=2, metavar="int",
            help="number of coordinates for the manifold")
    parser.add_argument("--hlle-reg",
            type=float, default=0.001, metavar="float",
            help="regularization constant, multiplies the trace of the local covariance matrix of the distances")
    parser.add_argument("--hlle-eigen-solver",
            type=str, default="auto", metavar="str",
            choices=["auto", "arpack", "dense"],
            help="choices: {0}".format(["auto", "arpack", "dense"]))
    parser.add_argument("--hlle-tol",
            type=float, default=0.0, metavar="float",
            help="convergence tolerance passed to arpack or lobpcg; not used if eigen_solver == 'dense'")
    parser.add_argument("--hlle-max-iter",
            type=int, default=None, metavar="int",
            help="maximum number of iterations for the arpack solver; not used if eigen_solver == 'dense'")
    parser.add_argument("--hlle-hessian-tol",
            type=float, default=0.001, metavar="float",
            help="tolerance for hessian eigenmapping method; only used if method == 'hessian'")
    parser.add_argument("--hlle-neighbors-algorithm",
            type=str, default="auto", metavar="str",
            choices=["auto", "brute", "kd_tree", "ball_tree"],
            help="algorithm to use for nearest neighbors search - choices {0}".format(["auto", "brute", "kd_tree", "ball_tree"]))
    parser.add_argument("--hlle-random-state",
            type=int, default=None, metavar="int",
            help="seed used by the random number generator")
    parser.add_argument("--hlle-n-jobs",
            type=int, default=1, metavar="int",
            help="number of parallel jobs; if -1, then the number of jobs is set to the number of cores")

    parser.add_argument("in_filename", type=str, metavar="infile")
    parser.add_argument("out_filename", type=str, metavar="outfile")

    parser.add_argument("--profile",
            action="store_true",
            help="profile program execution")

    arguments = vars(parser.parse_args())

    args = {}
    for key,value in arguments.iteritems():
        if key.startswith("hlle_"):
            args.setdefault("hlle_args", {})[key[len("hlle_"):]] = value
        else:
            args[key] = value

    main(**args)
