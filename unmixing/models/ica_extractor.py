import argparse
from sklearn import decomposition

from .base_extractors import run_extractor
from .sklearn_base_extractors import SKLearnJointExtractor

class ICAExtractor(SKLearnJointExtractor):
    ALGORITHM = "ICA"

    def __init__(self, profile=False, **kwargs):
        self.model = decomposition.FastICA(**kwargs)
        super(ICAExtractor, self).__init__(profile)

def main(in_filename, out_filename, ica_args, profile):
    extractor = ICAExtractor(profile=profile, **ica_args)
    run_extractor(in_filename,
            out_filename, extractor, ica_args["n_components"])

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
            description="Independent Component Analysis",
            formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument("--ica-n-components",
            type=int, default=2, metavar="int",
            help="number of components to use")
    parser.add_argument("--ica-algorithm",
            type=str, default="parallel", metavar="str",
            choices=["parallel, deflation"],
            help="choices: {0}".format(["parallel, deflation"]))
    parser.add_argument("--ica-nowhiten",
            dest="ica_whiten",
            action="store_false",
            default=True,
            help="perform data whitening")
    parser.add_argument("--ica-fun",
            type=str, default="logcosh", metavar="str",
            choices=["logcosh", "exp", "cube"],
            help="choices: {0}".format(["logcosh", "exp", "cube"]))
    parser.add_argument("--ica-max-iter",
            type=int, default=1000, metavar="int",
            help="maximum number of iterations during fit")
    parser.add_argument("--ica-tol",
            type=float, default=0.0001, metavar="float",
            help="tolerance for numerical error")
    parser.add_argument("--ica-random-state",
            type=int, default=None, metavar="int",
            help="seed used by the random number generator")

    parser.add_argument("in_filename", type=str, metavar="infile")
    parser.add_argument("out_filename", type=str, metavar="outfile")

    parser.add_argument("--profile",
            action="store_true",
            help="profile program execution")

    arguments = vars(parser.parse_args())

    args = {}
    for key,value in arguments.iteritems():
        if key.startswith("ica_"):
            args.setdefault("ica_args", {})[key[len("ica_"):]] = value
        else:
            args[key] = value

    main(**args)
