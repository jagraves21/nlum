import argparse
from sklearn import decomposition

from nlum.algorithms import JointNMF
from .base_extractors import run_extractor
from .sklearn_base_extractors import SKLearnJointExtractor

class JointNMFExtractor(SKLearnJointExtractor):
    ALGORITHM = "Joint-NMF"

    def __init__(self, profile=False, **kwargs):
        self.model = JointNMF(**kwargs)
        super(JointNMFExtractor, self).__init__(profile)

def main(in_filename, out_filename, joint_nmf_args, profile):
    extractor = JointNMFExtractor(profile=profile, **joint_nmf_args)
    run_extractor(in_filename,
            out_filename, extractor, joint_nmf_args["n_components"])

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
            description="Joint-NMF",
            formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument("--joint-nmf-n-components",
            type=int, default=2, metavar="int",
            help="number of dictionary elements to extract")
    parser.add_argument("--joint-nmf-max-iter",
            type=int, default=10, metavar="int",
            help="maximum number of iterations to preform")
    parser.add_argument("--joint-nmf-alpha",
            type=float, default=1e-3, metavar="float",
            help="???")
    parser.add_argument("--joint-nmf-beta",
            type=float, default=1.0, metavar="float",
            help="???")
    parser.add_argument("--joint-nmf-reg-lambda",
            type=float, default=1e-3, metavar="float",
            help="???")

    parser.add_argument("in_filename", type=str, metavar="infile")
    parser.add_argument("out_filename", type=str, metavar="outfile")

    parser.add_argument("--profile",
            action="store_true",
            help="profile program execution")

    arguments = vars(parser.parse_args())

    args = {}
    for key,value in arguments.iteritems():
        if key.startswith("joint_nmf_"):
            args.setdefault("joint_nmf_args", {})[key[len("joint_nmf_"):]] = value
        else:
            args[key] = value

    main(**args)
