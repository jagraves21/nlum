import argparse
from sklearn import decomposition

from .base_extractors import run_extractor
from .sklearn_base_extractors import SKLearnAMExtractor

class KPCAExtractor(SKLearnAMExtractor):
    ALGORITHM = "KPCA"

    def __init__(self, profile=False, **kwargs):
        #kwargs["method"] = "exact"
        self.model = decomposition.KernelPCA(**kwargs)
        super(KPCAExtractor, self).__init__(profile)

def main(in_filename, out_filename, pca_args, profile):
    extractor = PCAExtractor(profile=profile, **pca_args)
    run_extractor(in_filename,
            out_filename, extractor, pca_args["n_components"])

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
            description="Principal Component Analysis",
            formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument("--pca-n-components",
            type=int, default=2, metavar="int",
            help="number of components to keep")
    parser.add_argument("--pca-whiten",
            action="store_true",
            help="whiten the component vectors before returning")
    parser.add_argument("--pca-svd-solver",
            type=str, default="auto", metavar="str",
            choices=["auto", "full", "arpack", "randomized"],
            help="choices: {0}".format(["auto", "full", "arpack", "randomized"]))
    parser.add_argument("--pca-tol",
            type=float, default=0.0, metavar="float",
            help="tolerance for singular values computed when svd_solver is 'arpack'")
    parser.add_argument("--pca-random-state",
            type=int, default=None, metavar="int",
            help="seed used by the random number generator")

    parser.add_argument("in_filename", type=str, metavar="infile")
    parser.add_argument("out_filename", type=str, metavar="outfile")

    parser.add_argument("--profile",
            action="store_true",
            help="profile program execution")

    arguments = vars(parser.parse_args())

    args = {}
    for key,value in arguments.iteritems():
        if key.startswith("pca_"):
            args.setdefault("pca_args", {})[key[len("pca_"):]] = value
        else:
            args[key] = value

    main(**args)
