import argparse
from sklearn import decomposition

from .base_extractors import run_extractor
from .sklearn_base_extractors import SKLearnJointExtractor

class LDAExtractor(SKLearnJointExtractor):
    ALGORITHM = "LDA"

    def __init__(self, profile=False, **kwargs):
        self.model = decomposition.LatentDirichletAllocation(**kwargs)
        super(LDAExtractor, self).__init__(profile)

def main(in_filename, out_filename, lda_args, profile):
    extractor = LDAExtractor(profile=profile, **lda_args)
    run_extractor(in_filename,
            out_filename, extractor, lda_args["n_components"])

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
            description="Latent Dirichlet Allocation",
            formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument("--lda-n-components",
            type=int, default=2, metavar="int",
            help="number of topics")
    parser.add_argument("--lda-doc-topic-prior",
            type=float, default=None, metavar="float",
            help="prior of document topic distribution theta")
    parser.add_argument("--lda-topic-word-prior",
            type=float, default=None, metavar="float",
            help="prior of topic word distribution beta")
    parser.add_argument("--lda-learning-method",
            type=str, default="online", metavar="str",
            choices=["batch", "online"],
            help="choices: {0}".format(["batch", "online"]))
    parser.add_argument("--lda-learning-decay", 
            type=float, default=0.7, metavar="float",
            help="controls the learning rate in the online learning method")
    parser.add_argument("--lda-learning-offset", 
            type=float, default=10.0, metavar="float",
            help="positive parameter that downweights early iterations in online learning")
    parser.add_argument("--lda-max-iter",
            type=int, default=10, metavar="int",
            help="maximum number of iterations to preform")
    parser.add_argument("--lda-batch-size",
            type=int, default=128, metavar="int",
            help="number of document to use in each EM iteration in online learning")
    parser.add_argument("--lda-mean-change-tol",
            type=float, default=1e-3, metavar="float",
            help="stopping tolerance for updating document topic distribution in E-step")
    parser.add_argument("--lda-max-doc-update-iter",
            type=int, default=100, metavar="int",
            help="max number of iterations for updating document topic distribution in the E-step")
    parser.add_argument("--lda-n-jobs",
            type=int, default=1, metavar="int",
            help="number of parallel jobs; if -1, then the number of jobs is set to the number of cores")
    parser.add_argument("--lda-verbose",
            type=int, default=0, metavar="int",
            help="level of verbosity")
    parser.add_argument("--lda-random-state",
            type=int, default=None, metavar="int",
            help="seed used by the random number generator")

    parser.add_argument("in_filename", type=str, metavar="infile")
    parser.add_argument("out_filename", type=str, metavar="outfile")

    parser.add_argument("--profile",
            action="store_true",
            help="profile program execution")

    arguments = vars(parser.parse_args())

    args = {}
    for key,value in arguments.iteritems():
        if key.startswith("lda_"):
            args.setdefault("lda_args", {})[key[len("lda_"):]] = value
        else:
            args[key] = value

    main(**args)
