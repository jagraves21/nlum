import argparse
from sklearn import decomposition

from nlum.algorithms import LinearFUNNL
from .base_extractors import run_extractor
from .sklearn_base_extractors import SKLearnJointExtractor

class LinearFUNNLExtractor(SKLearnJointExtractor):
    ALGORITHM = "Linear-FUNNL"

    def __init__(self, profile=False, **kwargs):
        self.model = LinearFUNNL(**kwargs)
        super(LinearFUNNLExtractor, self).__init__(profile)

def main(in_filename, out_filename, linear_funnl_args, profile):
    extractor = LinearFUNNLExtractor(profile=profile, **linear_funnl_args)
    run_extractor(in_filename,
            out_filename, extractor, linear_funnl_args["n_components"])

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
            description="RFB FUNNL",
            formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument("--linear-funnl-n-components",
            type=int, default=2, metavar="int",
            help="number of dictionary elements to extract")
    parser.add_argument("--linear-funnl-max-iter",
            type=int, default=10, metavar="int",
            help="maximum number of iterations to preform")
    parser.add_argument("--linear-funnl-alpha",
            type=float, default=1e-3, metavar="float",
            help="???")
    parser.add_argument("--linear-funnl-beta",
            type=float, default=1.0, metavar="float",
            help="???")
    parser.add_argument("--linear-funnl-reg-lambda",
            type=float, default=1e-3, metavar="float",
            help="???")

    parser.add_argument("in_filename", type=str, metavar="infile")
    parser.add_argument("out_filename", type=str, metavar="outfile")

    parser.add_argument("--profile",
            action="store_true",
            help="profile program execution")

    arguments = vars(parser.parse_args())

    args = {}
    for key,value in arguments.iteritems():
        if key.startswith("linear_funnl_"):
            args.setdefault("linear_funnl_args", {})[key[len("linear_funnl_"):]] = value
        else:
            args[key] = value

    main(**args)
