import argparse
from sklearn import decomposition

from .base_extractors import run_extractor
from .sklearn_base_extractors import SKLearnAMExtractor

class LinearKPCAExtractor(SKLearnAMExtractor):
    ALGORITHM = "Linear-KPCA"
    
    def __init__(self, profile=False, **kwargs):
        kwargs["kernel"] = "linear"
        self.model = decomposition.KernelPCA(**kwargs)
        super(LinearKPCAExtractor, self).__init__(profile)

def main(in_filename, out_filename, linear_kpca_args, profile):
    extractor = LinearKPCAExtractor(profile=profile, **linear_kpca_args)
    run_extractor(in_filename,
            out_filename, extractor, linear_kpca_args["n_components"])

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
            description="Principal Component Analysis",
            formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument("--linear-kpca-n-components",
            type=int, default=None, metavar="int",
            help="number of components to keep")
    parser.add_argument("--linear-kpca-eigen-solver",
            type=str, default="auto", metavar="str",
            choices=["auto", "dense", "arpack"],
            help="choices: {0}".format(["auto", "dense", "arpack"]))
    parser.add_argument("--linear-kpca-tol",
            type=float, default=0.0, metavar="float",
            help="convergence tolerance for arpack; if 0, optimal value is chosen by arpack")
    parser.add_argument("--linear-kpca-max-iter",
            type=int, default=None, metavar="int",
            help="maximum number of iterations for arpack; if None optimal value is chosen by arpack")
    parser.add_argument("--linear-kpca-random-state",
            type=int, default=None, metavar="int",
            help="seed used by the random number generator")
    parser.add_argument("--linear-kpca-n-jobs",
            type=int, default=1, metavar="int",
            help="number of parallel jobs; if -1, then the number of jobs is set to the number of cores")

    parser.add_argument("in_filename", type=str, metavar="infile")
    parser.add_argument("out_filename", type=str, metavar="outfile")

    parser.add_argument("--profile",
            action="store_true",
            help="profile program execution")

    arguments = vars(parser.parse_args())

    args = {}
    for key,value in arguments.iteritems():
        if key.startswith("linear_kpca_"):
            args.setdefault("linear_kpca_args", {})[key[len("linear_kpca_"):]] = value
        else:
            args[key] = value

    main(**args)
