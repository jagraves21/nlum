import argparse
import warnings
from sklearn import manifold

from .base_extractors import run_extractor
from .sklearn_base_extractors import SKLearnAMExtractor

class LTSAExtractor(SKLearnAMExtractor):
    ALGORITHM = "LTSA"

    def __init__(self, profile=False, **kwargs):
        kwargs["method"] = "ltsa"
        self.model = manifold.LocallyLinearEmbedding(**kwargs)
        super(LTSAExtractor, self).__init__(profile)

def main(in_filename, out_filename, ltsa_args, profile):
    if ltsa_args["n_neighbors"] < ltsa_args["n_components"]:
        message = "n_neighbors ({}) must be greater than of equal to n_components"
        message = message.format(ltsa_args["n_neighbors"], ltsa_args["n_components"])
        message += ", setting n_neighbors={}".format(ltsa_args["n_components"])
        warnings.warn(message, UserWarning)
        ltsa_args["n_neighbors"] = ltsa_args["n_components"]
        
    extractor = LTSAExtractor(profile=profile, **ltsa_args)
    run_extractor(in_filename,
            out_filename, extractor, ltsa_args["n_components"])

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Locally Linear Embeding",
            formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument("--ltsa-n-neighbors",
            type=int, default=5, metavar="int",
            help="number of neighbors to consider for each point")
    parser.add_argument("--ltsa-n-components",
            type=int, default=2, metavar="int",
            help="number of coordinates for the manifold")
    parser.add_argument("--ltsa-reg",
            type=float, default=0.001, metavar="float",
            help="regularization constant, multiplies the trace of the local covariance matrix of the distances")
    parser.add_argument("--ltsa-eigen-solver",
            type=str, default="auto", metavar="str",
            choices=["auto", "arpack", "dense"],
            help="choices: {0}".format(["auto", "arpack", "dense"]))
    parser.add_argument("--ltsa-tol",
            type=float, default=0.0, metavar="float",
            help="convergence tolerance passed to arpack or lobpcg; not used if eigen_solver == 'dense'")
    parser.add_argument("--ltsa-max-iter",
            type=int, default=None, metavar="int",
            help="maximum number of iterations for the arpack solver; not used if eigen_solver == 'dense'")
    parser.add_argument("--ltsa-neighbors-algorithm",
            type=str, default="auto", metavar="str",
            choices=["auto", "brute", "kd_tree", "ball_tree"],
            help="algorithm to use for nearest neighbors search - choices {0}".format(["auto", "brute", "kd_tree", "ball_tree"]))
    parser.add_argument("--ltsa-random-state",
            type=int, default=None, metavar="int",
            help="seed used by the random number generator")
    parser.add_argument("--ltsa-n-jobs",
            type=int, default=1, metavar="int",
            help="number of parallel jobs; if -1, then the number of jobs is set to the number of cores")

    parser.add_argument("in_filename", type=str, metavar="infile")
    parser.add_argument("out_filename", type=str, metavar="outfile")

    parser.add_argument("--profile",
            action="store_true",
            help="profile program execution")

    arguments = vars(parser.parse_args())

    args = {}
    for key,value in arguments.iteritems():
        if key.startswith("ltsa_"):
            args.setdefault("ltsa_args", {})[key[len("ltsa_"):]] = value
        else:
            args[key] = value

    main(**args)
