import argparse
from sklearn import manifold

from .base_extractors import run_extractor
from .sklearn_base_extractors import SKLearnAMExtractor

class MLLEExtractor(SKLearnAMExtractor):
    ALGORITHM = "MLLE"

    def __init__(self, profile=False, **kwargs):
        kwargs["method"] = "modified"
        self.model = manifold.LocallyLinearEmbedding(**kwargs)
        super(MLLEExtractor, self).__init__(profile)

def main(in_filename, out_filename, mlle_args, profile):
    extractor = MLLEExtractor(profile=profile, **mlle_args)
    run_extractor(in_filename,
            out_filename, extractor, mlle_args["n_components"])

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Modified Locally Linear Embeding",
            formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument("--mlle-n-neighbors",
            type=int, default=5, metavar="int",
            help="number of dimensions in which to immerse the dissimilarities")
    parser.add_argument("--mlle-n-components",
            type=int, default=2, metavar="int",
            help="number of coordinates for the manifold")
    parser.add_argument("--mlle-reg",
            type=float, default=0.001, metavar="float",
            help="regularization constant, multiplies the trace of the local covariance matrix of the distances")
    parser.add_argument("--mlle-eigen-solver",
            type=str, default="auto", metavar="str",
            choices=["auto", "arpack", "dense"],
            help="choices: {0}".format(["auto", "arpack", "dense"]))
    parser.add_argument("--mlle-tol",
            type=float, default=0.0, metavar="float",
            help="convergence tolerance passed to arpack or lobpcg; not used if eigen_solver == 'dense'")
    parser.add_argument("--mlle-max-iter",
            type=int, default=None, metavar="int",
            help="maximum number of iterations for the arpack solver; not used if eigen_solver == 'dense'")
    parser.add_argument("--mlle-modified-tol",
            type=float, default=1e-12, metavar="float",
            help="tolerance for modified LLE method")
    parser.add_argument("--mlle-neighbors-algorithm",
            type=str, default="auto", metavar="str",
            choices=["auto", "brute", "kd_tree", "ball_tree"],
            help="algorithm to use for nearest neighbors search - choices {0}".format(["auto", "brute", "kd_tree", "ball_tree"]))
    parser.add_argument("--mlle-random-state",
            type=int, default=None, metavar="int",
            help="seed used by the random number generator")
    parser.add_argument("--mlle-n-jobs",
            type=int, default=1, metavar="int",
            help="number of parallel jobs; if -1, then the number of jobs is set to the number of cores")

    parser.add_argument("in_filename", type=str, metavar="infile")
    parser.add_argument("out_filename", type=str, metavar="outfile")

    parser.add_argument("--profile",
            action="store_true",
            help="profile program execution")

    arguments = vars(parser.parse_args())

    args = {}
    for key,value in arguments.iteritems():
        if key.startswith("mlle_"):
            args.setdefault("mlle_args", {})[key[len("mlle_"):]] = value
        else:
            args[key] = value

    main(**args)
