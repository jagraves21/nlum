import argparse
from sklearn import manifold

from .base_extractors import run_extractor
from .sklearn_base_extractors import SKLearnAMExtractor

class MMDSExtractor(SKLearnAMExtractor):
    ALGORITHM = "MMDS"

    def __init__(self, profile=False, **kwargs):
        kwargs["metric"] = True
        self.model = manifold.MDS(**kwargs)
        super(MMDSExtractor, self).__init__(profile)

def main(in_filename, out_filename, mmds_args, profile):
    extractor = MMDSExtractor(profile=profile, **mmds_args)
    run_extractor(in_filename,
            out_filename, extractor, mmds_args["n_components"])

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
            description="Metric Multi-Dimensional Scaling",
            formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument("--mmds-n-components",
            type=int, default=2, metavar="int",
            help="number of dimensions in which to immerse the dissimilarities")
    parser.add_argument("--mmds-n-init",
            type=int, default=4, metavar="int",
            help="number of times the SMACOF algorithm will be run with different initializations")
    parser.add_argument("--mmds-max-iter",
            type=int, default=300, metavar="int",
            help="number of times the SMACOF algorithm will be run with different initializations")
    parser.add_argument("--mmds-verbose",
            type=int, default=0, metavar="int",
            help="level of verbosity")
    parser.add_argument("--mmds-eps",
            type=float, default=1e-3, metavar="float",
            help="relative tolerance with respect to stress at which to declare convergence")
    parser.add_argument("--mmds-n-jobs",
            type=int, default=1, metavar="int",
            help="number of parallel jobs; if -1, then the number of jobs is set to the number of cores")
    parser.add_argument("--mmds-random-state",
            type=int, default=None, metavar="int",
            help="seed used by the random number generator")
    parser.add_argument("--mmds-dissimilarity",
            type=str, default="euclidean", metavar="str",
            choices=["euclidean", "precomputed"],
            help="choices: {0}".format(["euclidean", "precomputed"]))

    parser.add_argument("in_filename", type=str, metavar="infile")
    parser.add_argument("out_filename", type=str, metavar="outfile")

    parser.add_argument("--profile",
            action="store_true",
            help="profile program execution")

    arguments = vars(parser.parse_args())

    args = {}
    for key,value in arguments.iteritems():
        if key.startswith("mmds_"):
            args.setdefault("mmds_args", {})[key[len("mmds_"):]] = value
        else:
            args[key] = value

    main(**args)
