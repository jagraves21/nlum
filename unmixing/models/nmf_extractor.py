# standard library imports
import argparse

# third party imports
from sklearn import decomposition

# local application imports
from .sklearn_base_extractors import SKLearnJointExtractor

class NMFExtractor(SKLearnJointExtractor):
    ALGORITHM = "NMF"
    def __init__(self, profile=False, **kwargs):
        self.model = decomposition.NMF(**kwargs)
        super(NMFExtractor, self).__init__(profile)

def main(in_filename, out_filename, nmf_args, profile):
    from .utils import run_standalone
    run_standalone("nmf", in_filename, nmf_args["n_components"], out_filename, constructor_kwargs=nmf_args)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
            description="Non-Negative Matrix Factorization",
            formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument("--nmf-n-components",
            type=int, default=2, metavar="int",
            help="number of components")
    parser.add_argument("--nmf-init",
            type=str, default=None, metavar="str",
            choices=["random", "nndsvd", "nndsvda", "nndsvdar", "custom"],
            help="choices: {0}".format(["random", "nndsvd", "nndsvda", "nndsvdar", "custom"]))
    parser.add_argument("--nmf-solver",
            type=str, default="cd", metavar="str",
            choices=["cd", "mu"],
            help="choices: {0}".format(["cd", "mu"]))
    parser.add_argument("--nmf-beta-loss",
            type=str, default="frobenius", metavar="str",
            choices=["frobenius", "kullback-leibler", "itakura-saito"],
            help="choices: {0}".format(["frobenius", "kullback-leibler", "itakura-saito"]))
    parser.add_argument("--nmf-tol",
            type=float, default=1e-4, metavar="float",
            help="tolerance of the stopping condition")
    parser.add_argument("--nmf-max-iter",
            type=int, default=200, metavar="int",
            help="maximum number of iterations before timing out")
    parser.add_argument("--nmf-random-state",
            type=int, default=None, metavar="int",
            help="seed used by the random number generator")
    parser.add_argument("--nmf-alpha",
            type=float, default=0.0, metavar="float",
            help="constant that multiplies the regularization terms")
    parser.add_argument("--nmf-l1_ratio",
            type=float, default=0.0, metavar="float",
            help="regularization mixing parameter")
    parser.add_argument("--nmf-verbose",
            type=int, default=0, metavar="int",
            help="level of verbosity")
    parser.add_argument("--nmf-shuffle",
            action="store_true",
            help="randomize the order of the coordinates in the CD solver")

    parser.add_argument("in_filename", type=str, metavar="infile")
    parser.add_argument("out_filename", type=str, metavar="outfile")

    parser.add_argument("--profile",
            action="store_true",
            help="profile program execution")

    arguments = vars(parser.parse_args())

    args = {}
    for key,value in arguments.iteritems():
        if key.startswith("nmf_"):
            args.setdefault("nmf_args", {})[key[len("nmf_"):]] = value
        else:
            args[key] = value

    main(**args)
