# third party imports
import numpy as np
import pysptools.eea

# local application imports
from ...utils import time_function
from .base_extractors import EndmembersExtractor

# class ATGPExtractor(EndmembersExtractor):
#     ALGORITHM = "ATGP"

#     def __init__(self, profile=False):
#         super(ATGPExtractor, self).__init__(profile)
        
#     @time_function("endmember extraction time:")
#     def extract_endmembers_2d(self, hsi_2d, n_endmembers, **kwargs):
#         return pysptools.eea.eea.ATGP(hsi_2d, n_endmembers, **kwargs)

#     @time_function("endmember extraction time:")
#     def extract_endmembers_3d(self, hsi_3d, n_endmembers, **kwargs):
#         return pysptools.eea.ATGP().extract(hsi_3d, n_endmembers, **kwargs)

class FIPPIExtractor(EndmembersExtractor):
    ALGORITHM = "FIPPI"

    def __init__(self, profile=False):
        super(FIPPIExtractor, self).__init__(profile)
        
    @time_function("endmember extraction time:")
    def extract_endmembers_2d(self, hsi_2d, n_endmembers, **kwargs):
        return pysptools.eea.eea.FIPPI(hsi_2d, n_endmembers, **kwargs)

    @time_function("endmember extraction time:")
    def extract_endmembers_3d(self, hsi_3d, n_endmembers, **kwargs):
        return pysptools.eea.FIPPI().extract(hsi_3d, n_endmembers, **kwargs)

class NFINDRExtractor(EndmembersExtractor):
    ALGORITHM = "N-FINDR"

    def __init__(self, profile=False):
        super(NFINDRExtractor, self).__init__(profile)

    @time_function("endmember extraction time:")
    def extract_endmembers_3d(self, hsi_3d, n_endmembers, **kwargs):
        return pysptools.eea.NFINDR().extract(hsi_3d, n_endmembers, **kwargs)

class PPIExtractor(EndmembersExtractor):
    ALGORITHM = "PPI"

    def __init__(self, profile=False):
        #self.n_skewers = n_skewers
        super(PPIExtractor, self).__init__(profile)

    @time_function("endmember extraction time:")
    def extract_endmembers_2d(self, hsi_2d, n_endmembers, **kwargs):
        if "numSkewers" not in kwargs:
            kwargs["numSkewers"] = 10000
        return pysptools.eea.eea.PPI(hsi_2d, n_endmembers, **kwargs)

    @time_function("endmember extraction time:")
    def extract_endmembers_3d(self, hsi_3d, n_endmembers, **kwargs):
        return pysptools.eea.PPI().extract(hsi_3d, n_endmembers, **kwargs)