from utils import run_extractor, ATGPExtractor

import argparse
import cProfile
import numpy as np

def main(in_filename, out_filename, n_components, profile):
	extractor = ATGPExtractor(profile)
	run_extractor(in_filename, out_filename, extractor, n_components)

if __name__ == "__main__":
	parser = argparse.ArgumentParser(
			description="Automatic Target Generation Process (ATGP)",
			formatter_class=argparse.ArgumentDefaultsHelpFormatter)

	parser.add_argument("-n", "--n-components", type=int, default=2,
			metavar="int", help="number of endmembers to be induced")
	parser.add_argument("in_filename", type=str, metavar="infile")
	parser.add_argument("out_filename", type=str, metavar="outfile")
	parser.add_argument("--profile",
			action="store_true",
			help="profile program execution")
	
	args = vars(parser.parse_args())
	main(**args)
