from utils import run_extractor, FIPPIExtractor

import argparse
import cProfile
import numpy as np

def main(in_filename, out_filename, n_components, fippi_args, profile):
	extractor = FIPPIExtractor(profile=profile, )
	run_extractor(in_filename, out_filename, extractor, n_components)

if __name__ == "__main__":
	parser = argparse.ArgumentParser(
			description="Fast Iterative Pixel Purity Index (FIPPI)",
			formatter_class=argparse.ArgumentDefaultsHelpFormatter)
	
	parser.add_argument("-n", "--n-components", type=int, default=2,
			metavar="int", help="number of endmembers to be induced")
	parser.add_argument("--fippi-far", type=float, default=None,
			metavar="float", help="false alarm rate")
	parser.add_argument("--fippi-maxit", type=int, default=None,
			metavar="int", help="maximum number of iterations")
	parser.add_argument("in_filename", type=str, metavar="infile")
	parser.add_argument("out_filename", type=str, metavar="outfile")
	parser.add_argument("--profile",
			action="store_true",
			help="profile program execution")
	
	arguments = vars(parser.parse_args())
	
	args = {}
	for key,value in arguments.iteritems():
		if key.startswith("fippi_"):
			args.setdefault("fippi_args", {})[key[len("fippi_"):]] = value
		else:
			args[key] = value
	
	main(**args)
