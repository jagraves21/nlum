from utils import run_extractor, NFINDRExtractor

import argparse
import cProfile
import numpy as np

def main(in_filename, out_filename, n_components, nfindr_args, profile):
	extractor = NFINDRExtractor(profile=profile, **nfindr_args)
	run_extractor(in_filename, out_filename, extractor, n_components)

if __name__ == "__main__":
	parser = argparse.ArgumentParser(
			description="N-FINDR",
			formatter_class=argparse.ArgumentDefaultsHelpFormatter)
	parser.add_argument("-n", "--n-components", type=int, default=2,
			metavar="int", help="number of endmembers to be induced")
	parser.add_argument("--nfindr-maxit", type=int, default=None,
			metavar="int", help="maximum number of iterations")
	parser.add_argument("--nfindr-normalize",
			action="store_true",
			help="normalized before endmembers induction")
	parser.add_argument("--nfindr-ATGP-init",
			action="store_true",
			help="use ATGP to generate the first endmembers")
	parser.add_argument("in_filename", type=str, metavar="infile")
	parser.add_argument("out_filename", type=str, metavar="outfile")
	parser.add_argument("--profile",
			action="store_true",
			help="profile program execution")
	
	arguments = vars(parser.parse_args())
	
	args = {}
	for key,value in arguments.iteritems():
		if key.startswith("nfindr_"):
			args.setdefault("nfindr_args", {})[key[len("nfindr_"):]] = value
		else:
			args[key] = value
	
	main(**args)
