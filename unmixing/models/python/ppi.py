from utils import run_extractor, PPIExtractor

import argparse
import cProfile
import numpy as np

def main(in_filename, out_filename, n_components, ppi_args, profile):
#def main(in_filename, out_filename, n_components, profile):
	extractor = PPIExtractor(profile=profile, **ppi_args)
	run_extractor(in_filename, out_filename, extractor, n_components)

if __name__ == "__main__":
	parser = argparse.ArgumentParser(
			description="Pixel Purity Index (PPI)",
			formatter_class=argparse.ArgumentDefaultsHelpFormatter)

	parser.add_argument("--n-components", type=int, default=2,
			metavar="int", help="number of endmembers to be induced")
	parser.add_argument("--ppi-n-skewers", type=int, default=10000,
			metavar="int", help="number of vectors to project data onto")
	parser.add_argument("in_filename", type=str, metavar="infile")
	parser.add_argument("out_filename", type=str, metavar="outfile")
	parser.add_argument("--profile",
			action="store_true",
			help="profile program execution")
	
	arguments = vars(parser.parse_args())
	
	args = {}
	for key,value in arguments.iteritems():
		if key.startswith("ppi_"):
			args.setdefault("ppi_args", {})[key[len("ppi_"):]] = value
		else:
			args[key] = value
	
	main(**args)
