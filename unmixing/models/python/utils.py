import matplotlib
matplotlib.use("Agg")

import time
from datetime import timedelta

import numpy as np

from sklearn import manifold
from sklearn import decomposition

import pysptools.eea
from pysptools.abundance_maps.amaps import FCLS
from pysptools.abundance_maps.amaps import NNLS

import cProfile

from JointNMF import JointNMF

def time_function(msg="Elapsed Time:"):
	def real_timing_function(function):
		def wrapper(*args, **kwargs):
			start_time = time.time()
			res = function(*args, **kwargs)
			elapsed = time.time() - start_time
			print msg, timedelta(seconds=elapsed)
			return res
		return wrapper
	return real_timing_function

def profile_code(run_profiler, out_filename, profiler=cProfile.Profile()):
	def run_profile(function):
		def wrapper(*args, **kwargs):
			try:
				profiler.enable()
				res = function(*args, **kwargs)
				profiler.disable()
				return res
			finally:
				profiler.print_stats()
				profiler.dump_stats(out_filename)
		return wrapper

	def skip_profile(function):
		def wrapper(*args, **kwargs):
			res = function(*args, **kwargs)
			return res
		return wrapper

	if run_profiler:
		return run_profile
	else:
		return skip_profile

class ComponentExtractor(object):
	ALGORITHM = None

	def __init__(self):
		pass
	
	def get_components(self, hsi_3d, n_components):
		raise NotImplementedError()
	
	@staticmethod
	@time_function(msg="endmembers estimation time:")
	def estimate_endmembers(hsi_3d, abundance_maps):
		hsi_2d = np.reshape(hsi_3d, (-1, hsi_3d.shape[2]) )
		abundance_maps = np.moveaxis(abundance_maps, 0, 2)
		abundance_maps = np.reshape(abundance_maps,
				(-1, abundance_maps.shape[2]))
		endmembers = NNLS(hsi_2d.transpose(), abundance_maps.transpose())
		endmembers = endmembers.transpose()
		return endmembers

	@staticmethod
	@time_function(msg="abundance maps estimation time:")
	def estimate_abundance_maps(hsi_3d, endmembers):
		hsi_2d = np.reshape(hsi_3d, (-1, hsi_3d.shape[2]) )
		abundance_maps = NNLS(hsi_2d, endmembers)
		abundance_maps = np.moveaxis(abundance_maps, 1, 0)
		abundance_maps = np.reshape(abundance_maps,
				(abundance_maps.shape[0], hsi_3d.shape[0], hsi_3d.shape[1]))
		return abundance_maps

class EndmembersExtractor(ComponentExtractor):
	def __init__(self, profile=False):
		self.profile = profile
		super(EndmembersExtractor, self).__init__()

	def extract_endmembers(self, hsi_3d, n_endmembers):
		raise NotImplementedError()

	def get_components(self, hsi_3d, n_components):
		profile_filename = "{}.{}.prof".format(self.ALGORITHM.lower(),
				n_components)
		endmembers = \
			profile_code(self.profile, profile_filename)(
					self.extract_endmembers)(hsi_3d, n_components)
		#endmembers = self.extract_endmembers(hsi_3d, n_components)
		abundance_maps = self.estimate_abundance_maps(hsi_3d, endmembers)
		return endmembers, abundance_maps

class AbundanceMapsExtractor(ComponentExtractor):
	def __init__(self, profile=False):
		self.profile = profile
		super(AbundanceMapsExtractor, self).__init__()

	def extract_abundance_maps(self, hsi_3d, n_abundance_maps):
		raise NotImplementedError()

	def get_components(self, hsi_3d, n_components):
		profile_filename = "{}.{}.prof".format(self.ALGORITHM.lower(),
				n_components)
		abundance_maps = \
			profile_code(self.profile, profile_filename)(
					self.extract_abundance_maps)(hsi_3d, n_components)
		#abundance_maps = self.extract_abundance_maps(hsi_3d, n_components)
		endmembers = self.estimate_endmembers(hsi_3d, abundance_maps)
		return endmembers, abundance_maps






class ATGPExtractor(EndmembersExtractor):
	ALGORITHM = "ATGP"
	
	def __init__(self, profile=False):
		super(ATGPExtractor, self).__init__(profile)

	@time_function("endmember extraction time:")
	def extract_endmembers(self, hsi_3d, n_endmembers):
		hsi_2d = hsi_3d.reshape( (-1, hsi_3d.shape[2]) )
		endmembers, indicies = pysptools.eea.eea.ATGP(hsi_2d, n_endmembers)
		return endmembers
	
class FIPPIExtractor(EndmembersExtractor):
	ALGORITHM = "FIPPI"
	
	def __init__(self, far=None, maxit=None, profile=False):
		self.far = far
		self.maxit = maxit
		super(FIPPIExtractor, self).__init__(profile)

	@time_function("endmember extraction time:")
	def extract_endmembers(self, hsi_3d, n_endmembers):
		hsi_2d = hsi_3d.reshape( (-1, hsi_3d.shape[2]) )
		endmembers, indicies = pysptools.eea.eea.FIPPI(hsi_2d, n_endmembers,
				far=self.far, maxit=self.maxit)
		return endmembers

class NFINDRExtractor(EndmembersExtractor):
	ALGORITHM = "N-FINDR"
	
	def __init__(self, maxit=None, normalize=False, ATGP_init=False,
			profile=False):
		self.maxit = maxit
		self.normalize = normalize
		self.ATGP_init = ATGP_init
		super(NFINDRExtractor, self).__init__(profile)

	@time_function("endmember extraction time:")
	def extract_endmembers(self, hsi_3d, n_endmembers):
		endmembers = pysptools.eea.NFINDR().extract(hsi_3d, n_endmembers,
				maxit=self.maxit, normalize=self.normalize,
				ATGP_init=self.ATGP_init)
		return endmembers
	
class PPIExtractor(EndmembersExtractor):
	ALGORITHM = "PPI"
	
	def __init__(self, n_skewers=10000, profile=False):
		self.n_skewers = n_skewers
		super(PPIExtractor, self).__init__(profile)

	@time_function("endmember extraction time:")
	def extract_endmembers(self, hsi_3d, n_endmembers):
		hsi_2d = hsi_3d.reshape( (-1, hsi_3d.shape[2]) )
		endmembers, indicies = pysptools.eea.eea.PPI(hsi_2d, n_endmembers,
				self.n_skewers)
		return endmembers






class SKLearnAMExtractor(AbundanceMapsExtractor):
	ALGORITHM = None
	
	def __init__(self, profile=False):
		super(SKLearnAMExtractor, self).__init__(profile)
	
	@time_function("abundance maps extraction time:")
	def extract_abundance_maps(self, hsi_3d, n_endmembers):
		hsi_2d = hsi_3d.reshape( (-1, hsi_3d.shape[2]) )
		abundance_maps = self.model.fit_transform(hsi_2d)
		abundance_maps = np.moveaxis(abundance_maps, 1, 0)
		abundance_maps = np.reshape(abundance_maps,
				(abundance_maps.shape[0], hsi_3d.shape[0], hsi_3d.shape[1]))
		return abundance_maps

class IsomapExtractor(SKLearnAMExtractor):
	ALGORITHM = "ISOMAP"
	
	def __init__(self, profile=False, **kwargs):
		self.model = manifold.Isomap(**kwargs)
		super(IsomapExtractor, self).__init__(profile)

class LLEExtractor(SKLearnAMExtractor):
	ALGORITHM = "LLE"
	
	def __init__(self, profile=False, **kwargs):
		kwargs["method"] = "standard"
		self.model = manifold.LocallyLinearEmbedding(**kwargs)
		super(LLEExtractor, self).__init__(profile)

class MLLEExtractor(SKLearnAMExtractor):
	ALGORITHM = "MLLE"
	
	def __init__(self, profile=False, **kwargs):
		kwargs["method"] = "modified"
		self.model = manifold.LocallyLinearEmbedding(**kwargs)
		super(MLLEExtractor, self).__init__(profile)

class HLLEExtractor(SKLearnAMExtractor):
	ALGORITHM = "HLLE"
	
	def __init__(self, profile=False, **kwargs):
		kwargs["method"] = "hessian"
		kwargs["eigen_solver"] = "dense"
		self.model = manifold.LocallyLinearEmbedding(**kwargs)
		super(HLLEExtractor, self).__init__(profile)

class SEExtractor(SKLearnAMExtractor):
	ALGORITHM = "SE"
	
	def __init__(self, profile=False, **kwargs):
		self.model = manifold.SpectralEmbedding(**kwargs)
		super(SEExtractor, self).__init__(profile)

class LTSAExtractor(SKLearnAMExtractor):
	ALGORITHM = "LTSA"
	
	def __init__(self, profile=False, **kwargs):
		self.model = manifold.LocallyLinearEmbedding(**kwargs)
		super(LTSAExtractor, self).__init__(profile)
		
class MMDSExtractor(SKLearnAMExtractor):
	ALGORITHM = "MMDS"
	
	def __init__(self, profile=False, **kwargs):
		kwargs["metric"] = True
		self.model = manifold.MDS(**kwargs)
		super(MMDSExtractor, self).__init__(profile)
		
class NMDSExtractor(SKLearnAMExtractor):
	ALGORITHM = "NMDS"
	
	def __init__(self, profile=False, **kwargs):
		kwargs["metric"] = False
		self.model = manifold.MDS(**kwargs)
		super(NMDSExtractor, self).__init__(profile)

class TSNEExtractor(SKLearnAMExtractor):
	ALGORITHM = "TSNE"
	
	def __init__(self, profile=False, **kwargs):
		kwargs["method"] = "exact"
		self.model = manifold.TSNE(**kwargs)
		super(TSNEExtractor, self).__init__(profile)
		
class KPCAExtractor(SKLearnAMExtractor):
	ALGORITHM = "KPCA"
	
	def __init__(self, profile=False, **kwargs):
		#kwargs["method"] = "exact"
		self.model = decomposition.KernelPCA(**kwargs)
		super(KPCAExtractor, self).__init__(profile)






class SKLearnJointExtractor(ComponentExtractor):
	ALGORITHM = None
	
	def __init__(self, profile=False):
		self.profile = profile
		super(SKLearnJointExtractor, self).__init__()

	@time_function("joint extraction time:")
	def get_components(self, hsi_3d, n_components):
		hsi_2d = np.reshape(hsi_3d,
				(hsi_3d.shape[0]*hsi_3d.shape[1], hsi_3d.shape[2]))
		profile_filename = "{}.{}.prof".format(self.ALGORITHM.lower(),
				n_components)
		abundance_maps = \
			profile_code(self.profile, profile_filename)(
					self.model.fit_transform)(hsi_2d)
		#abundance_maps = self.model.fit_transform(hsi_2d)
		endmembers = self.model.components_
		abundance_maps = np.moveaxis(abundance_maps, 1, 0)
		abundance_maps = np.reshape(abundance_maps,
				(abundance_maps.shape[0], hsi_3d.shape[0], hsi_3d.shape[1]))
		return endmembers, abundance_maps
		
class PCAExtractor(SKLearnJointExtractor):
	ALGORITHM = "PCA"
	
	def __init__(self, profile=False, **kwargs):
		self.model = decomposition.PCA(**kwargs)
		super(PCAExtractor, self).__init__(profile)

class SPCAExtractor(SKLearnJointExtractor):
	ALGORITHM = "SPCA"
	
	def __init__(self, profile=False, **kwargs):
		self.model = decomposition.SparsePCA(**kwargs)
		super(SPCAExtractor, self).__init__(profile)

class NMFExtractor(SKLearnJointExtractor):
	ALGORITHM = "NMF"
	
	def __init__(self, profile=False, **kwargs):
		self.model = decomposition.NMF(**kwargs)
		super(NMFExtractor, self).__init__(profile)
		
class DLExtractor(SKLearnJointExtractor):
	ALGORITHM = "DL"
	
	def __init__(self, profile=False, **kwargs):
		self.model = decomposition.DictionaryLearning(**kwargs)
		super(DLExtractor, self).__init__(profile)

class FAExtractor(SKLearnJointExtractor):
	ALGORITHM = "FA"
	
	def __init__(self, profile=False, **kwargs):
		self.model = decomposition.FactorAnalysis(**kwargs)
		super(FAExtractor, self).__init__(profile)
	
class ICAExtractor(SKLearnJointExtractor):
	ALGORITHM = "ICA"
	
	def __init__(self, profile=False, **kwargs):
		self.model = decomposition.FastICA(**kwargs)
		super(ICAExtractor, self).__init__(profile)
		
class LDAExtractor(SKLearnJointExtractor):
	ALGORITHM = "LDA"
	
	def __init__(self, profile=False, **kwargs):
		self.model = decomposition.LatentDirichletAllocation(**kwargs)
		super(LDAExtractor, self).__init__(profile)

class TSVDExtractor(SKLearnJointExtractor):
	ALGORITHM = "TSVD"
	
	def __init__(self, profile=False, **kwargs):
		self.model = decomposition.TruncatedSVD(**kwargs)
		super(TSVDExtractor, self).__init__(profile)
		
class JointNMFExtractor(SKLearnJointExtractor):
	ALGORITHM = "Joint-NMF"
	
	def __init__(self, profile=False, **kwargs):
		self.model = JointNMF(**kwargs)
		super(JointNMFExtractor, self).__init__(profile)

class GRNMFExtractor(SKLearnJointExtractor):
	ALGORITHM = "GR-NMF"
	
	def __init__(self, profile=False, **kwargs):
		self.model = GRNMF(**kwargs)
		super(GRNMFExtractor, self).__init__(profile)






def run_extractor(in_filename, out_filename, extractor, n_components):
	with np.load(in_filename) as npzfile:
		data = npzfile["data"]
		endmembers, abundance_maps = \
				extractor.get_components(data, n_components)
	print "endmembers", endmembers.shape
	print "abundance_maps", abundance_maps.shape
	np.savez(out_filename,
		abundance_maps=abundance_maps,
		endmembers=endmembers)
	
