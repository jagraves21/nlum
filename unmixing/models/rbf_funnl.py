# standard library imports
import argparse

# local application imports
from ...algorithms import RBFFUNNL
from .base_extractors import run_extractor
from .sklearn_base_extractors import SKLearnJointExtractor

class RBFFUNNLExtractor(SKLearnJointExtractor):
    ALGORITHM = "RBF-FUNNL"
    def __init__(self, profile=False, **kwargs):
        self.model = RBFFUNNL(**kwargs)
        super(RBFFUNNLExtractor, self).__init__(profile)

def main(in_filename, out_filename, rbf_funnl_args, profile):
    from .utils import run_standalone
    run_standalone("rbf_funnl", in_filename, rbf_funnl_args["n_components"], out_filename, constructor_kwargs=rbf_funnl_args)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
            description="RFB FUNNL",
            formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument("--rbf-funnl-n-components",
            type=int, default=2, metavar="int",
            help="number of dictionary elements to extract")
    parser.add_argument("--rbf-funnl-max-iter",
            type=int, default=10, metavar="int",
            help="maximum number of iterations to preform")
    parser.add_argument("--rbf-funnl-alpha",
            type=float, default=1e-3, metavar="float",
            help="???")
    parser.add_argument("--rbf-funnl-beta",
            type=float, default=1.0, metavar="float",
            help="???")
    parser.add_argument("--rbf-funnl-reg-lambda",
            type=float, default=1e-3, metavar="float",
            help="???")

    parser.add_argument("in_filename", type=str, metavar="infile")
    parser.add_argument("out_filename", type=str, metavar="outfile")

    parser.add_argument("--profile",
            action="store_true",
            help="profile program execution")

    arguments = vars(parser.parse_args())

    args = {}
    for key,value in arguments.iteritems():
        if key.startswith("rbf_funnl_"):
            args.setdefault("rbf_funnl_args", {})[key[len("rbf_funnl_"):]] = value
        else:
            args[key] = value

    main(**args)
