# standard library imports
import argparse

# third party imports
from sklearn import decomposition

# local application imports
from .sklearn_base_extractors import SKLearnJointExtractor

class RBFKPCAExtractor(SKLearnJointExtractor):
    ALGORITHM = "RBF-KPCA"
    def __init__(self, profile=False, **kwargs):
        kwargs["kernel"] = "rbf"
        self.model = decomposition.KernelPCA(**kwargs)
        super(RBFKPCAExtractor, self).__init__(profile)

def main(in_filename, out_filename, rbf_kpca_args, profile):
    from .utils import run_standalone
    run_standalone("rbf_kpca", in_filename, rbf_kpca_args["n_components"], out_filename, constructor_kwargs=rbf_kpca_args)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
            description="Principal Component Analysis",
            formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument("--rbf-kpca-n-components",
            type=int, default=None, metavar="int",
            help="number of components to keep")
    parser.add_argument("--rbf-kpca-gamma",
            type=float, default=None, metavar="float",
            help="kernel coefficient for rbf, poly and sigmoid kernels")
    parser.add_argument("--rbf-kpca-eigen-solver",
            type=str, default="auto", metavar="str",
            choices=["auto", "dense", "arpack"],
            help="choices: {0}".format(["auto", "dense", "arpack"]))
    parser.add_argument("--rbf-kpca-tol",
            type=float, default=0.0, metavar="float",
            help="convergence tolerance for arpack; if 0, optimal value is chosen by arpack")
    parser.add_argument("--rbf-kpca-max-iter",
            type=int, default=None, metavar="int",
            help="maximum number of iterations for arpack; if None optimal value is chosen by arpack")
    parser.add_argument("--rbf-kpca-random-state",
            type=int, default=None, metavar="int",
            help="seed used by the random number generator")
    parser.add_argument("--rbf-kpca-n-jobs",
            type=int, default=1, metavar="int",
            help="number of parallel jobs; if -1, then the number of jobs is set to the number of cores")

    parser.add_argument("in_filename", type=str, metavar="infile")
    parser.add_argument("out_filename", type=str, metavar="outfile")

    parser.add_argument("--profile",
            action="store_true",
            help="profile program execution")

    arguments = vars(parser.parse_args())

    args = {}
    for key,value in arguments.iteritems():
        if key.startswith("rbf_kpca_"):
            args.setdefault("rbf_kpca_args", {})[key[len("rbf_kpca_"):]] = value
        else:
            args[key] = value

    main(**args)
