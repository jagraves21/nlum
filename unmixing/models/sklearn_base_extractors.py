# third party imports
import numpy as np

# local application imports
from ...utils import time_function, profile_code
from .base_extractors import ComponentExtractor, AbundanceMapsExtractor
    
class SKLearnAMExtractor(AbundanceMapsExtractor):
    ALGORITHM = None

    def __init__(self, profile=False):
        super(SKLearnAMExtractor, self).__init__(profile)
        
    @time_function("abundance maps extraction time:")
    def extract_abundance_maps_2d(self, hsi_2d, n_abundance_maps, **kwargs):
        abundance_maps = self.model.fit_transform(hsi_2d, **kwargs)
        abundance_maps = np.moveaxis(abundance_maps, 0,1)
        return abundance_maps

class SKLearnJointExtractor(ComponentExtractor):
    ALGORITHM = None

    def __init__(self, profile=False):
        self.profile = profile
        super(SKLearnJointExtractor, self).__init__()
    
    @time_function("joint extraction time:")
    def get_components_2d(self, hsi_2d, n_components, **kwargs):
        profile_filename = "{}.{}.prof".format(self.ALGORITHM.lower(), n_components)
        abundance_maps = profile_code(self.profile, profile_filename)(self.model.fit_transform)(hsi_2d, **kwargs)
        endmembers = self.model.components_
        abundance_maps = np.moveaxis(abundance_maps, 1, 0)
        return endmembers, abundance_maps