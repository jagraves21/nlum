# standard library imports
import argparse

# third party imports
from sklearn import decomposition

# local application imports
from .sklearn_base_extractors import SKLearnJointExtractor

class SPCAExtractor(SKLearnJointExtractor):
    ALGORITHM = "SPCA"
    def __init__(self, profile=False, **kwargs):
        self.model = decomposition.SparsePCA(**kwargs)
        super(SPCAExtractor, self).__init__(profile)

def main(in_filename, out_filename, spca_args, profile):
    from .utils import run_standalone
    run_standalone("sparse_pca", in_filename, spca_args["n_components"], out_filename, constructor_kwargs=spca_args)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
            description="Sparse Principal Component Analysis",
            formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument("--spca-n-components",
            type=int, default=2, metavar="int",
            help="number of sparse atoms to extract")
    parser.add_argument("--spca-alpha",
            type=float, default=1.0, metavar="float",
            help="sparsity controlling parameter; higher values lead to sparser components")
    parser.add_argument("--spca-ridge-alpha",
            type=float, default=0.01, metavar="float",
            help="amount of ridge shrinkage to apply in order to improve conditioning when calling the transform method")
    parser.add_argument("--spca-max-iter",
            type=int, default=1000, metavar="int",
            help="maximum number of iterations to perform")
    parser.add_argument("--spca-tol",
            type=float, default=1e-08, metavar="float",
            help="tolerance for the stopping condition")
    parser.add_argument("--spca-method",
            type=str, default="lars", metavar="str",
            choices=["lars", "cd"],
            help="choices: {0}".format(["lars", "cd"]))
    parser.add_argument("--spca-n-jobs",
            type=int, default=1, metavar="int",
            help="number of parallel jobs; if -1, then the number of jobs is set to the number of cores")
    parser.add_argument("--spca-verbose",
            type=int, default=0, metavar="int",
            help="level of verbosity")
    parser.add_argument("--spca-random-state",
            type=int, default=None, metavar="int",
            help="random number seed")

    parser.add_argument("in_filename", type=str, metavar="infile")
    parser.add_argument("out_filename", type=str, metavar="outfile")

    parser.add_argument("--profile",
            action="store_true",
            help="profile program execution")

    arguments = vars(parser.parse_args())

    args = {}
    for key,value in arguments.iteritems():
        if key.startswith("spca_"):
            args.setdefault("spca_args", {})[key[len("spca_"):]] = value
        else:
            args[key] = value

    main(**args)
