# standard library imports
import argparse

# third party imports
from sklearn import manifold

# local application imports
from .sklearn_base_extractors import SKLearnAMExtractor

class TSNEExtractor(SKLearnAMExtractor):
    ALGORITHM = "TSNE"
    def __init__(self, profile=False, **kwargs):
        kwargs["method"] = "exact"
        self.model = manifold.TSNE(**kwargs)
        super(TSNEExtractor, self).__init__(profile)

def main(in_filename, out_filename, tsne_args, profile):
    from .utils import run_standalone
    run_standalone("tsne", in_filename, tsne_args["n_components"], out_filename, constructor_kwargs=tsne_args)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
            description="Truncated Singular Value Decomposition",
            formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument("--tsne-n-components",
            type=int, default=2, metavar="int",
            help="dimension of the embedded space")
    parser.add_argument("--tsne-perplexity",
            type=float, default=30.0, metavar="float",
            help="perplexity is related to the number of nearest neighbors that is used; larger datasets usually require larger perplexity")
    parser.add_argument("--tsne-early-exaggeration",
            type=float, default=12.0, metavar="float",
            help="controls how tight natural clusters in the original space are in the embedded space and how much space will be between them")
    parser.add_argument("--tsne-learning-rate",
            type=float, default=200.0, metavar="float",
            help="learning rate for t-SNE")
    parser.add_argument("--tsne-n-iter",
            type=int, default=1000, metavar="int",
            help="maximum number of iterations for the optimization")
    parser.add_argument("--tsne-n-iter-without-progress",
            type=int, default=300, metavar="int",
            help="maximum number of iterations without progress before we abort the optimization")
    parser.add_argument("--tsne-metric",
            type=str, default="euclidean", metavar="str",
            choices=["braycurtis", "canberra", "chebyshev", "cityblock", "correlation", "cosine", "dice", "euclidean", "hamming", "jaccard", "kulsinski", "mahalanobis", "matching", "minkowski", "rogerstanimoto", "russellrao", "seuclidean", "sokalmichener", "sokalsneath", "sqeuclidean", "yule"],
            help="choices: {0}".format(["braycurtis", "canberra", "chebyshev", "cityblock", "correlation", "cosine", "dice", "euclidean", "hamming", "jaccard", "kulsinski", "mahalanobis", "matching", "minkowski", "rogerstanimoto", "russellrao", "seuclidean", "sokalmichener", "sokalsneath", "sqeuclidean", "yule"]))
    parser.add_argument("--tsne-init",
            type=str, default="random", metavar="str",
            choices=["random", "pca"],
            help="choices: {0}".format(["random", "pca"]))
    parser.add_argument("--tsne-verbose",
            type=int, default=0, metavar="int",
            help="level of verbosity")
    parser.add_argument("--tsne-random-state",
            type=int, default=None, metavar="int",
            help="seed used by the random number generator")
    parser.add_argument("--tsne-method",
            type=str, default="barnes_hut", metavar="str",
            choices=["barnes_hut", "exact"],
            help="choices: {0}".format(["barnes_hut", "exact"]))
    parser.add_argument("--tsne-angle",
            type=float, default=0.5, metavar="float",
            help="Trade-off between speed and accuracy for Barnes-Hut T-SNE; the angular size of a distant node as measured from a point.")

    parser.add_argument("in_filename", type=str, metavar="infile")
    parser.add_argument("out_filename", type=str, metavar="outfile")

    parser.add_argument("--profile",
            action="store_true",
            help="profile program execution")

    arguments = vars(parser.parse_args())

    args = {}
    for key,value in arguments.iteritems():
        if key.startswith("tsne_"):
            args.setdefault("tsne_args", {})[key[len("tsne_"):]] = value
        else:
            args[key] = value

    main(**args)
