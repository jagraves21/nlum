# standard library imports
import argparse

# third party imports
from sklearn import decomposition

# local application imports
from .sklearn_base_extractors import SKLearnJointExtractor

class TSVDExtractor(SKLearnJointExtractor):
    ALGORITHM = "TSVD"
    def __init__(self, profile=False, **kwargs):
        self.model = decomposition.TruncatedSVD(**kwargs)
        super(TSVDExtractor, self).__init__(profile)

def main(in_filename, out_filename, tsvd_args, profile):
    from .utils import run_standalone
    run_standalone("truncated_svd", in_filename, tsvd_args["n_components"], out_filename, constructor_kwargs=tsvd_args)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
            description="Truncated Singular Value Decomposition",
            formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument("--tsvd-n-components",
            type=int, default=2, metavar="int",
            help="desired dimensionality of output data")
    parser.add_argument("--tsvd-algorithm",
            type=str, default="randomized", metavar="str",
            choices=["arpack", "randomized"],
            help="choices: {0}".format(["arpack", "randomized"]))
    parser.add_argument("--tsvd-n-iter",
            type=int, default=5, metavar="int",
            help="number of interations for randomized SVD solver")
    parser.add_argument("--tsvd-random-state",
            type=int, default=None, metavar="int",
            help="seed used by the random number generator")
    parser.add_argument("--tsvd-tol",
            type=float, default=0.0, metavar="float",
            help="tolerance for ARPACK")

    parser.add_argument("in_filename", type=str, metavar="infile")
    parser.add_argument("out_filename", type=str, metavar="outfile")

    parser.add_argument("--profile",
            action="store_true",
            help="profile program execution")

    arguments = vars(parser.parse_args())

    args = {}
    for key,value in arguments.iteritems():
        if key.startswith("tsvd_"):
            args.setdefault("tsvd_args", {})[key[len("tsvd_"):]] = value
        else:
            args[key] = value

    main(**args)
