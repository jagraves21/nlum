# standard library imports
import datetime
import functools
import platform
import socket

# third party imports
import numpy as np

# local application imports
from .atgp_extractor import ATGPExtractor
from .pysptools_base_extractors import FIPPIExtractor
from .pysptools_base_extractors import NFINDRExtractor
from .pysptools_base_extractors import PPIExtractor

from .isomap_extractor import IsomapExtractor
from .lle_extractor import LLEExtractor
from .mlle_extractor import MLLEExtractor
from .hlle_extractor import HLLEExtractor
from .se_extractor import SEExtractor
from .ltsa_extractor import LTSAExtractor
from .mmds_extractor import MMDSExtractor
from .nmds_extractor import NMDSExtractor
from .tsne_extractor import TSNEExtractor
from .linear_kpca_extractor import LinearKPCAExtractor
from .poly_kpca_extractor import PolyKPCAExtractor
from .rbf_kpca_extractor import RBFKPCAExtractor
from .sigmoid_kpca_extractor import SigmoidKPCAExtractor
from .cosine_kpca_extractor import CosineKPCAExtractor

from .pca_extractor import PCAExtractor
from .spca_extractor import SPCAExtractor
from .nmf_extractor import NMFExtractor
from .dl_extractor import DLExtractor
from .fa_extractor import FAExtractor
from .ica_extractor import ICAExtractor
from .lda_extractor import LDAExtractor
from .tsvd_extractor import TSVDExtractor
from .joint_nmf_extractor import JointNMFExtractor
from .fast_joint_nmf_extractor import FastJointNMFExtractor
from .gr_nmf_extractor import GRNMFExtractor
from .linear_funnl import LinearFUNNLExtractor
from .rbf_funnl import RBFFUNNLExtractor

UNMIXING_MODELS = {
    "atgp":ATGPExtractor,
    "fippi":FIPPIExtractor,
    "nfindr":NFINDRExtractor,
    "ppi":PPIExtractor,

    "isomap":IsomapExtractor,
    "lle":LLEExtractor,
    "modified_lle":MLLEExtractor,
    "hessian_lle":HLLEExtractor,
    "spectral_embedding":SEExtractor,
    "ltsa":LTSAExtractor,
    "metric_mds":MMDSExtractor,
    "nonmetric_mds":NMDSExtractor,
    "tsne":TSNEExtractor,
    "linear_kpca":LinearKPCAExtractor,
    "poly_kpca":PolyKPCAExtractor,
    "rbf_kpca":RBFKPCAExtractor,
    "sigmoid_kpca":SigmoidKPCAExtractor,
    "cosine_kpca":CosineKPCAExtractor,

    "pca":PCAExtractor,
    "sparse_pca":SPCAExtractor,
    "nmf":NMFExtractor,
    "dictionary_learning":DLExtractor,
    "factor_analysis":FAExtractor,
    "independent_component_analysis":ICAExtractor,
    "latent_dirichlet_allocation":LDAExtractor,
    "truncated_svd":TSVDExtractor,
    "joint_nmf":JointNMFExtractor,
    "fast_joint_nmf":FastJointNMFExtractor,
    "gr_nmf":GRNMFExtractor,
    "linear_funnl":LinearFUNNLExtractor,
    "rbf_funnl":RBFFUNNLExtractor
}

def unmix(hsi_3d, n_components, n_neighbors=5, model="pca", constructor_kwargs=dict(), **kwargs):
    #if sys.version_info[0] == 3:
    #    argspec = inspect.getfullargspec
    #else:
    #    argspec = inspect.getargspec
        
    constructor_kwargs = dict(constructor_kwargs)
    
    if model in UNMIXING_MODELS:
        model_classobj = UNMIXING_MODELS[model]
        
        sklearn_local_algorithms = [IsomapExtractor, LLEExtractor, MLLEExtractor, HLLEExtractor, SEExtractor, LTSAExtractor]
        if model_classobj in sklearn_local_algorithms:
            constructor_kwargs.setdefault("n_components", n_components)
            constructor_kwargs.setdefault("n_neighbors", n_neighbors)
        
        sklearn_global_algorithms = [MMDSExtractor, NMDSExtractor, TSNEExtractor, LinearKPCAExtractor, PolyKPCAExtractor, RBFKPCAExtractor, SigmoidKPCAExtractor, CosineKPCAExtractor]
        if model_classobj in sklearn_global_algorithms:
            constructor_kwargs.setdefault("n_components", n_components)

        joint_algorithms = [PCAExtractor, SPCAExtractor, NMFExtractor, DLExtractor, FAExtractor, ICAExtractor, LDAExtractor, TSVDExtractor, JointNMFExtractor, FastJointNMFExtractor, GRNMFExtractor, LinearFUNNLExtractor, RBFFUNNLExtractor]
        if model_classobj in joint_algorithms:
            constructor_kwargs.setdefault("n_components", n_components)
        
        model_class = model_classobj(**constructor_kwargs)
        func = functools.partial(model_class.get_components, data=hsi_3d, n_components=n_components)
    elif callable(model):
        func = functools.partial(model, data=hsi_3d, n_components=n_components)
    else:
        message = "Unknown unmixing model `{}`".format(model)
        raise ValueError(message)

    return func(**kwargs)

def run_standalone(model, hsi_3d_filename, n_components, results_filename, constructor_kwargs=dict(), **kwargs):
    hsi_3d = np.load(hsi_3d_filename)
    
    endmembers, abundances = unmix(
        hsi_3d=hsi_3d,
        n_components=n_components,
        model=model,
        constructor_kwargs=constructor_kwargs,
        **kwargs
    )
    
    timestamp = datetime.datetime.now().strftime("%Y_%m_%d-%H_%M_%S")
    machine_id = socket.getfqdn()
    platform_string = platform.platform()
    
    np.savez(
        results_filename,
        endmembers=endmembers,
        abundances=abundances,
        model=model,
        n_components=n_components,
        hsi_3d_filename=hsi_3d_filename,
        timestamp=timestamp,
        machine_id=machine_id,
        platform=platform_string,
        **kwargs
    )
