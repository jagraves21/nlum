import enum
import os
import functools
import re
import warnings
import h5py

from nlum.utils import hdf as hdf_utils
from .models import unmix, UNMIXING_MODELS

class Compute(enum.Enum):
    AUTO = "AUTO"
    RECOMPUTE = "RECOMPUTE"
    CACHE_ONLY = "CACHE_ONLY"

# def extract_components(extractor, hsi_3d, n_endmembers, results_filename=None, compute=Compute.AUTO, save_results=True):
#     if not isinstance(compute, Compute):
#         raise TypeError("compute must be an instance of Compute Enum")
    
#     image_dim = hsi_3d.shape[0:2]
#     if compute != compute.RECOMPUTE:
#         try:
#             endmembers, abundance_maps = read_results(results_filename)
#             expected_shape = (n_endmembers, hsi_3d.shape[-1])
#             if endmembers.shape != expected_shape:
#                 message = "Unexpected endmembers shape {}, expecting {}"
#                 message = message.format(endmembers.shape, expected_shape)
#                 warnings.warn(message, UserWarning)
#                 #raise RuntimeError(message)
#             expected_shape = (n_endmembers, hsi_3d.shape[0], hsi_3d.shape[1])
#             if abundance_maps.shape != expected_shape:
#                 message = "Unexpected abundance maps shape {}, expecting {}"
#                 message = message.format(abundance_maps.shape, expected_shape)
#                 warnings.warn(message, UserWarning)
#                 #raise RuntimeError(message)
#         except Exception as ex:
#             if compute != Compute.CACHE_ONLY:
#                 message = "{}\nrecomputing {}".format(ex, extractor.ALGORITHM)
#                 warnings.warn(message, RuntimeWarning)
#                 #print >> sys.stderr, ex
#                 #print >> sys.stderr, "recomputing", extractor.ALGORITHM
#                 compute = Compute.RECOMPUTE
#             else:
#                 raise
    
#     if compute == Compute.RECOMPUTE:
#         endmembers, abundance_maps = extractor.get_components(hsi_3d, n_endmembers)
#         expected_shape = (n_endmembers, hsi_3d.shape[-1])
#         if endmembers.shape != expected_shape:
#             message = "Unexpected endmembers shape {}, expecting {}"
#             message = message.format(endmembers.shape, expected_shape)
#             warnings.warn(message, RuntimeWarning)
#             #print >> sys.stderr, message
#         expected_shape = (n_endmembers, hsi_3d.shape[0], hsi_3d.shape[1])
#         if abundance_maps.shape != expected_shape:
#             message = "Unexpected abundance maps shape {}, expecting {}"
#             message = message.format(abundance_maps.shape, expected_shape)
#             warnings.warn(message, RuntimeWarning)
#             #print >> sys.stderr, message
        
#         if save_results:
#             write_results(results_filename, endmembers, abundance_maps)
#     return endmembers, abundance_maps

def _get_unmixing_results_base_location(
    algorithm_name,
    hdf_hsi_3d_source=None,
):
    return "{}-{}".format(os.path.basename(hdf_hsi_3d_source.name), algorithm_name)
    
def _find_newest_unmixing_result(
    algorithm_name,
    hdf_hsi_3d_source=None,
    n_components=None
):
    group_name = _get_unmixing_results_base_location(algorithm_name=algorithm_name, hdf_hsi_3d_source=hdf_hsi_3d_source) 
    pattern = "^{}[0-9]{{3}}$".format( re.escape("{}_".format(group_name)) )
    hdf_groups = hdf_utils.get_groups(hdf_hsi_3d_source.parent, pattern, regex=True)
    
    #if n_components is not None:
    #    for item in hdf_groups:
    #        print "item", item
    #    a = b + c
    
    if len(hdf_groups) > 0:
        hdf_groups.sort(key=lambda hdf_group: hdf_group.attrs["time_stamp"])
        return hdf_groups[-1]
    else:
        return None

def load_unmixing_results(
    hdf_group,
    endmembers_name="Endmembers",
    abundances_name="Abundances"
):
    endmembers = None if endmembers_name is None else hdf_utils.load_dataset(hdf_group, endmembers_name)
    abundances = None if abundances_name is None else hdf_utils.load_dataset(hdf_group, abundances_name)
    return endmembers, abundances 

def create_unmixing_dataset(
    hdf_group,
    
    endmembers,
    abundances,
    
    endmembers_attrs=dict(),
    abundances_attrs=dict()
):
    if endmembers is not None:
        endmembers_attrs = dict(endmembers_attrs)
        if "name" not in endmembers_attrs:
            endmembers_attrs["name"] = "Endmembers"
        hdf_endmembers_data = hdf_utils.create_endmembers_dataset(hdf_group, endmembers=endmembers, **endmembers_attrs)
    else:
        hdf_endmembers_data = None
    
    if abundances is not None:
        abundances_attrs = dict(abundances_attrs)
        if "name" not in abundances_attrs:
            abundances_attrs["name"] = "Abundances"
        if hdf_endmembers_data is not None:
            if "Position_Indices" not in abundances_attrs:
                abundances_attrs["Position_Indices"] = hdf_endmembers_data.attrs["Position_Indices"]
            if "Position_Values" not in abundances_attrs:
                abundances_attrs["Position_Values"] = hdf_endmembers_data.attrs["Position_Values"]
        hdf_abundances_data = hdf_utils.create_abundances_dataset(hdf_group, abundances=abundances, **abundances_attrs)
    else:
        hdf_abundances_data = None
    
    return hdf_endmembers_data, hdf_abundances_data

def save_unmixing_results(
    algorithm_name,
    
    endmembers,
    abundances,
    
    hdf_hsi_3d_source=None,
    
    endmembers_attrs=dict(),
    abundances_attrs=dict(),
    group_attrs=dict()
):
    group_name = _get_unmixing_results_base_location(
        algorithm_name,
        hdf_hsi_3d_source=hdf_hsi_3d_source,
    )
    
    pattern = "^{}[0-9]{{3}}$".format( re.escape("{}_".format(group_name)) )
    hdf_groups = hdf_utils.get_groups(hdf_hsi_3d_source.parent, pattern, regex=True)
    idx = 0
    for hdf_group in hdf_groups:
        try:
            tmp_idx = int( hdf_group.name.split("_")[-1] )
            if idx <= tmp_idx:
                idx = tmp_idx+1
        except:
            pass
    group_name = "{}_{:03}".format(group_name, idx)
    
    group_attrs = dict(group_attrs)
    group_attrs.setdefault("algorithm", algorithm_name)
    group_attrs.setdefault("source", hdf_hsi_3d_source.ref)
    hdf_unmixing_group = hdf_utils.create_group(hdf_hsi_3d_source.parent, group_name, **group_attrs)
    
    hdf_endmembers_data, hdf_abundances_data = create_unmixing_dataset(
        hdf_unmixing_group,
        
        endmembers=endmembers,
        abundances=abundances,
        
        endmembers_attrs=endmembers_attrs,
        abundances_attrs=abundances_attrs
    )
    
    return hdf_unmixing_group, hdf_endmembers_data, hdf_abundances_data

def run_algorithm(
    algorithm,
    compute,
    
    hsi_3d,
    
    n_components,
    
    hdf_hsi_3d_source=None,
    
    endmembers_attrs=dict(),
    abundances_attrs=dict(),
    
    group_attrs=dict(),
    
    save_results=True
):
    if not isinstance(compute, Compute):
        raise TypeError("`compute` must be an instance of Compute Enum")
        
    replace_dic = {
        "atgp":"ATGP",
        "fippi":"FIPPI",
        "gr":"GR",
        "kpca":"KPCA",
        "lle":"LLE",
        "ltsa":"LTSA",
        "mds":"MDS",
        "nfindr":"NFINDR",
        "nmf":"NMF",
        "pca":"PCA",
        "ppi":"PPI",
        "rbf":"RBF",
        "svd":"SVD",
        "tsne":"TSNE"
    }
    algorithm_name = "_".join([replace_dic.get(ss, ss.title()) for ss in algorithm.split("_")])
        
    if compute != Compute.RECOMPUTE:
        try:
            message = []
            if hdf_hsi_3d_source is None:
                message.append("hdf_hsi_3d_source must not be None")
            if len(message) > 0:
                message = "When compute = Compute.CACHE_ONLY, {}".format( ",".join(message) )
                raise ValueError(message)
        
            hdf_unmixing_group = _find_newest_unmixing_result(
                algorithm_name=algorithm_name,
                hdf_hsi_3d_source=hdf_hsi_3d_source,
                n_components=n_components
            )

            if hdf_unmixing_group is None:
                message = "{}: no suitable results found".format(algorithm)
                raise RuntimeError(message)

            unmixed_endmembers, unmixed_abundances = load_unmixing_results(hdf_unmixing_group)

            return unmixed_endmembers, unmixed_abundances, hdf_unmixing_group
        except StandardError as se:
            if compute == Compute.CACHE_ONLY: 
                raise
            else:
                message = "{}, executing {}".format(str(se), algorithm)
                warnings.warn(message)
    
    unmixed_endmembers, unmixed_abundances = unmix(hsi_3d, n_components=n_components, model=algorithm)
    
    if hdf_hsi_3d_source is None:
        hdf_unmixing_group = None
        if save_results:
            message = "unable to save results: hdf_hsi_3d_source not provided"
            warnings.warn(message)
    else:
        if save_results:
            group_attrs = dict(group_attrs)
            group_attrs.setdefault("n_components", n_components)
            
            hdf_unmixing_group, _, _ = save_unmixing_results(
                algorithm_name=algorithm_name,

                endmembers=unmixed_endmembers,
                abundances=unmixed_abundances,

                hdf_hsi_3d_source=hdf_hsi_3d_source,

                endmembers_attrs=endmembers_attrs,
                abundances_attrs=abundances_attrs,
                group_attrs=group_attrs
            )
        else:
            hdf_unmixing_group = None
    return unmixed_endmembers, unmixed_abundances, hdf_unmixing_group




            
# def run_algorithm(alg_name, dirname, n_endmembers, n_neighbors, hsi_3d, compute=Compute.AUTO, save_results=True):
#     basename = "{}.{}.npz".format(alg_name.lower(), n_endmembers)
#     results_filename = os.path.join(dirname, basename)
    
#     pysp_algorithms = [ATGPExtractor, FIPPIExtractor, NFINDRExtractor, PPIExtractor]
#     for extractor in pysp_algorithms:
#         if extractor.ALGORITHM == alg_name:
#             return extract_components(extractor(), hsi_3d, n_endmembers, results_filename=results_filename, compute=compute, save_results=save_results)
        
#     sklearn_local_algorithms = [IsomapExtractor, LLEExtractor, MLLEExtractor, HLLEExtractor, SEExtractor, LTSAExtractor]
#     for extractor in sklearn_local_algorithms:
#         if extractor.ALGORITHM == alg_name:
#             return extract_components(extractor(n_components=n_endmembers, n_neighbors=n_neighbors), hsi_3d, n_endmembers, results_filename=results_filename, compute=compute, save_results=save_results)
        
#     sklearn_global_algorithms = [MMDSExtractor, NMDSExtractor, TSNEExtractor, LinearKPCAExtractor, PolyKPCAExtractor, RBFKPCAExtractor, SigmoidKPCAExtractor, CosineKPCAExtractor]
#     for extractor in sklearn_global_algorithms:
#         if extractor.ALGORITHM == alg_name:
#             return extract_components(extractor(n_components=n_endmembers), hsi_3d, n_endmembers, results_filename=results_filename, compute=compute, save_results=save_results)
    
#     joint_algorithms = [PCAExtractor, SPCAExtractor, NMFExtractor, DLExtractor, FAExtractor, ICAExtractor, LDAExtractor, TSVDExtractor, JointNMFExtractor, FastJointNMFExtractor, GRNMFExtractor, LinearFUNNLExtractor, RBFFUNNLExtractor]
#     for extractor in joint_algorithms:
#         if extractor.ALGORITHM == alg_name:
#             return extract_components(extractor(n_components=n_endmembers), hsi_3d, n_endmembers, results_filename=results_filename, compute=compute, save_results=save_results)
    
#     message = "Algorithm Not Implemented: {}".format(alg_name)
#     raise NotImplementedError(message)
