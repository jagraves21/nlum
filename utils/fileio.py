import sys

import numpy as np

from sklearn import preprocessing

from pysptools.abundance_maps.amaps import FCLS
from pysptools.abundance_maps.amaps import NNLS

def read_hsi_data(filename):
    with np.load( filename ) as npz_file:
        expected_fields = ["data", "labels"]
        unexpected_fileds = [name for name in npz_file.files if name not in expected_fields]
        if unexpected_fileds:
            message = "ignoring unexpected fileds in '{}': {}"
            message = message.format(filename, ", ".join(unexpected_fileds))
            print >> sys.stderr, message

        hsi_3d = npz_file["data"]
        if len(hsi_3d.shape) != 3:
            raise RuntimeError("Image must have size height x width x bands")

        if "labels" in npz_file.files:
            labels = npz_file["labels"]
            if len(labels.shape) != 2:
                raise RuntimeError("Labels must have size height x width")
            elif hsi_3d.shape[:-1] != labels.shape:
                message = "Image and label dimensions do not mach: {} {}"
                message = message.format(hsi_3d.shape, labels.shape)
                raise RuntimeError(message)
        else:
            labels = None
    return hsi_3d, labels 

def read_ground_truth(filename, hsi_3d):
    hsi_2d = np.reshape(hsi_3d, (hsi_3d.shape[0]*hsi_3d.shape[1], hsi_3d.shape[2]))
    image_dim = (hsi_3d.shape[0], hsi_3d.shape[1])
    spectrial_len = hsi_3d.shape[2]

    with np.load( filename ) as npz_file:
        expected_fields = ["abundance_maps", "endmembers"]
        unexpected_fileds = [name for name in npz_file.files if name not in expected_fields]
        if unexpected_fileds:
            message = "ignoring unexpected fileds in '{}': {}"
            message = message.format(filename, ", ".join(unexpected_fileds))
            print >> sys.stderr, message

        if "abundance_maps" in npz_file.files or "endmembers" in npz_file.files:
            if "abundance_maps" in npz_file.files:
                abundance_maps = npz_file["abundance_maps"]
                if abundance_maps.shape[1:3] != image_dim:
                    message = "data image size does not match ground truth image size: {} {}"
                    message = message.format(hsi_3d.shape, abundance_maps.shape)
                    raise RuntimeError(message)

            if "endmembers" in npz_file.files:
                endmembers = npz_file["endmembers"]
                if endmembers.shape[1] != spectrial_len:
                    message = "data spectrum does not mach ground truth spectrum: {} {}"
                    message = message.format(hsi_3d.shape, endmembers.shape)
                    raise RuntimeError(message)

            if "abundance_maps" not in npz_file.files:
                message = "ground truth abundance maps not provided, estimating with NNLS"
                print >> sys.stderr, message
                #abundance_maps = FCLS(hsi_2d, endmembers)
                abundance_maps = NNLS(hsi_2d, endmembers)
                abundance_maps = preprocessing.normalize(abundance_maps, norm="l1")
                abundance_maps = np.reshape(abundance_maps, (hsi_3d.shape[0], hsi_3d.shape[1], abundance_maps.shape[1]))
                abundance_maps = np.moveaxis(abundance_maps, 2, 0)
            elif "endmembers" not in npz_file.files:
                message = "ground truth endmembers not provided, estimating with NNLS"
                print >> sys.stderr, message
                print abundance_maps.shape
                tmp_maps = np.moveaxis(abundance_maps, 0, 2)
                print tmp_maps.shape
                tmp_maps = np.reshape(tmp_maps, (tmp_maps.shape[0]*tmp_maps.shape[1],tmp_maps.shape[2]))
                print tmp_maps.shape 
                endmembers = NNLS(hsi_2d.transpose(), tmp_maps.transpose())
                endmembers = endmembers.transpose()
                print endmembers.shape
            elif abundance_maps.shape[0] != endmembers.shape[0]:
                message = "number of abundance maps ({}) does not match associated endmembers ({})"
                message = message.format(abundance_maps.shape, endmembers.shape)
                print >> sys.stderr, message

        else:
            message = "No ground truth data found in '{}'"
            message = message.format(filename)
            raise RuntimeError(message)
    return endmembers, abundance_maps

def read_results(filename):
    with np.load(filename) as npz_file:
        unexpected_fileds = [field for field in npz_file.files if field not in ["endmembers", "abundance_maps"]]

        if unexpected_fileds:
            message = "ignoring unexpected fileds in '{}': {}"
            message = message.format(filename, ", ".join(unexpected_fileds))
            print >> sys.stderr, message

        endmembers = npz_file["endmembers"]
        abundance_maps = npz_file["abundance_maps"]

        return endmembers, abundance_maps

def write_results(filename, endmembers, abundance_maps):
    np.savez(filename, endmembers=endmembers, abundance_maps=abundance_maps)
