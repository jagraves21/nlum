import datetime
import itertools
import numpy as np
import os
import platform
import re
import socket
import six
import h5py

_PYUSID_VERSION_ = "0.0.1"

def display_tree(hdf_parent, indent_width=2):
    def _traverse(name, obj):
        def _get_label(tmp_obj):
            if isinstance(tmp_obj, h5py.Group):
                return tmp_obj.name
            else:
                return "{} - {}".format(tmp_obj.name, tmp_obj.shape)
        parent, child = _get_label(obj.parent), _get_label(obj)
        tree.setdefault(parent, []).append(child)
    
    def _print_tree(parent, grandparent, tree, prefix):
        if parent != hdf_parent.name:
            print parent.split("/")[-1]
        if parent in tree:
            for ii,child in enumerate(tree[parent],1):
                if ii != len(tree[parent]):
                    s1, s2 = u"\u2560", u"\u2551"
                else:
                    s1, s2 = u"\u255A", " "
                s3 = u"\u2550" if tree.get(child) == None else u"\u2566"
                print u"{}{}{}{}".format(prefix, s1, u"\u2550" * indent_width, s3),
                new_prefix = u"{}{}{}".format(prefix, s2, " " * indent_width)
                _print_tree(child, parent, tree, new_prefix)
    
    tree = {}
    hdf_parent.visititems(_traverse)
    
    print hdf_parent.filename
    _print_tree(hdf_parent.name, None, tree, "")
    
def display_objects(hdf_parent):
    def _traverse(name, obj):
        print name,
        if isinstance(obj, h5py.Dataset):
            print obj.shape
        else:
            print
        for key,value in obj.attrs.items():
            print "  ", key, value,
            if isinstance(value, h5py.Reference):
                print "->", hdf_parent[value].name
            else:
                print
    hdf_parent.visititems(_traverse)

def get_dataset_shape(hdf_parent, name):
    hsi_dataset = hdf_parent[name]
    
    position_indices = hdf_parent[ hsi_dataset.attrs["Position_Indices"] ]
    position_indices = position_indices[()]
    
    spectroscopic_indices = hdf_parent[ hsi_dataset.attrs["Spectroscopic_Indices"] ]
    spectroscopic_indices = spectroscopic_indices[()]
    
    shape = np.hstack([
        np.flip(np.max(position_indices, axis=0)) + 1,
        np.flip(np.max(spectroscopic_indices, axis=1)) + 1
    ])
    
    return tuple(shape)
    
def load_dataset(hdf_parent, name):
    shape = get_dataset_shape(hdf_parent, name)
    hsi_dataset = hdf_parent[name]
    return np.reshape(hsi_dataset[()], shape)

def get_objects(hdf_parent, pattern, regex=False, re_flags=0, instanceof=None):
    results = []
    def _traverse(name, obj):
        if instanceof is None or isinstance(obj, instanceof):
            if pattern in os.path.basename(name):
                results.append(obj)
    def _traverse_regex(name, obj):
        if instanceof is None or isinstance(obj, instanceof):
            res = re.search(pattern, os.path.basename(name), flags=re_flags)
            if res is not None:
                results.append(obj)
    hdf_parent.visititems( _traverse_regex if regex else _traverse )
    return results

def get_datasets(hdf_parent, pattern, regex=False, re_flags=0):
    return get_objects(hdf_parent=hdf_parent, pattern=pattern, regex=regex, re_flags=re_flags, instanceof=h5py.Dataset)

def get_groups(hdf_parent, pattern, regex=False, re_flags=0):
    return get_objects(hdf_parent=hdf_parent, pattern=pattern, regex=regex, re_flags=re_flags, instanceof=h5py.Group)

def create_group(hdf_parent, name, track_order=None, **group_attrs):
    if "time_stamp" not in group_attrs:
        group_attrs["time_stamp"] = datetime.datetime.now().strftime("%Y_%m_%d-%H_%M_%S")
    if "machine_id" not in group_attrs:
        group_attrs["machine_id"] = socket.getfqdn()
    if "pyUSID_version" not in group_attrs:
        group_attrs["pyUSID_version"] = _PYUSID_VERSION_
    if "platform" not in group_attrs:
        group_attrs["platform"] = platform.platform()
    group = hdf_parent.create_group(name, track_order=track_order)
    group.attrs.update(group_attrs)
    return group

def create_dataset(hdf_parent, name, shape=None, dtype=None, data=None, create_kwargs=dict(), **dataset_attrs):
    hdf_dataset = hdf_parent.create_dataset(name, shape=shape, dtype=dtype, data=data, **create_kwargs)
    hdf_dataset.attrs.update(dataset_attrs)
    return hdf_dataset

def _create_position_entry(hdf_parent, name, **kwargs):
    if len(six.viewkeys(kwargs) & {"hsi_3d", "data"}) != 1:
        message = "Either hsi_3d or data (but not both) keyword is requiared."
        raise RuntimeError(message)
    if "labels" not in kwargs:
        kwargs["labels"] = ["X", "Y"]
    if "units" not in kwargs:
        kwargs["units"] = ["N/A", "N/A"]
    if "data" not in kwargs:
        hsi_3d = kwargs["hsi_3d"]
        del kwargs["hsi_3d"]
        kwargs["data"] = np.flip(np.asarray(list( itertools.product( *[np.arange(ii) for ii in hsi_3d.shape[:2]] ) )), axis=1)
    return create_dataset(hdf_parent, name, **kwargs)

def create_position_indices(hdf_parent, **kwargs):
    if "name" not in kwargs:
        kwargs["name"] = "Position_Indices"
    return _create_position_entry(hdf_parent, **kwargs)

def create_position_values(hdf_parent, **kwargs):
    if "name" not in kwargs:
        kwargs["name"] = "Position_Values"
    return _create_position_entry(hdf_parent, **kwargs)

def _create_spectroscopic_entry(hdf_parent, name, **kwargs):
    if len(six.viewkeys(kwargs) & {"hsi_3d", "data"}) != 1:
        message = "Either hsi_3d or data (but not both) keyword is requiared."
        raise RuntimeError(message)
    if "labels" not in kwargs:
        kwargs["labels"] = ["Z"]
    if "units" not in kwargs:
        kwargs["units"] = ["N/A"]
    if "data" not in kwargs:
        hsi_3d = kwargs["hsi_3d"]
        del kwargs["hsi_3d"]
        kwargs["data"] = np.arange(hsi_3d.shape[2]).reshape((1,-1))
    return create_dataset(hdf_parent, name, **kwargs)

def create_spectroscopic_indices(hdf_parent, **kwargs):
    if "name" not in kwargs:
        kwargs["name"] = "Spectroscopic_Indices"
    return _create_spectroscopic_entry(hdf_parent, **kwargs)
#     return _create_position_entry(hdf_parent, **kwargs)

def create_spectroscopic_values(hdf_parent, **kwargs):
    if "name" not in kwargs:
        kwargs["name"] = "Spectroscopic_Values"
    return _create_spectroscopic_entry(hdf_parent, **kwargs)
#     return _create_position_entry(hdf_parent, **kwargs)

def create_endmembers_dataset(hdf_parent, name, endmembers, **kwargs):
    assert len(endmembers.shape) == 2, "endmembers is expected to be 2D"
    
    if "time_stamp" not in kwargs:
        kwargs["time_stamp"] = datetime.datetime.now().strftime("%Y_%m_%d-%H_%M_%S")
    if "machine_id" not in kwargs:
        kwargs["machine_id"] = socket.getfqdn()
    if "pyUSID_version" not in kwargs:
        kwargs["pyUSID_version"] = _PYUSID_VERSION_
    if "platform" not in kwargs:
        kwargs["platform"] = platform.platform()
    if "quantity" not in kwargs:
        kwargs["quantity"] = "N/A"
    if "units" not in kwargs:
        kwargs["units"] = "N/A"
    if "Position_Indices" not in kwargs:
        tmp_data = np.reshape(np.arange(len(endmembers)), (-1,1))
        position_indices = create_position_indices(hdf_parent, name="Endmember_Indices", data=tmp_data)
        kwargs["Position_Indices"] = position_indices.ref
    if "Position_Values" not in kwargs:
        tmp_data = np.reshape(np.arange(len(endmembers)), (-1,1))
        position_values = create_position_values(hdf_parent, name="Endmember_Values", data=tmp_data)
        kwargs["Position_Values"] = position_values.ref
    if "Spectroscopic_Indices" not in kwargs:
        tmp_data = np.reshape(np.arange(endmembers.shape[1]), (1,-1))
        spectroscopic_indices = create_spectroscopic_indices(hdf_parent, data=tmp_data)
        kwargs["Spectroscopic_Indices"] = spectroscopic_indices.ref
    if "Spectroscopic_Values" not in kwargs:
        tmp_data = np.reshape(np.arange(endmembers.shape[1]), (1,-1))
        spectroscopic_values = create_spectroscopic_values(hdf_parent, data=tmp_data)
        kwargs["Spectroscopic_Values"] = spectroscopic_values.ref
        
    if not endmembers.flags["C_CONTIGUOUS"]:
        endmembers = np.ascontiguousarray(endmembers)
    return create_dataset(hdf_parent, name, data=endmembers, **kwargs)

def create_abundances_dataset(hdf_parent, name, abundances, **kwargs):
    assert len(abundances.shape) == 3, "abundances is expected to be 3D"
    
    if "time_stamp" not in kwargs:
        kwargs["time_stamp"] = datetime.datetime.now().strftime("%Y_%m_%d-%H_%M_%S")
    if "machine_id" not in kwargs:
        kwargs["machine_id"] = socket.getfqdn()
    if "pyUSID_version" not in kwargs:
        kwargs["pyUSID_version"] = _PYUSID_VERSION_
    if "platform" not in kwargs:
        kwargs["platform"] = platform.platform()
    if "quantity" not in kwargs:
        kwargs["quantity"] = "N/A"
    if "units" not in kwargs:
        kwargs["units"] = "N/A"
    if "Position_Indices" not in kwargs:
        tmp_data = np.reshape(np.arange(len(abundances)), (-1,1))
        position_indices = create_position_indices(hdf_parent, name="Endmember_Indices", data=tmp_data)
        kwargs["Position_Indices"] = position_indices.ref
    if "Position_Values" not in kwargs:
        tmp_data = np.reshape(np.arange(len(abundances)), (-1,1))
        position_values = create_position_values(hdf_parent, name="Endmember_Values", data=tmp_data)
        kwargs["Position_Values"] = position_values.ref
    if "Spectroscopic_Indices" not in kwargs:
        tmp_data = np.flip(np.asarray(list( itertools.product( *[np.arange(ii) for ii in abundances.shape[1:]] ) )), axis=1)
        tmp_data = np.transpose(tmp_data)
        spectroscopic_indices = create_spectroscopic_indices(hdf_parent, name="Abundance_Indices", data=tmp_data)
        kwargs["Spectroscopic_Indices"] = spectroscopic_indices.ref
    if "Spectroscopic_Values" not in kwargs:
        tmp_data = np.flip(np.asarray(list( itertools.product( *[np.arange(ii) for ii in abundances.shape[1:]] ) )), axis=1)
        tmp_data = np.transpose(tmp_data)
        spectroscopic_values = create_spectroscopic_values(hdf_parent, name="Abundance_Values", data=tmp_data)
        kwargs["Spectroscopic_Values"] = spectroscopic_values.ref
        
    if not abundances.flags["C_CONTIGUOUS"]:
        abundances = np.ascontiguousarray(abundances)
    abundances = np.reshape(abundances, (abundances.shape[0],-1))
    return create_dataset(hdf_parent, name, data=abundances, **kwargs)

def create_hsi_3d_dataset(hdf_parent, name, hsi_3d, **kwargs):
    assert len(hsi_3d.shape) == 3, "hsi_3d is expected to be 3D"
    
    if "time_stamp" not in kwargs:
        kwargs["time_stamp"] = datetime.datetime.now().strftime("%Y_%m_%d-%H_%M_%S")
    if "machine_id" not in kwargs:
        kwargs["machine_id"] = socket.getfqdn()
    if "pyUSID_version" not in kwargs:
        kwargs["pyUSID_version"] = _PYUSID_VERSION_
    if "platform" not in kwargs:
        kwargs["platform"] = platform.platform()
    if "quantity" not in kwargs:
        kwargs["quantity"] = "N/A"
    if "units" not in kwargs:
        kwargs["units"] = "N/A"
    if "Position_Indices" not in kwargs:
        position_indices = create_position_indices(hdf_parent, hsi_3d=hsi_3d)
        kwargs["Position_Indices"] = position_indices.ref
    if "Position_Values" not in kwargs:
        position_values = create_position_values(hdf_parent, hsi_3d=hsi_3d)
        kwargs["Position_Values"] = position_values.ref
    if "Spectroscopic_Indices" not in kwargs:
        spectroscopic_indices = create_spectroscopic_indices(hdf_parent, hsi_3d=hsi_3d)
        kwargs["Spectroscopic_Indices"] = spectroscopic_indices.ref
    if "Spectroscopic_Values" not in kwargs:
        spectroscopic_values = create_spectroscopic_values(hdf_parent, hsi_3d=hsi_3d)
        kwargs["Spectroscopic_Values"] = spectroscopic_values.ref
        
    if not hsi_3d.flags["C_CONTIGUOUS"]:
        hsi_3d = np.ascontiguousarray(hsi_3d)
    hsi_3d = np.reshape(hsi_3d, (-1,hsi_3d.shape[2]))
    return create_dataset(hdf_parent, name, data=hsi_3d, **kwargs)
