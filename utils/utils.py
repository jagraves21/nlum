import time
from datetime import timedelta

import cProfile

def time_function(msg="Elapsed Time:"):
    def real_timing_function(function):
        def wrapper(*args, **kwargs):
            start_time = time.time()
            res = function(*args, **kwargs)
            elapsed = time.time() - start_time
            print msg, timedelta(seconds=elapsed)
            return res
        return wrapper
    return real_timing_function

def profile_code(run_profiler, out_filename, profiler=cProfile.Profile()):
    def run_profile(function):
        def wrapper(*args, **kwargs):
            try:
                profiler.enable()
                res = function(*args, **kwargs)
                profiler.disable()
                return res
            finally:
                profiler.print_stats()
                profiler.dump_stats(out_filename)
        return wrapper

    def skip_profile(function):
        def wrapper(*args, **kwargs):
            res = function(*args, **kwargs)
            return res
        return wrapper

    if run_profiler:
        return run_profile
    else:
        return skip_profile

