import numpy as np

import matplotlib as mpl
from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import ticker
from mpl_toolkits.axes_grid1 import make_axes_locatable

from scipy import spatial

def display_matrix(matrix, floatfmt="0.4f", title_kwargs=None, xlabel_kwargs=None, ylabel_kwargs=None, figsize=(3,3), cmap=mpl.cm.binary, ax=None):
    
    if len(matrix.shape) == 1:
        matrix = spatial.distance.squareform(matrix)
    
    no_ax = ax is None
    if no_ax:
        fig, ax = plt.subplots(figsize=figsize)
        
    if title_kwargs is not None:
        title_kwargs.setdefault("y", 1.08)
        ax.set_title(**title_kwargs)
    if xlabel_kwargs is not None:
        ax.set_xlabel(**xlabel_kwargs)
    if ylabel_kwargs is not None:
        ax.set_ylabel(**ylabel_kwargs)
    
    xticks = range(1,matrix.shape[1]+1)
    ax.set_xticklabels([""]+xticks)
    ax.xaxis.set_major_locator(ticker.MultipleLocator(1))
    
    yticks = range(1,matrix.shape[0]+1)
    ax.set_yticklabels([""]+yticks)
    ax.yaxis.set_major_locator(ticker.MultipleLocator(1))
            
    ax.tick_params(axis="both", which="both", labelbottom=True, labeltop=True, labelleft=True, labelright=True, length=0)
    
    vmin, vmax = np.nanmin(matrix), np.nanmax(matrix)
    im = ax.imshow(matrix, interpolation="nearest", cmap=cmap, vmin=vmin, vmax=vmax)
    
    threshold = (matrix.max()-matrix.min()) / 2. + matrix.min()
    for ii in xrange(matrix.shape[0]):
        for jj in xrange(matrix.shape[1]):
            text = format(matrix[ii,jj], floatfmt)
            if matrix[ii,jj] > threshold:
                color = "white"
            else:
                color = "black"
            plt.text(jj, ii, text, horizontalalignment="center", color=color)
        
    if no_ax:
        divider = make_axes_locatable(ax)
        cax = divider.append_axes("right", size="5%", pad=0.25)
        plt.colorbar(im, cax=cax, ticks=np.linspace(matrix.min(), matrix.max(), 5))
        plt.show()

def display_cube(hsi_3d, top_band=None, top_cmap=plt.cm.jet, side_cmap=plt.cm.jet, aspect="auto", elev=None, azim=-30, title_kwargs=None, figsize=None, ax=None):
    top_cmap = plt.get_cmap(top_cmap)
    side_cmap = plt.get_cmap(side_cmap)

    if top_band is None:
        top_band = np.argmax(np.var(np.reshape(hsi_3d, (-1, hsi_3d.shape[2])), 0))

    no_ax = ax is None
    if no_ax:
        fig = plt.figure(figsize=figsize)
        ax = fig.gca(projection="3d")

    if title_kwargs is not None:
        ax.set_title(**title_kwargs)
    ax.set_aspect(aspect)
    ax.set_axis_off()
    ax.view_init(elev, azim)

    # X constant faces
    YY, ZZ, = np.mgrid[0:hsi_3d.shape[1], 0:hsi_3d.shape[2]]
    XX = np.zeros(YY.shape)
    #ax.plot_surface(XX, YY, ZZ, shade=False, facecolors=side_cmap(plt.Normalize()(hsi_3d[0,:,:])))
    XX += hsi_3d.shape[0]-1
    ax.plot_surface(XX, YY, ZZ, shade=False, facecolors=side_cmap(plt.Normalize()(hsi_3d[-1,:,:])))

    # Y constant faces
    XX, ZZ, = np.mgrid[0:hsi_3d.shape[0], 0:hsi_3d.shape[2]]
    YY = np.zeros(XX.shape)
    ax.plot_surface(XX, YY, ZZ, shade=False, facecolors=side_cmap(plt.Normalize()(hsi_3d[:,0,:])))
    YY += hsi_3d.shape[1]-1
    #ax.plot_surface(XX, YY, ZZ, shade=False, facecolors=side_cmap(plt.Normalize()(hsi_3d[:,-1,:])))

    # Z constant faces
    XX, YY, = np.mgrid[0:hsi_3d.shape[0], 0:hsi_3d.shape[1]]
    ZZ = np.zeros(XX.shape)
    #ax.plot_surface(XX, YY, ZZ, shade=False, facecolors=side_cmap(plt.Normalize()(hsi_3d[:,:,0])))
    ZZ += hsi_3d.shape[2]-1
    ax.plot_surface(XX, YY, ZZ, shade=False, facecolors=top_cmap(plt.Normalize()(hsi_3d[:,:,top_band])))

    # Create cubic bounding box to simulate equal aspect ratio
    max_range = np.max(hsi_3d.shape)
    Xb = 0.5*max_range*np.mgrid[-1:2:2,-1:2:2,-1:2:2][0].flatten() + 0.5*(hsi_3d.shape[0])
    Yb = 0.5*max_range*np.mgrid[-1:2:2,-1:2:2,-1:2:2][1].flatten() + 0.5*(hsi_3d.shape[1])
    Zb = 0.5*max_range*np.mgrid[-1:2:2,-1:2:2,-1:2:2][2].flatten() + 0.5*(hsi_3d.shape[0])
    # Comment or uncomment following both lines to test the fake bounding box:
    for xb, yb, zb in zip(Xb, Yb, Zb):
        pass #ax.plot([xb], [yb], [zb], 'w')

    if no_ax:
#         plt.tight_layout()
#         plt.savefig("cube.eps", format="eps", transparent=True)
        plt.show()
        
def display_pixel_spectra(hsi_3d, hsi_3d_2=None, n_spectra=8, n_columns=4, figsize=(3,3)):
    hsi_2d = np.reshape(hsi_3d, (-1,hsi_3d.shape[2]))
    qq,rr = divmod(hsi_2d.shape[0],n_spectra)
    pixels = xrange(0,hsi_2d.shape[0]-rr,qq)

    n_rows = int(np.ceil( len(pixels)/float(n_columns) ) )
    
    figsize = (n_columns*figsize[0], n_rows*figsize[1])

    fig = plt.figure(figsize=figsize)
    gs = mpl.gridspec.GridSpec(n_rows, n_columns)
    for index,pixel in enumerate(pixels):
        row,col = divmod(index, n_columns)
        spectrum = hsi_2d[pixel]
        ax = fig.add_subplot(gs[row,col])
        pixel_loc = tuple(ii+1 for ii in divmod(pixel,hsi_3d.shape[1]))
        ax.set_title("Pixel {}".format(pixel_loc))
        ax.set_xticks([])
        ax.set_yticks([])
        ax.plot(spectrum)
        
        if hsi_3d_2 is not None:
            tmp = hsi_3d_2.reshape((-1,hsi_3d_2.shape[-1]))
            ax.plot(tmp[pixel])
    plt.tight_layout()
    plt.show()
    
def display_spectral_bands(hsi_3d, n_bands=8, n_columns=4, cmap=mpl.cm.jet, figsize=(3,3)):
    qq,rr = divmod(hsi_3d.shape[2],n_bands)
    bands = xrange(0,hsi_3d.shape[2]-rr,qq)

    n_rows = int(np.ceil( len(bands)/float(n_columns) ) )
    
    figsize = (n_columns*figsize[0], n_rows*figsize[1])

    fig = plt.figure(figsize=figsize)
    gs = mpl.gridspec.GridSpec(n_rows, n_columns)
    for index,band in enumerate(bands):
        row,col = divmod(index, n_columns)
        band_image = hsi_3d[:,:,band]
        ax = fig.add_subplot(gs[row,col])
        ax.set_title("Band {}".format(band))
        ax.set_xticks([])
        ax.set_yticks([])
        ax.imshow(band_image, cmap=cmap)
    plt.tight_layout()
    plt.show()
    
def display_hsi_labels(labels, unique_labels, cmap=mpl.cm.jet, figsize=(5,5), title_kwargs=None, ax=None):
    cmap = plt.get_cmap(cmap)
    
    no_ax = ax is None
    if no_ax:
        fig, ax = plt.subplots(figsize=figsize)
    
    if title_kwargs is not None:
        ax.set_title(**title_kwargs)
    ax.set_xticks([])
    ax.set_yticks([])
    
    cmap = plt.get_cmap(cmap.name, np.max(labels)-np.min(labels)+1)
    im = ax.imshow(labels, cmap=cmap, vmin = np.min(labels)-.5, vmax = np.max(labels)+.5)
    colors = im.cmap(im.norm(unique_labels))
    
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.05)
    if no_ax:
        fig.colorbar(im, cax=cax)
        plt.show()
    else:
        ax.get_figure().colorbar(im, cax=cax)
    
    return colors

def display_label_histogram(labels, unique_labels, label_counts, colors=None, figsize=(5,5), title_kwargs=None, xlabel_kwargs=None, ylabel_kwargs1=None, ylabel_kwargs2=None, ax=None):
    no_ax = ax is None
    if no_ax:
        fig, ax = plt.subplots(figsize=figsize)
    
    if title_kwargs is not None:
        ax.set_title(**title_kwargs)
    if xlabel_kwargs is not None:
        ax.set_xlabel(**xlabel_kwargs)
    if ylabel_kwargs1 is not None:
        ax.set_ylabel(**ylabel_kwargs1)
    if ylabel_kwargs2 is not None:
        ax = ax.twinx()
        ax.set_ylabel(**ylabel_kwargs2)
    
    ax.bar(unique_labels, label_counts, color=colors)
    ax.bar(unique_labels, label_counts/float(np.sum(label_counts)), color=colors)
    
    if no_ax:
        plt.show()

def display_endmembers(endmembers, ylim=None, title_kwargs=None, xlabel_kwargs=None, ylabel_kwargs=None, figsize=(5,5), ax=None, output_filename=None):
    if ylim is None:
        ylim = np.min([0.0,endmembers.min()]), endmembers.max()

    no_ax = ax is None
    if no_ax:
        fig, ax = plt.subplots(figsize=figsize)
    
    if title_kwargs is not None:
        ax.set_title(**title_kwargs)
    if xlabel_kwargs is not None:
        ax.set_xlabel(**xlabel_kwargs)
    if ylabel_kwargs is not None:
        ax.set_ylabel(**ylabel_kwargs)
    ax.set_ylim(ylim)
    
    lines = ax.plot( np.transpose(endmembers) )
    colors = [line.get_color() for line in lines]

    if no_ax:
        if output_filename is not None:
            plt.gcf().patch.set_facecolor("w")
            plt.gcf().patch.set_alpha(0.0)
            for ax in plt.gcf().get_axes(): ax.patch.set_facecolor("w"), ax.patch.set_alpha(1.0)
            plt.savefig(output_filename, format="png", facecolor=plt.gcf().get_facecolor(), edgecolor="none")
        plt.show()

    return colors





def display_endmember_components(endmembers, ylim=None, same_scale=False, vertical=True, ax_title_kwargs=None, plot_kwargs=None, xlabel_kwargs=None, ylabel_kwargs=None, figsize=(5,5), fig=None, gs=None):
    n_components = len(endmembers)
    if same_scale and ylim is None:
        ylim = np.min([0., endmembers.min()]), np.max(endmembers)

    no_fig = fig is None
    if no_fig:
        if vertical:
            gs = mpl.gridspec.GridSpec(n_components, 1)
            figsize = (figsize[0], figsize[1]*n_components)
        else:
            gs = mpl.gridspec.GridSpec(1, n_components)
            figsize = (figsize[0]*n_components, figsize[1])

        fig = plt.figure(figsize=figsize)

    for index in xrange(n_components):
        ax = fig.add_subplot(gs[index])
        if isinstance(ax_title_kwargs, dict):
            ax.set_title(**ax_title_kwargs)
        elif ax_title_kwargs is not None:
            ax.set_title(**ax_title_kwargs[index])
        
        if vertical:
            if index+1 != n_components:
                ax.set_xticks([])
            elif xlabel_kwargs is not None:
                ax.set_xlabel(**xlabel_kwargs)

            if ylabel_kwargs is not None:
                ax.set_ylabel(**ylabel_kwargs)
        else:
            if index != 0 and same_scale:
                ax.set_yticks([])
            if index == 0 and ylabel_kwargs is not None:
                ax.set_ylabel(**ylabel_kwargs)

            if xlabel_kwargs is not None:
                ax.set_xlabel(**xlabel_kwargs)
        if ylim is not None:
            ax.set_ylim(ylim)

        if isinstance(plot_kwargs, dict):
            kwargs = plot_kwargs
        elif plot_kwargs is not None:
            kwargs = plot_kwargs[index]
        else:
            kwargs = {}
        
        ax.plot(endmembers[index,:], **kwargs)
        
        if not no_fig:
            fig.add_subplot(ax)
    if no_fig:
        plt.show()
    
def display_abundance_map_components(abundances, vmin=None, vmax=None, same_scale=False, vertical=True, cmap=mpl.cm.viridis, ax_title_kwargs=None, figsize=(5,5), fig=None, gs=None):
    n_components = len(abundances)
    
    if same_scale:
        if vmin is None:            
            vmin = np.min([0., abundances.min()])
        if vmax is None:
            vmax = np.max(abundances)

    no_fig = fig is None
    if no_fig:
        if vertical:
            gs = mpl.gridspec.GridSpec(n_components, 1)
            figsize = (figsize[0], figsize[1]*n_components)
        else:
            gs = mpl.gridspec.GridSpec(1, n_components)
            figsize = (figsize[0]*n_components, figsize[1])

        fig = plt.figure(figsize=figsize)

    for index in xrange(n_components):
        ax = fig.add_subplot(gs[index])
        
        if isinstance(ax_title_kwargs, dict):
            ax.set_title(**ax_title_kwargs)
        elif ax_title_kwargs is not None:
            ax.set_title(**ax_title_kwargs[index])
        ax.set_xticks([])
        ax.set_yticks([])

        im = ax.imshow(abundances[index,:,:], vmin=vmin, vmax=vmax, cmap=cmap)
        if vertical or not same_scale:
            divider = make_axes_locatable(ax)
            cax = divider.append_axes("right", size="5%", pad=0.05)
            fig.colorbar(im, cax=cax)

        if not no_fig:
            fig.add_subplot(ax)
    if no_fig:
        plt.show()

def display_components(endmembers=None, abundances=None, ylim=None, vmin=None, vmax=None, same_scale_endmembers=False, same_scale_abundance_maps=False, vertical=True, endmember_plot_kwargs=None, cmap=mpl.cm.jet, title_kwargs=None, xlabel_kwargs=None, ylabel_kwargs=None, figsize=(5,5), wspace=None, hspace=None):
    if endmembers is not None and abundances is not None:
        if len(endmembers) != len(abundances):
            message = "endmember count ({}) and abundances count ({}) must match"
            message = message.format(endmembers.shape, abundances.shape)
            raise RuntimeError(message)
        n_components = len(endmembers)

        if vertical:
            figsize = (2*figsize[0], n_components*figsize[1])
            outer_gs = mpl.gridspec.GridSpec(1, 2, wspace=wspace)
            em_gs = mpl.gridspec.GridSpecFromSubplotSpec(n_components, 1, subplot_spec=outer_gs[0], hspace=hspace)
            am_gs = mpl.gridspec.GridSpecFromSubplotSpec(n_components, 1, subplot_spec=outer_gs[1], hspace=hspace)
        else:
            figsize = (n_components*figsize[0], 2*figsize[1])
            outer_gs = mpl.gridspec.GridSpec(2, 1, hspace=hspace)
            em_gs = mpl.gridspec.GridSpecFromSubplotSpec(1, n_components, subplot_spec=outer_gs[0], wspace=wspace)
            am_gs = mpl.gridspec.GridSpecFromSubplotSpec(1, n_components, subplot_spec=outer_gs[1], wspace=wspace)

        fig = plt.figure(figsize=figsize)
        if title_kwargs is not None:
            fig.suptitle(**title_kwargs)

        display_endmember_components(endmembers, ylim=ylim, same_scale=same_scale_endmembers, vertical=vertical, plot_kwargs=endmember_plot_kwargs, xlabel_kwargs=xlabel_kwargs, ylabel_kwargs=ylabel_kwargs, figsize=figsize, fig=fig, gs=em_gs)
        display_abundance_map_components(abundances, vmin=vmin, vmax=vmax, same_scale=same_scale_abundance_maps, vertical=vertical, cmap=cmap, figsize=figsize, fig=fig, gs=am_gs)

        plt.show()
    elif endmembers is not None:
        display_endmember_components(endmembers, ylim=ylim, same_scale=same_scale_endmembers, vertical=vertical, plot_kwargs=endmember_plot_kwargs, xlabel_kwargs=xlabel_kwargs, ylabel_kwargs=ylabel_kwargs, figsize=figsize)
    elif abundances is not None:
        display_abundance_map_components(abundances, vmin=vmin, vmax=vmax, same_scale=same_scale_abundance_maps, vertical=vertical, cmap=cmap, figsize=figsize)
    else:
        raise RuntimeError("both endmembers and abundances cannot be None")

def display_endmember_comparison(endmembers1, endmembers2, ylim1=None, ylim2=None, same_scale=False, vertical=False, ax_title1_kwargs=None, ax_title2_kwargs=None, plot1_kwargs=None, plot2_kwargs=None, title_kwargs=None, xlabel_kwargs=None, ylabel_kwargs=None, figsize=(5,5), wspace=None, hspace=None):
    n_components = np.max([endmembers1.shape[0], endmembers2.shape[0]])
    
    if same_scale:
        if ylim1 is None and ylim2 is None:
            ylim1 = np.min([0.0,endmembers1.min(),endmembers2.min()]), np.max([endmembers1.max(),endmembers2.max()])
            ylim2 = ylim1
        elif ylim1 is None:
            ylim1 = np.min([0.0,endmembers1.min()]), endmembers1.max()
        else:
            ylim2 = np.min([0.0,endmembers2.min()]), endmembers2.max()
    
    if vertical:
        figsize = (2*figsize[0], n_components*figsize[1])
        outer_gs = mpl.gridspec.GridSpec(1, 2, wspace=wspace)
        em1_gs = mpl.gridspec.GridSpecFromSubplotSpec(n_components, 1, subplot_spec=outer_gs[0], hspace=hspace)
        em2_gs = mpl.gridspec.GridSpecFromSubplotSpec(n_components, 1, subplot_spec=outer_gs[1], hspace=hspace)
    else:
        figsize = (n_components*figsize[0], 2*figsize[1])
        outer_gs = mpl.gridspec.GridSpec(2, 1, hspace=hspace)
        em1_gs = mpl.gridspec.GridSpecFromSubplotSpec(1, n_components, subplot_spec=outer_gs[0], wspace=wspace)
        em2_gs = mpl.gridspec.GridSpecFromSubplotSpec(1, n_components, subplot_spec=outer_gs[1], wspace=wspace)

    fig = plt.figure(figsize=figsize)
    if title_kwargs is not None:
        fig.suptitle(**title_kwargs)

    display_endmember_components(endmembers1, ylim=ylim1, vertical=vertical, ax_title_kwargs=ax_title1_kwargs, plot_kwargs=plot1_kwargs, same_scale=same_scale, xlabel_kwargs=xlabel_kwargs, ylabel_kwargs=ylabel_kwargs, fig=fig, gs=em1_gs)
    display_endmember_components(endmembers2, ylim=ylim2, vertical=vertical, ax_title_kwargs=ax_title2_kwargs, plot_kwargs=plot2_kwargs, same_scale=same_scale, xlabel_kwargs=xlabel_kwargs, ylabel_kwargs=ylabel_kwargs, fig=fig, gs=em2_gs)
    
    plt.show()
    
def display_abundance_map_comparison(abundance_maps1, abundance_maps2, vmin1=None, vmax1=None, vmin2=None, vmax2=None, same_scale=False, vertical=False, cmap=mpl.cm.viridis, ax_title1_kwargs=None, ax_title2_kwargs=None, title_kwargs=None, xlabel_kwargs=None, ylabel_kwargs=None, figsize=(5,5), wspace=None, hspace=None):
    n_components = np.max([abundance_maps1.shape[0], abundance_maps2.shape[0]])
    
    if same_scale:
        if vmin1 is None and vmin2 is None:
            vmin1 = np.min([0.0,abundance_maps1.min(),abundance_maps2.min()])
            vmin2 = vmin1
        elif vmin1 is None:
            vmin1 = np.min([0.0,abundance_maps1.min()])
        else:
            vmin2 = np.min([0.0,abundance_maps2.min()])
        
        if vmin1 is None and vmin2 is None:
            vmax1 = np.max([abundance_maps1.max(),abundance_maps2.max()])
            vmax2 = vmax1
        elif vmax1 is None:
            vmax1 = abundance_maps1.max()
        else:
            vmax2 = abundance_maps2.max()

    if vertical:
        figsize = (2*figsize[0], n_components*figsize[1])
        outer_gs = mpl.gridspec.GridSpec(1, 2, wspace=wspace)
        em1_gs = mpl.gridspec.GridSpecFromSubplotSpec(n_components, 1, subplot_spec=outer_gs[0], hspace=hspace)
        em2_gs = mpl.gridspec.GridSpecFromSubplotSpec(n_components, 1, subplot_spec=outer_gs[1], hspace=hspace)
    else:
        figsize = (n_components*figsize[0], 2*figsize[1])
        outer_gs = mpl.gridspec.GridSpec(2, 1, hspace=hspace)
        em1_gs = mpl.gridspec.GridSpecFromSubplotSpec(1, n_components, subplot_spec=outer_gs[0], wspace=wspace)
        em2_gs = mpl.gridspec.GridSpecFromSubplotSpec(1, n_components, subplot_spec=outer_gs[1], wspace=wspace)

    fig = plt.figure(figsize=figsize)
    if title_kwargs is not None:
        fig.suptitle(**title_kwargs)
    
    display_abundance_map_components(abundance_maps1, vmin=vmin1, vmax=vmax1, same_scale=same_scale, vertical=vertical, cmap=cmap, ax_title_kwargs=ax_title1_kwargs, fig=fig, gs=em1_gs)
    display_abundance_map_components(abundance_maps2, vmin=vmin2, vmax=vmax2, same_scale=same_scale, vertical=vertical, cmap=cmap, ax_title_kwargs=ax_title2_kwargs, fig=fig, gs=em2_gs)

    plt.show()
